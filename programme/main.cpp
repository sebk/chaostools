/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <thread>
#include <boost/program_options/parsers.hpp>
#include <signal.h>

#include "configreader.h"
#include "pool.h"

// instances
#include "logistische_gleichung.h"
#include "duffing_gleichung.h"
#include "duffing-gleichung-bifurkation.h"
#include "duffing-gleichung-bifurkation_3d.h"
#include "duffing_gleichung_convergence.h"
#include "sinusabbildung.h"
#include "elektronischer-oszillator.h"
#include "elektronischer-oszillator-bifurkation.h"
#include "elektronischer-oszillator-phasenportrait.h"
#include "npy_feigenbaum.h"
#include "npy_phasespace.h"
#include "pendel_trace.h"
#include "pendel_convergence.h"
#include "pendel_pot.h"

Pool<MAIN_CLASS, ConfigReader&>* poolInstance;

void handleSigINT( int signum ) {
    poolInstance->stop();
    signal( SIGINT, SIG_DFL );
}
    
int main( int argc, char* argv[] )
{
    ConfigReader config;
    int loglevel = 0;
    
    config.args( argc, argv, loglevel );
    
    config.setSection( "default" );
    uint threads = config.get<uint>( "threads" );
    if( MAIN_CLASS::maxThreads && MAIN_CLASS::maxThreads < threads ) {
        std::cout << "Warning: thread count reduced to " << MAIN_CLASS::maxThreads << std::endl;
        threads = MAIN_CLASS::maxThreads;
    }
    
    Pool<MAIN_CLASS, ConfigReader&> p( threads, config );
    poolInstance = &p;
    signal( SIGINT, handleSigINT );
    
    p.start( config );
    p.wait( loglevel );
    
    p.collect()->finish( config );
    
    return 0;
}
