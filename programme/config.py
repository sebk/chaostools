#!/usr/bin/python2.7

#   This file is part of ChaosTools.
#
#   ChaosTools is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   ChaosTools is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.

import re
import os.path


class ConfigReader:
    expr = re.compile(r"%\(([a-zA-Z0-9._\-]+)\)")
    
    def __init__(self):
        self.d = {}
    
    def copy(self):
        c = ConfigReader()
        for k, v in self.d.items():
            c.d[k] = list(v)
        
        return c
    
    def read(self, filename):
        f = open(filename, "rt")
        section = None
        
        while True:
            l = f.readline()
            if not l:
                break
            
            i = l.find("#")
            if i != -1:
                l = l[:i]
            
            l = l.strip()
            if not l:
                continue
            
            if l[0] == "@":
                args = l[1:].strip().split()
                if args[0] == "include":
                    self.read( args[1] )
                elif args[0] == "alias":
                    self.d[args[1]] = self.d[args[2]]
                else:
                    print("unknown command %s" % args[0])
                
                continue
            
            if l[0] == "[":
                assert l[-1] == "]"
                s = l[1:-1]
                if s in self.d:
                    section = self.d[s]
                else:
                    section = list()
                    self.d[s] = section
                
                continue
            
            i = l.find("=")
            key = l[:i].strip()
            value = l[i+1:].strip().decode("utf-8")
            
            found = False
            for i in range( len(section) ):
                if section[i][0] == key:
                    section[i] = (key, value)
                    found = True
                    continue
            
            if not found:
                section.append((key, value))
        
        f.close()
    
    def repl(self, m):
        sec, key = m.group(1).rsplit(".")
        return self.get(sec, key)
    
    def repl2(self, m):
        sec, key = m.group(1).rsplit(".")
        v = self.get(sec, key)
        try:
            f = float(v)
            return MathNumber(f)
        except:
            return v
        
    def parse(self, s, LaTeX, convert):
        try:
            p = self.expr.sub(self.repl2 if LaTeX else self.repl, s)
            
            if convert:
                return self.convert(p)
            else:
                return p
                
        except KeyError as e:
            KeyError("error while parsing %r: %r" % (s, e))
    
    def convert(self, s):
        try:
            return int(s)
        except ValueError:
            pass
          
        try:
            return float(s)
        except ValueError:
            pass
          
        return s
    
    def get(self, section, key, LaTeX = False, convert=False):
        sectionl = self.d[section]
        
        for i in range( len(sectionl) ):
            if sectionl[i][0] == key:
                return self.parse( sectionl[i][1], LaTeX, convert )
                
        raise KeyError("could not find %s.%s" % (section, key))
    
    
    def getDefault(self, section, key, default, LaTeX = False, convert=False):
        try:
            return self.get(section, key, LaTeX, convert)
        except KeyError:
            return default
    
    def items(self, section, LaTeX = False, convert = False, empty=[]):
        if section in self.d:
            return list( (k, self.parse(v, LaTeX, convert)) for k, v in self.d[section] )
        else:
            return empty
            
    def save(self, filename):
        if os.path.exists(filename):
            raise ValueError("file exists")
       
        
        f = open(filename, "wt")
        for section, entries in self.d.items():
            print(section)
            f.write("[%s]\n" % section)
            for key, value in entries:
                f.write("    %s = %s\n" % (key, value) )
            
            f.write("\n")
        
        f.close()

if __name__ == "__main__":
    from sys import argv
    if argv[1] == "apply":
        c = ConfigReader()
        for f in argv[3:]:
            c.read( f )
        
        c.save(argv[2])
        