/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "socketstream.h"
#include <string.h>

inline std::size_t SocketStream::readInto( char* buffer, std::size_t maxSize, std::size_t minSize )
{
    assert( maxSize > 0 );
    assert( minSize > 0 );

    std::size_t written = 0;
    while( written < minSize ) {
        ssize_t s = ::read( m_socket, buffer + written, maxSize - written );
        if( s <= 0 )
            throw SocketError();
        else {
            m_count += s;
            written += s;   
        }
    }

    return written;
}

inline std::size_t SocketStream::findEndline( std::size_t maxLength, char delim )
{
    char* endLine = m_buffer + m_buffer_start;
    for( char* bufferEnd = m_buffer + m_buffer_start + m_buffer_fill; endLine < bufferEnd && *endLine != delim; endLine++ );

    return endLine - (m_buffer + m_buffer_start);
}

inline void SocketStream::buffer_recv( std::size_t minSize )
{
    assert( m_buffer_start + m_buffer_fill < m_buffer_size );
    
    std::size_t maxSize = m_buffer_size - m_buffer_start - m_buffer_fill;
    
    assert( minSize <= maxSize );
    
    // TODO
    m_buffer_fill += readInto( m_buffer + m_buffer_start, maxSize, minSize );
    //m_buffer_fill += readInto( m_buffer + m_buffer_start, minSize, minSize );
}

inline void SocketStream::buffer_take( char* buffer, std::size_t size )
{
    assert( m_buffer_start + m_buffer_fill <= m_buffer_size );
    assert( size <= m_buffer_fill );
    
    memcpy( buffer, m_buffer + m_buffer_start, size );
    m_buffer_start += size;
    m_buffer_fill -= size;
}

inline void SocketStream::buffer_reset()
{
    assert( m_buffer_fill == 0 );
    m_buffer_start = 0;
}

void SocketStream::read( char* buffer, std::size_t size )
{
    if( size <= m_buffer_fill ) {
        buffer_take( buffer, size );
        return;
    }

    std::size_t written = 0;
    if( m_buffer_fill ) {
        written += m_buffer_fill;
        buffer_take( buffer, m_buffer_fill );
    }

    buffer_reset();
    
    std::size_t length = size - written;
    if( length >= m_buffer_size ) {
        written += readInto( buffer + written, length, length );
    } else {
        buffer_recv( length );
        buffer_take( buffer + written, length );
    }
}

std::size_t SocketStream::readLine( char* buffer, std::size_t size, char delim )
{
    std::size_t written = 0;
    while( size > written ) {
        if( m_buffer_fill ) {
            std::size_t end = findEndline( std::min( size, m_buffer_fill ), delim );
            if( end < m_buffer_fill ) {
                std::size_t length = std::min( end + 1, size - written );
                buffer_take( buffer + written, length );
                
                written += length - 1;
                buffer[ written ] = '\0';
                return written;
            } else {
                std::size_t fill = m_buffer_fill;
                buffer_take( buffer + written, fill );
                buffer_reset();
                written += fill;
            }
        }

        buffer_recv( 1 );
    }
    
    return written;
}
