/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CANVAS_QWT_RASTER_DATA_H
#define CANVAS_QWT_RASTER_DATA_H

#define CANVAS_TRACK_MAXVAL 1
#define CANVAS_ENABLE_RESIZE 1

#include "canvas.h"
#include <qt4/Qt/qnamespace.h>
#include <qt4/Qt/qrect.h>
#include <qwt6/qwt_raster_data.h>

class CanvasQwtRasterData: public Canvas<double, double>, public QwtRasterData
{
public:
    CanvasQwtRasterData( ConfigReader& conf, const char* name = "canvas" );
    CanvasQwtRasterData( double x0, double x1, double y0, double y1, int width, int height );
    virtual QRectF pixelHint( const QRectF& area ) const;
    virtual double value( double x, double y ) const;
    virtual void updateSize();
    virtual ~CanvasQwtRasterData();
    
    void updateRange();
};

#endif
