/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NPY_FEIGENBAUM_H
#define NPY_FEIGENBAUM_H

#ifndef USE_SINGLE
#define USE_NPY_FEIGENBAUM
#endif

#ifdef USE_NPY_FEIGENBAUM
#define MAIN_CLASS NpyFeigenbaum

#include "canvas.h"
#include "npy_processer.h"

class NpyFeigenbaum: public NpyProcessor {
protected:
    boost::filesystem::path             m_directory;
    Canvas<double, float>               m_canvas;
    
    virtual void processArray( npArray_t::array_ptr array )
    {
        array_t::const_multi_array_ref ref( *array );
        
        size_t N = ref.shape()[0];
        
        if( N < 2 )
            return;
        
        size_t M = ref.shape()[1];
        if( m_timeChannel >= M || m_XChannel >= M || m_YChannel >= M )
            throw;
        
        
        // find trigger
        std::vector<double> slopes;
        findTriggerSlopes( slopes, ref, m_triggerChannel, m_triggerThreshold, edgeType::RisingEdge );
        
        int slopeCount = slopes.size();
        
        if( slopeCount < 2 )
            return;
        
        // measure mean voltage
        double sum = 0.0;
        
        size_t n0 = slopes[0];
        size_t n1 = slopes[slopeCount-1];
        for( size_t n = n0; n < n1; n++ ) {
            double v = ref[n][m_XChannel];
            sum += v * v;
        }
        
        double avgVoltage = sqrt( sum / (n1 - n0) );
        std::cout << "  " << avgVoltage << "V";
        
        double yMin = 0.0;
        double yMax = 0.0;
        
        for( int slope = -1; slope < slopeCount; slope++ ) {
            double t0 = slope >= 0 ? slopes[slope] : 2.0 * slopes[0] - slopes[1];
            double t1 = slope < (slopeCount - 1) ? slopes[slope + 1] : 2.0 * slopes[slopeCount - 1] - slopes[slopeCount - 2];
            
            double t = t0 * (1.0 - m_phaseshift) + m_phaseshift * t1;
            if( t >= 0.0 ) {
                size_t n = t;
                if( t < N ) {
                    double y = ref[n][m_YChannel];
                    m_canvas.add( avgVoltage, y, 1.0 );
                    
                    if( y > yMax )
                        yMax = y;
                    
                    if( y < yMin )
                        yMin = y;
                }
            }
        }
        
        std::cout << "  " 
            << (slopeCount - 1) / (slopes[slopeCount-1] - slopes[0]) * (N-1) / (ref[N-1][m_timeChannel] - ref[0][m_timeChannel])
            << "Hz" << "  " << yMin << "V | "<< yMax << "V";
    }
    
    
public:
    NpyFeigenbaum( ConfigReader& config ): NpyProcessor( config ), m_canvas( config, 0.0 )
    {
        config.setSection( "feigenbaum" );
        m_directory = boost::filesystem::path( config.get<std::string>("dir") );
    }
    
    virtual void run()
    {
        scanDirectory( m_directory );
    }
    
    void finish( ConfigReader& conf )
    {
        conf.setSection("default");
        m_canvas.write( conf.get<std::string>("datafile"));
    }
    
    
};

#endif
#endif

