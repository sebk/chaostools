/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_DUFFING_GL_BIFURK
#endif

#ifdef USE_DUFFING_GL_BIFURK
#define MAIN_CLASS DuffingGleichungBifurkation

#include "canvas.h"
#include "instance.h"
#include "pool.h"
#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <functional>
#include "configreader.h"
#include "duffing.h"

class DuffingGleichungBifurkation : public Instance {
public:
    typedef double real;
    typedef Canvas<real, float>       canvas_t;
    typedef boost::array<real, 2>       state_t;
    
    DuffingGleichungBifurkation( ConfigReader& config ):
        Instance(),
        m_canvas( config, 0 ),
        m_canvas2( config, 0, "canvas2" )
    {
        config.setSection("duffing");
        m_duffing.init( config );
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        m_x0 = config.get<real>("x0");
        m_y0 = config.get<real>("y0");
        m_phase = config.get<real>("phase");
        m_samples = config.get<int>("samples");
        m_points = config.get<int>("points");
        
        m_convergence = config.get<bool>("convergence", false);
        
        std::string paramName = config.get<std::string>("param");
        if( paramName == "epsilon" )
            m_param = &m_duffing.epsilon;
        else if( paramName == "lambda" )
            m_param = &m_duffing.lambda;
        else if( paramName == "alpha" )
            m_param = &m_duffing.alpha;
        else if( paramName == "beta" )
            m_param = &m_duffing.beta;
        else
            throw std::runtime_error("param invalid");
    }
    
    virtual void run()
    {
        // boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real> stepper;
        boost::numeric::odeint::controlled_runge_kutta<boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real>> stepper;
        
        boost::random::mt19937_64 generator;
        boost::random::uniform_real_distribution<real> distributionA( -m_x0, m_x0 );
        boost::random::uniform_real_distribution<real> distributionB( -m_y0, m_y0 );
        
        real p_delta = (m_canvas.xValue( m_canvas.width() ) - m_canvas.xValue( 0 )) / m_samples;
        real p_0 = m_canvas.xValue( 0 ) + p_delta * (real) m_thread / (real) m_threadCount;
        
        real maxAbsYVal = std::max( fabs(m_canvas.yValue( 0 )), fabs( m_canvas.yValue( m_canvas.height() ) ) );
        
        for( int s = 0; s < m_samples && !m_stop; s++ ) {
            *m_param = p_0 + p_delta * s;
        
            state_t state;
            state[ 0 ] = distributionA( generator );
            state[ 1 ] = distributionB( generator );
            
            real increment = 2.0 * M_PI / m_duffing.Omega;
            real t_0 = m_phase / m_duffing.Omega;
            real t_1 = t_0 + increment;
            int stepCount = (m_t1 - m_t0) / increment;
            
            real lastValue = 0.0;
            for( int step = 0; step < stepCount; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt );
                
                if( fabs( state[ 0 ] - lastValue ) < 1e-6 * maxAbsYVal )
                    break;
                    
                if( (step % 16) == 0 )
                    lastValue = state[ 0 ];
            }
            
            for( int step = 0; step < m_points; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt );
                m_canvas.add( *m_param, state[0], 1 );
                m_canvas2.add( *m_param, state[1], 1 );
            }
            
            setProgress( s, m_samples );
        }
        setProgress( 1.0 );
    }
    
    void collect( DuffingGleichungBifurkation& other ) {
        m_canvas += other.m_canvas;
        m_canvas2 += other.m_canvas2;
    }
    
    void finish( ConfigReader& config ) {
        m_canvas.write();
        m_canvas2.write();
    }
    
private:
    DuffingGl<real>     m_duffing;
    real                m_t0;
    real                m_t1;
    real                m_dt;
    bool                m_convergence;
    real                m_x0;
    real                m_y0;
    canvas_t            m_canvas;
    canvas_t            m_canvas2;
    real                m_phase;
    int                 m_samples;
    int                 m_points;
    
    real*               m_param;
};

#endif
