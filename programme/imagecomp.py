#!/usr/bin/python2.7

from PIL import Image, PngImagePlugin
import numpy as np

import sys
reserved = ('interlace', 'gamma', 'dpi', 'transparency', 'aspect')
meta = PngImagePlugin.PngInfo()

def addMeta(im):
  for k, v in im.info.iteritems():
    if k in reserved: continue
    meta.add_text(k, v, 0)
  

im = Image.open(sys.argv[1])
addMeta(im)
data = np.array( im, dtype=np.uint16 )

for arg in sys.argv[2:-1]:
  i = Image.open(arg)
  addMeta(i)
  data2 = np.array( i, dtype=np.uint16 )
  for i in range(3):
    data[..., ..., i] = (data[..., ..., i] * (255 - data2[..., ..., 3]) + data2[..., ..., i] * data2[..., ..., 3]) / 255

Image.fromarray( np.array(data, dtype=np.uint8) ).save( sys.argv[-1], "PNG", pnginfo=meta )
