/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ELEKTRONISCHER_OSZILLATOR_SYSTEM_H
#define ELEKTRONISCHER_OSZILLATOR_SYSTEM_H

#include <math.h>
#include <boost/array.hpp>
#include <boost/concept_check.hpp>
#include "configreader.h"

template <class real>
class ElektrOszillSystem {
public:
    typedef boost::array<real, 2>       state_t;
    
    real        C_f;
    real        C_r;
    real        I_f;
    real        V_t;
    real        V_s;
    real        phi;
    //real        gamma;
    real        R;
    real        L;
    real        omega;
    
    void init( ConfigReader& config )
    {
        C_f = config.get<real>("C_f");
        C_r = config.get<real>("C_r");
        I_f = config.get<real>("I_f");
        V_t = config.get<real>("V_t");
        V_s = config.get<real>("V_s");
        phi = config.get<real>("phi");
        //gamma = config.get<real>("gamma");
        R = config.get<real>("R");
        L = config.get<real>("L");
        omega = config.get<real>("omega");
    }
    
    void operator()( const state_t& x, state_t& dxdt, const real t )
    {
        real V_d = x[0];
        real I = x[1];
        real e_part = exp( - V_d / V_t );
        
        if( V_d > -phi )
            dxdt[0] = (I - I_f * ( 1.0 - e_part ) ) / (C_r * exp( 1.0 + V_d / phi ));
        else
            dxdt[0] = (I - I_f * ( 1.0 - e_part ) ) / (C_f * e_part);
        
        dxdt[1] = (V_s * cos( omega * t ) - V_d - R * I) / L;
    }
};

template <class real>
class ElektrOszillSystem2 {
public:
    typedef boost::array<real, 2>       state_t;
    
    /* diode parameters */
    real        C_r; // reverse capacity ~ 16 pF
    real        I_0; // 
    real        V_T; // thermal energy in eV
    real        eta; // electon mobility
    real        tau; // pair mobility ~ 1µs
    real        gamma; // magic number ~ 0.4
    real        V_pn; // junction voltage ~ 0.8V
    real        V_sigma; // ~ 0.16
    
    /* other components */
    real        R; // resistor
    real        L; // inducticity
    real        omega; // driving circular frequency
    real        V_s; // driving voltage ~ 1V
    
    /* calculated parameters */
    real        param1; // eta * V_T / tau;
    real        param2; // eta * V_T
    
    // internal variables to match both capacities
    real        C_offset;
    real        V_zero;
    
    void init( ConfigReader& config )
    {
        C_r = config.get<real>("C_r");
        I_0 = config.get<real>("I_0_");
        V_T = config.get<real>("V_T");
        V_s = config.get<real>("V_s");
        V_sigma = config.get<real>("V_sigma");
        eta = config.get<real>("eta");
        tau = config.get<real>("tau");
        gamma = config.get<real>("gamma");
        V_pn = config.get<real>("V_pn");
        R = config.get<real>("R");
        L = config.get<real>("L");
        omega = config.get<real>("omega");
        prepare();
    }
    
    void prepare() {
        param1 = eta * V_T;
        param2 = tau * I_0 / (eta * V_T);
    }
    
    void operator()( const state_t& x, state_t& dxdt, const real t )
    {
        real V_d = x[0];
        real I = x[1];
        
        double var1 = exp( V_d / param1 );
        dxdt[0] = ( I - I_0 * var1 ) / ( param2 * var1 + pow( V_sigma + pow( V_pn - V_d, 2 ), -0.5 * gamma ) );
        dxdt[1] = (V_s * cos( omega * t ) - V_d - R * I) / L;
    }
};
#endif