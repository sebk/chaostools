/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOCKETSTREAM_H
#define SOCKETSTREAM_H

#include <stdexcept>
#include <assert.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

class SocketStream
{
public:
    class SocketError: public std::exception
    {
    };
    
private:
    std::size_t         m_buffer_size;
    int                 m_socket;
    char*               m_buffer;
    std::size_t         m_buffer_start;
    std::size_t         m_buffer_fill; // amount of bytes ready starting at m_buffer + m_buffer_start
    
    std::size_t         m_count;
    
    // reads at least minSize into buffer but not more then maxSize and returns the written bytes
    std::size_t readInto( char* buffer, std::size_t maxSize, std::size_t minSize );
    
    // returns index of delim or maxLength if not found
    std::size_t findEndline( std::size_t maxLength, char delim );
    
    // fill buffer
    void buffer_recv( std::size_t minSize );
    
    // get from buffer
    void buffer_take( char* buffer, std::size_t size );
    void buffer_reset();
    
public:
    SocketStream( int socket, std::size_t buffersize = 256 * 1024 )
    {
        int flags = fcntl( socket, F_GETFL, 0 );
        if( flags < 0 )
            throw SocketError();
        
        if( fcntl(socket, F_SETFL, flags & ~O_NONBLOCK) )
            throw SocketError();
            
        m_buffer_size = buffersize;
        m_buffer_start = 0;
        m_buffer_fill = 0;
        m_count = 0;
        m_socket = socket;
        m_buffer = (char*) std::malloc( m_buffer_size );
    }
    
    ~SocketStream()
    {
        free( m_buffer );
        close( m_socket );
    }
    
    void read( char* buffer, std::size_t size );
    
    std::size_t readLine( char* buffer, std::size_t size, char delim='\n' );
    
    std::size_t count()
    {
        return m_count;
    }
};

#endif
