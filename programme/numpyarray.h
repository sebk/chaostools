/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NUMPYARRAY_H
#define NUMPYARRAY_H

#include <fstream>
#include <list>
#include <boost/multi_array.hpp>
#include <string.h>
#include "typeinfo.h"

template <class Type, size_t N>
class NumPyArray {
public:
    typedef boost::multi_array<Type, N> array_t;
    typedef std::shared_ptr<array_t>    array_ptr;
    
    class npzEntry {
    public:
        bool operator>( const npzEntry& other ) const {
            return name > other.name;
        }
        bool operator<( const npzEntry& other ) const {
            return name < other.name;
        }
        npzEntry() {}
        npzEntry( const npzEntry& other ) {
            name = other.name;
            array = other.array;
        }
        
        std::string     name;
        array_ptr       array;
    };
    
    static array_ptr readNpy( std::istream& stream ) {
        TypeInfo<Type> type;
        bool isFortranOrder;
        char buffer[256];
        
        stream.read( buffer, 10 );
        if( stream.eof() )
            return array_ptr();
        
        if( strncmp( buffer, "\x93NUMPY" "\x01\x00", 6 + 2 ) )
            return array_ptr();
        
        size_t hLen = *((unsigned short*) (buffer + 8));
        
        stream.read( buffer + 10, hLen );
        
        std::string header( buffer + 10, hLen );
        assert( header.back() == '\n' );
       
        int loc1, loc2;

        //fortran order
        loc1 = header.find("fortran_order")+16;
        if( header.substr(loc1,5) == "True" )
            isFortranOrder = true;
        else if( header.substr(loc1,5) == "False" )
            isFortranOrder = false;
        else
            return array_ptr();
        
        //shape
        loc1 = header.find("(");
        loc2 = header.find(")");
        std::string str_shape = header.substr(loc1+1,loc2-loc1-1);
        
        int dimensions;
        if( str_shape[str_shape.size()-1] == ',' )
            dimensions = 1;
        else
            dimensions = std::count( str_shape.begin(), str_shape.end(),',' ) + 1;
        
        if( dimensions != N )
            return array_ptr();
        
        boost::array<unsigned long int, N> shape;
        
        for( size_t i = 0; i < N; i++ ) {
            loc1 = str_shape.find(",");
            shape[i] = atoi(str_shape.substr(0,loc1).c_str());
            str_shape = str_shape.substr(loc1+1);
        }

        //endian, word size, data type
        //byte order code | stands for not applicable. 
        //not sure when this applies except for byte array
        loc1 = header.find("descr")+9;
        bool littleEndian = (header[loc1] == '<' || header[loc1] == '|' ? true : false);
        assert(littleEndian);

        loc2 = header.find("'", loc1+1);
    
        std::string descr = header.substr( loc1+1, loc2 - loc1 - 1 );
        if( descr != type.numpyName() ) {
            std::cout << "data type is " << descr << " and not " << type.numpyName() << " as expected" << std::endl;
            return array_ptr();
        }
        
        size_t datasize = type.size();
        for( size_t n = 0; n < N; n++ )
            datasize *= shape[n];
        
        array_ptr ptr;
        if( isFortranOrder )
            ptr = std::make_shared<array_t>( shape, boost::fortran_storage_order() );
        else
            ptr = std::make_shared<array_t>( shape, boost::c_storage_order() );
        
        stream.read( (char*) ptr->data(), datasize );
        
        return ptr;
    }
    
    static std::vector<npzEntry> readNpz( std::istream& stream ) {
        std::vector<npzEntry> arrays;
        
        while( !stream.eof() ) {
            char local_header[30];
            stream.read( local_header, 30 );
            
            //if we've reached the global header, stop reading
            if(local_header[2] != 0x03 || local_header[3] != 0x04)
                break;

            //read in the variable name
            unsigned short name_len = *((unsigned short*) (local_header + 26));
            
            std::string filename(name_len,' ');
            stream.read( &filename[0], name_len );

            //read in the extra field
            unsigned short extra_field_len = *((unsigned short*) (local_header + 28));
            if( extra_field_len > 0 )
                stream.ignore( extra_field_len );
        
            
            npzEntry e;
            e.name = filename;
            e.array = readNpy( stream );
            arrays.push_back( e );
        }
        
        return arrays;
    }
    
    static void write( const array_t& array, std::ostream& file ) {
        TypeInfo<Type> type;
        
        std::string header = "{'descr': '";
        header += ENDIAN_STR;
        header += type.numpyName();
        header += "', 'fortran_order': False, 'shape': (";
        
        for( size_t n = 0; n < N; n++ ) {
            header += std::to_string( array.shape()[n] );
            header += ", ";
        }
        header += ")}";
        
        while( (6 + 4 + header.length() - 1) % 16 )
            header += " ";
        header += "\n";
        
        file.write( "\x93NUMPY" "\x01\x00", 6 + 2 );
        unsigned short hLen = header.length();
        file.write( (const char*) &hLen, 2 );
        
        size_t datasize = type.size();
        for( size_t n = 0; n < N; n++ )
            datasize *= array.shape()[n];
        
        file << header;
        file.write( (const char*) array.data(), datasize );
    }
};

#endif