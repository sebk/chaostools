/*
 * feigenbaum.cpp
 * Berechnet die Feigenbaumkonstante, in dem der Lyapunovexponent berechnet wird und dann
 * Stellen gesucht werden, an denen der Lyapunovexponent -unendlich ist.
 *  Created on: 13.03.2013
 *      Author: marlon
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/concept_check.hpp>
#include <vector>
#include "mpreal.h"
#include <future>

using namespace mpfr;

//Startwert für die Suche nach dem Lyapunovexponenten
mpreal x0;
mpreal roff;

/*
 * Gibt den Log(f'(x_i) aus.
 */
mpreal ablfunktion( const mpreal& xi, const mpreal& r ){
	//return log(fabs(r-2*r*xi));
        return log(fabs(r * (1- 2 * xi) ) );
}

//Funktionswert der Funktion
mpreal funktion( const mpreal& xi, const mpreal& r){
	return r*xi*(1-xi);
}

void lyapunov(mpreal* l, const mpreal* r, const mpreal& epsilon ){
	//Speichert den aktuellen Werte für die Summe über log(f')
	mpreal sum=ablfunktion(x0,*r);
	//der aktuelle Wert von x
	mpreal cx=funktion(x0,*r);
	//Speichert alle Werte für den Lyapunov Exponenten
	//std::vector<mpreal> lyvec(1000000);
        mpreal lastValue( 100 );
        long int i = 1;
	while( 1 ){
		sum += ablfunktion(cx,*r);
                i++;
		//lyvec[i] = sum/i;
		cx=funktion(cx,*r);
                if( (i % 1000) == 0 ) {
                    *l = sum / i;
                    //std::cout << v << std::endl;
                    if( (abs(lastValue - *l) / abs(lastValue)) < epsilon )
                        break;
                    lastValue = *l;
                }
	}
}

class FindMin
{
    mpreal epsilon;
    mpreal l_epsilon;
    mpreal l_values[ 5 ];
    mpreal r_delta;
    mpreal r_left;
    
    std::ofstream& stream;

    
    void calcall()
    {
        mpreal r_values[ 5 ];
        std::future<void> futures[ 5 ];
        
        for( int i = 0; i < 5; i++ ) {
            r_values[ i ] = r_left + i * r_delta;
            futures[ i ] = std::async( std::launch::async, lyapunov, &l_values[ i ], &r_values[ i ], l_epsilon );
        }
        
        for( int i = 0; i < 5; i++ ) {
            futures[ i ].get();
            stream << (r_values[ i ] - roff).toString(60) << " " << l_values[ i ] << std::endl;
            if( !isregular(l_values[ i ]) )
                throw;
        }
    }
    
    void calc13()
    {
        mpreal r_values[ 2 ];
        std::future<void> futures[ 2 ];
        
        for( int j = 0; j < 2; j++ ) {
            int i = i * 2 + 1;
            r_values[ j ] = r_left + i * r_delta;
            futures[ j ] = std::async( std::launch::async, lyapunov, &l_values[ i ], &r_values[ j ], l_epsilon );
        }
        
        for( int j = 0; j < 2; j++ ) {
            int i = i * 2 + 1;
            futures[ j ].get();
            stream << (r_values[ j ] - roff).toString(60) << " " << l_values[ i ] << std::endl;
            if( !isregular(l_values[ i ]) )
                throw;
        }
    }
    
    int min_l( mpreal& l_min )
    {
        int i_min = 5;
        for( int i = 0; i < 5; i++ ) {
            if( l_values[ i ] < l_min ) {
                l_min = l_values[ i ];
                i_min = i;
            }
        }
        
        if( i_min == 5 )
            throw;
        
        return i_min;
    }
    
public:
    FindMin( std::ofstream& s ) : stream( s )
    {
    }
    
    mpreal find( const mpreal& r0, const mpreal& r1, mpreal& l )
    {
        r_left = r0;
        r_delta = (r1 - r0) / 4;
        
        epsilon = 0.01;
        l_epsilon = epsilon / 10;
    
        calcall();
        
        while( 1 ) {
            l = 0;
            int i_min = min_l( l );
            std::cout << (r_left + i_min * r_delta).toString(60) << " " << l << std::endl;
            
            for( int i = 0; i < 5; i++ ) {
                stream << (r_left - roff + i * r_delta).toString(60) << " " << l_values[ i ] << std::endl;
            }
            
            if( r_delta < 1e-60 ) {
                l = l_values[ i_min ];
                return r_left + i_min * r_delta;
            }
            
            if( i_min == 0 )
                i_min = 1;
            else if( i_min == 4 )
                i_min = 3;
            
            mpreal l_delta_max = 0;
            for( int i = i_min - 1; i < i_min + 1; i++ ) {
                mpreal l_delta = fabs( l_values[i] - l_values[i+1] );
                if( l_delta > l_delta_max )
                    l_delta_max = l_delta;
            }
            
            if( l_delta_max < epsilon ) {
                epsilon /= 10;
                l_epsilon = epsilon / 10;
                std::cout << "increase precision" << std::endl;
                calcall();
                continue;
            }
            
            r_left += r_delta * (i_min - 1);
            r_delta /= 2;
            
            /*
                [ 0   1   2   3   4 ]
            1:  [ 0 1 2 3 4 ]
                0 -> 0
                1 -> 2
                2 -> 4
            
                [ 0   1   2   3   4 ]
            2:       [0 1 2 3 4]
                1 -> 0
                2 -> 2
                3 -> 4
            
                [ 0   1   2   3   4 ]
            3:           [0 1 2 3 4]
                2 -> 0
                3 -> 2
                4 -> 4
                
            */
            
            int indexmap[3][2][2] = {
               // src   dest
               // case 1
               {
                { 2,    4 },
                { 1,    2 }
               },
               // case 2
               {
                { 1,    0 },
                { 3,    4 }
               },
               // case 3
               {
                { 2,    0 },
                { 3,    2 }
               }
            };
            
            for( int i = 0; i < 2; i++ ) {
                l_values[ indexmap[ i_min - 1 ][i][1] ] = l_values[ indexmap[ i_min - 1 ][i][0] ];
            }
            
            calc13();
        }
        
    }

};


void find( mpreal left, mpreal delta ) {
    std::ofstream log("/tmp/log.dat");
    std::ofstream peaks("/tmp/peak.dat");
    std::ofstream search("/tmp/search.dat");
    
    FindMin finder( search );
    
    mpreal epsilon = 0.00001;
    
    while( 1 ) {
        mpreal r = left;
        mpreal last_l;
        lyapunov( &last_l, &left, epsilon );
        mpreal last_rfound = left;
        mpreal l;
        int i = 0;
        bool falling = true;
        for( ;; i++, last_l = l ) {
            r += delta;
            lyapunov( &l, &r, epsilon );
            log << (r - roff) << " " << l << std::endl;
            if( l > 0.0 )
                return;
            
            if( falling ) {
                if( l > last_l ) {
                    // steigt wieder
                    mpreal lfound;
                    mpreal rfound = finder.find( r - 2 * delta, r, lfound );
                    if( !isregular(rfound) )
                        continue;
                    
                    peaks << (rfound - roff).toString(60) << " " << lfound << std::endl;
                    std::cout << "minimum bei: " << rfound.toString(20) << " (" << lfound << ")" << std::endl;
                    falling = false;
                    
                    delta = (rfound - last_rfound) / 20;
                    std::cout << "i=" << i << ", delta: " << delta << std::endl;
                    last_rfound = rfound;
                }
            } else {
                if( l < last_l ) {
                    // maximum erreicht
                    left = r;
                    delta /= 4;
                    break;
                }
            }
        }
        
    }
}

int main(){
        mpreal::set_default_prec( 400 );
        x0 = 0.121;
        roff = mpreal("3.56995");
        find( mpreal("3.5699433"), mpreal("0.0000002") );
	return 0;
}


