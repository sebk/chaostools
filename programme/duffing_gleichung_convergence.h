/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_DUFFING_GL_CONV
#endif

#ifdef USE_DUFFING_GL_CONV
#define MAIN_CLASS DuffingGleichungConvergence

#include "canvas.h"
#include "instance.h"
#include "pool.h"
#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/integrate/integrate_times.hpp>
#include <mutex>
#include <condition_variable>
#include "configreader.h"
#include "duffing.h"
#include "statetools.h"

class DuffingGleichungConvergence : public Instance {
    
public:
    typedef double real;
    typedef Canvas<double, float>       canvas_t;
    typedef boost::array<real, 2>       state_t;
    typedef TrajectoryList<real, 2>     TL_t;
    
    class Shared : public Instance::Shared {
        friend class DuffingGleichungConvergence;
        
        TL_t                    trajectories;
        std::string             indexFile;
        std::mutex              mutex;
        std::condition_variable condition;
        int                     waiting;
        int                     threads;
        canvas_t                canvas;
        canvas_t                canvas2;
        
    public:
        Shared( ConfigReader& config, int threadCount ) :
            Instance::Shared( config, threadCount ),
            trajectories( config.section_get<real>("duffing", "e_same") ),
            canvas( config, 0 ),
            canvas2( config, 0, "canvas2" )
        {
            if( config.section_get<int>("duffing", "plot") < 0 && !config.section_get<bool>("duffing", "tracking") )
                indexFile = config.section_get<std::string>("duffing", "indexfile");
            
            waiting = 0;
            threads = threadCount;
        }
        
        void sync()
        {
            std::unique_lock<std::mutex> lock( mutex );
            waiting++;
            
            if( waiting < threads ) {
                while( waiting && waiting < threads )
                    condition.wait( lock );
            } else {
                waiting = 0;
                condition.notify_all();
            }
        }
        
        ~Shared()
        {
            if( indexFile.size() ) {
                std::ofstream os( indexFile );
                trajectories.save( os );
            }
        }
    };
    
protected:
    real* getParam( const std::string& name )
    {
        if( name == "epsilon" )
            return &m_duffing.epsilon;
        else if( name == "lambda" )
            return &m_duffing.lambda;
        else if( name == "alpha" )
            return &m_duffing.alpha;
        else if( name == "beta" )
            return &m_duffing.beta;
        else if( name == "x0" )
            return &m_initstate[ 0 ];
        else if( name == "y0" )
            return &m_initstate[ 1 ];
        else
            throw std::runtime_error("param invalid");
    }
    
public:
    DuffingGleichungConvergence( ConfigReader& config ):
        Instance(),
        m_canvas3( config, 0, "canvas3" )
    {
        config.setSection("duffing");
        m_duffing.init( config );
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        
        m_initstate[ 0 ] = config.get<real>("x0");
        m_initstate[ 1 ] = config.get<real>("y0");
        
        m_param_x = getParam( config.get<std::string>("param_x") );
        m_param_y = getParam( config.get<std::string>("param_y") );
        
        m_extraLoops = config.get<uint>("extraloops", 0);
        
        m_phase = config.get<real>("phase", 0.0);
        m_epsilonStop = config.get<real>("e_stop");
        
        m_plotParam = config.get<int>("plot", 0);
        if( m_plotParam > 1 )
            m_plotParam = 1;
        else
            m_tracking = config.get<bool>("tracking");
    }
    
    virtual void run()
    {
        //boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real> stepper;
        boost::numeric::odeint::controlled_runge_kutta<boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real>> stepper;
        
        auto callback = std::bind(
            &DuffingGleichungConvergence::addLine, this, std::placeholders::_1, std::placeholders::_2 );
        
        Shared* s = static_cast<Shared*>( m_shared );
        int width = s->canvas.width();
        int height = s->canvas.height();
        
        TL_t* trajectories = &(s->trajectories);
        
        for( int px = 0; px < width; px++) {
            real x = s->canvas.xValue( px );
            for( int py = m_thread; py < height; py += m_threadCount ) {
                real y = s->canvas.yValue( py );
                
                *m_param_x = x;
                *m_param_y = y;
                
                state_t state = m_initstate;
                m_canvas3.moveTo( state[ 0 ], state[ 1 ] );
                
                real increment = 2.0 * M_PI / m_duffing.Omega;
                real t_0 = m_phase / m_duffing.Omega;
                real t_1 = t_0 + increment;
                int stepCount = (m_t1 - m_t0) / increment;
                
                ValueTester<real, 16, 2> tester( m_epsilonStop );
                
                int step = 0;
                int periodicity = 0;
                for( ; step < stepCount && !periodicity; step++ ) {
                    boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt, callback );
                    
                    periodicity = tester.test( state );
                }
                
                for( uint l = 0; l < m_extraLoops; l++ )
                    boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt, callback );
                
                if( periodicity ) {
                    s->canvas.addXY( px, py, step );
                    
                    if( m_plotParam < 0 ) {
                        static_cast<Shared*>( m_shared )->mutex.lock();
                        int index = trajectories->index( state );
                        if( index < 0 || m_tracking ) {
                            TL_t::trajectory_t t( periodicity );
                            t[ 0 ] = state;
                            
                            for( int j = 1; j < periodicity; j++ ) {
                                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt );
                                t[ j ] = state;
                            }
                            
                            if( index < 0 ) {
                                index = trajectories->add( t );
                                std::cout << index << " " << state[ 0 ] << " " << state[ 1 ] << std::endl;
                            }
                            else if( m_tracking )
                                trajectories->update( index, t );
                        }
                        
                        static_cast<Shared*>( m_shared )->mutex.unlock();
                        
                        s->canvas2.addXY( px, py, index + 1 );
                    } else
                        s->canvas2.addXY( px, py, state[ m_plotParam ] );
                    
                    s->canvas.addXY( px, py, step );
                } else
                    s->canvas.addXY( px, py, stepCount );
                
                setProgress( px * height + py, width * height );
            }
            
            if( m_tracking < 0 )
                static_cast<Shared*>( m_shared )->sync();
        }
        
        setProgress( 1.0 );
    }
    
    
    void addLine( state_t& state, real t ) {
        m_canvas3.lineTo( state[0], state[1], 1 );
    }
    
    void collect( DuffingGleichungConvergence& other ) {
        m_canvas3 += other.m_canvas3;
    }
    
    void finish( ConfigReader& config ) {
        Shared* s = static_cast<Shared*>( m_shared );
        s->canvas.write();
        s->canvas2.write();
        m_canvas3.write();
    }
    
private:
    DuffingGl<real>     m_duffing;
    real                m_t0;
    real                m_t1;
    real                m_dt;
    canvas_t            m_canvas3;
    real                m_phase;
    real                m_epsilonStop;
    real                m_epsilonSame;
    state_t             m_initstate;
    real*               m_param_x;
    real*               m_param_y;
    uint                m_extraLoops;
    int                 m_plotParam;
    bool                m_tracking;
};

#endif