/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_NPY_PHASESPACE
#endif

#ifdef USE_NPY_PHASESPACE
#define MAIN_CLASS NpyPhaseSpace

#define CANVAS_ENABLE_RESIZE 1
#include "canvas.h"
#include "npy_processer.h"

class NpyPhaseSpace: public NpyProcessor {
protected:
    std::string                         m_data;
    Canvas<double, float>*              m_canvas;
    int                                 m_voltageChannel;
    
    int                                 m_count;
    double                              m_voltageSum;
    double                              m_voltageOffsetSum;
    
    virtual void processArray( npArray_t::array_ptr array )
    {
        array_t::const_multi_array_ref ref( *array );
     
        if( !m_canvas ) {
            m_canvas = createCanvasForArray( ref );
            m_canvas->enableResize();
        }
        
        std::vector<double> slopes;
        Analyze<double>::findTriggerSlopes( slopes, ref, m_triggerChannel, m_triggerThreshold );
        
        std::size_t n0 = slopes.front();
        std::size_t n1 = slopes.back();
        
        if( n1 <= n0 )
            return;
        
        // first pass: find avg voltage
        double avgVoltage = 0.0;
        for( size_t n = n0; n < n1; n++ ) {
            avgVoltage += ref[n][m_voltageChannel];
        }
        
        avgVoltage /= (n1 - n0);
        
        double voltageSquared = 0.0;
        for( size_t n = n0; n < n1; n++ ) {
            double x = ref[n][m_XChannel];
            double y = ref[n][m_YChannel];
            
            m_canvas->add( x, y, 1.0 );
            
            double v = ref[n][m_voltageChannel] - avgVoltage;
            voltageSquared += v * v;
        }
        
        m_voltageSum += voltageSquared / (n1 - n0);
        m_voltageOffsetSum += avgVoltage;
        m_count += 1;
    }
    
    
public:
    NpyPhaseSpace( ConfigReader& config ): NpyProcessor( config )
    {
        config.setSection( "phasespace" );
        m_data = config.get<std::string>("data");
        m_voltageChannel = config.get<int>("voltage", -1);
        m_canvas = NULL;
        m_count = 0;
        m_voltageSum = 0.0;
        m_voltageOffsetSum = 0.0;
    }
    
    virtual ~NpyPhaseSpace() {
        if( m_canvas )
            delete m_canvas;
    }
    
    virtual void run()
    {
        addFilename( m_data );
    }
    
    void finish( ConfigReader& config )
    {
        std::ofstream s( config.get<std::string>("canvasinfo") );
        if( m_canvas ) {
            config.setSection("default");
            m_canvas->clip();
            m_canvas->write();
            
            config.setSection( "phasespace" );
            m_canvas->writeInfo( s );
        }
        
        s << std::endl << "[measure]" << std::endl;
        s << "  rms = " << sqrt( m_voltageSum / m_count ) << std::endl;
        s << "  avg = " << (m_voltageOffsetSum / m_count) << std::endl;
    }
};

#endif
