/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATAPROCESSOR_H
#define DATAPROCESSOR_H

#include <boost/asio/ip/tcp.hpp>
#include <QThread>
#include "configreader.h"
#include "linearstorage.h"
#include "canvas_qwt_raster_data.h"
#include "socketstream.h"


class DataProcesser : public QThread
{
    Q_OBJECT
    
    typedef boost::multi_array<std::size_t, 2>  shape_t;
    typedef boost::multi_array<double, 2>       array_t;
    typedef array_t::const_multi_array_ref      array_ref;
    
    int                                         m_socket;
    
    NumPyArray<double, 2>                       m_npy;
    
    CanvasQwtRasterData*                        m_canvas;
    
    int                                         m_sourceChannel;
    int                                         m_XChannel;
    int                                         m_YChannel;
    int                                         m_triggerChannel;
    double                                      m_phaseshift;
    double                                      m_threshold;
    double                                      m_sourceMin;
    double                                      m_sourceMax;
    int                                         m_width;
    int                                         m_oversample;
    
    bool                                        m_running;
    
    std::string                                 m_datafile;
    std::string                                 m_filename;
    
    LinearStorage*                              m_storage;
    
    double                                      m_lastVoltage;
    bool                                        m_clip_canvas;
    
public:
    explicit DataProcesser( QObject* parent = 0 ) :
        QThread( parent )
    {
        m_canvas = NULL;
        m_storage = NULL;
        m_running = false;
    }
    
    virtual ~DataProcesser() {
        if( m_storage )
            delete m_storage;
        
        std::cout << "DataProcesser terminated" << std::endl;
    }
    
    void save()
    {
        if( m_canvas ) {
            if( m_clip_canvas )
                m_canvas->clip();
            
            m_canvas->writeInfo( m_filename );
            m_canvas->write( m_datafile );
        }
        
        if( m_storage )
            m_storage->flush();
    }
    
    QwtRasterData* init( ConfigReader& config );
    
    virtual void run();
    
    void handler( SocketStream& s );
    
public slots:
    void stop()
    {
        m_running = false;
        close( m_socket );
        m_storage->flush();
    }
    
signals:
    void dataChanged( /*QwtRasterData* data*/ double );
    
protected:
    CanvasQwtRasterData* createCanvas( array_ref& array, std::size_t channel );
    
    void callback( LinearStorage::node_ptr<LinearStorage::nodeHead_t> head );
    
    void processInt16Array2d( const LinearStorage::node_ptr< LinearStorage::int16Array2d_t >& intArray,
                              LinearStorage::node_ptr< LinearStorage::intArrayInfo_t >& info );
    void processInt16Array2d( const LinearStorage::node_ptr< LinearStorage::int16Array2d_t >& intArray )
    {
        LinearStorage::node_ptr<LinearStorage::intArrayInfo_t> info;
        processInt16Array2d( intArray, info );
    }
    void processFloatArray2d( const LinearStorage::node_ptr< LinearStorage::floatArray2d_t >& floatArray );
    void processArray( array_t& array );
    
    void feigenbaum( array_ref& array );
    
    void update( array_ref& array );
};

#endif
