/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "linearstorage.h"
#include <stdexcept>
#include <string.h>
#include <algorithm>

void LinearStorage::IndexCache::flush() {
    if( m_dirtyInodes.size() ) {
        m_dirtyInodes.sort();
        
        for( std::list<InodeEntry>::const_iterator it = m_dirtyInodes.begin(); it != m_dirtyInodes.end(); it++ ) {
            m_file.write<indexNode_t>( &it->inode, it->position );
        }
        
        m_readCache.splice( m_readCache.begin(), m_dirtyInodes, m_dirtyInodes.begin(), --m_dirtyInodes.end() );
        
        std::cout << "IndexCache flushed" << std::endl;
    }
}

void LinearStorage::IndexCache::flushArea( LinearStorage::pos_t lower, LinearStorage::pos_t upper )
{
    std::list<InodeEntry>::iterator it_next = m_dirtyInodes.begin();
    for( std::list<InodeEntry>::iterator it = it_next; it != m_dirtyInodes.end(); it = it_next ) {
        it_next++;
        if( it->position >= lower && (it->position + it->inode.head.size) <= upper ) {
            m_file.write<indexNode_t>( &it->inode, it->position );
            
            m_readCache.splice( m_readCache.begin(), m_dirtyInodes, it );
        }
    }
}

const LinearStorage::indexNode_t* LinearStorage::IndexCache::read( pos_t position )
{
    // look in dirty nodes
    for( InodeEntry& e : m_dirtyInodes ) {
        if( e.position == position ) {
            m_read_hits_writecache++;
            return &e.inode;
        }
    }

    // look in the cache
    std::list<InodeEntry>::iterator it;
    for( InodeEntry& e : m_dirtyInodes ) {
        if( e.position == position ) {
            m_read_hits_readcache++;
            break;
        }
    }

    if( it == m_readCache.end() ) {
        if( m_readCache.size() >= maxReadCacheSize )
            m_readCache.splice( m_readCache.begin(), m_readCache, --m_readCache.end() );
        else {
            m_readCache.emplace_front();
        }
        it = m_readCache.begin();
        it->position = position;
        m_file.read<indexNode_t>( &it->inode, position );
        m_read_misses++;
    } else if( it != m_readCache.begin() )
        m_readCache.splice( m_readCache.begin(), m_readCache, it );

    return &it->inode;
}

void LinearStorage::IndexCache::write( const indexNode_t* node, pos_t position )
{
    // look in dirty nodes
    for( InodeEntry& e : m_dirtyInodes ) {
        if( e.position == position ) {
            assert( &e.inode != node );
            // memcpy( &e.inode, node, sizeof( indexNode_t ) );
            e.inode = *node;
            m_read_hits_writecache++;
            return;
        }
    }

    // look in cache
    std::list<InodeEntry>::iterator it;
    for( it = m_readCache.begin(); it != m_readCache.end(); it++ ) {
        if( it->position == position ) {
            it->inode = *node;
            m_dirtyInodes.splice( m_dirtyInodes.end(), m_readCache, it );

            if( m_dirtyInodes.size() >= maxDirtyInodes )
                flush();
            
            m_write_hits_readcache++;
            return;
        }
    }

    // place in dirtyInodes
    if( m_readCache.size() > maxReadCacheSize ) {
        m_dirtyInodes.splice( m_dirtyInodes.begin(), m_readCache, --m_readCache.end() );
        std::list<InodeEntry>::iterator it = m_dirtyInodes.begin();
        it->position = position;
        it->inode = *node;
    } else
        m_dirtyInodes.emplace_back<pos_t&, const indexNode_t&>( position, *node );
    
    if( m_dirtyInodes.size() >= maxDirtyInodes )
        flush();
    
    m_write_misses++;
}

LinearStorage::LinearStorage( const char* filename, bool overwrite ) :
    m_file( filename, overwrite ), m_cache( m_file )
{
    for( int i = 0; i < numNodeTypes; i++ )
        m_callbacks[i] = nullptr;
    
    if( m_file.size() ) {
        loadFile();
    } else {
        initFile();
    }
}

void LinearStorage::initFile()
{   
    // that way the root inode gets index 0
    m_nodeCount = -1;
    m_endpos = 0;
    
    infoNode_t info;
    newNode( (nodeHead_t*) &info, InfoNode, sizeof(infoNode_t) );
    strncpy( info.name, infostring, sizeof(info.name) );
    info.version = version_number;
    m_file.write<infoNode_t>( &info, 0 );
    
    // create index structure
    createIndex( INDEX_DEPTH );
    m_cache.flush();
}

void LinearStorage::loadFile()
{
    m_nodeCount = 0;
    m_endpos = m_file.size();
    
    infoNode_t infoNode;
    m_file.read<infoNode_t>( &infoNode, 0 );
    
    if( infoNode.head.type != InfoNode )
        throw std::invalid_argument("invalid header: wrong node type");
    
    if( infoNode.head.index != -1 )
        throw std::invalid_argument("invalid header: wrong node index");
    
    if( infoNode.head.size != sizeof( infoNode_t ) )
        throw std::invalid_argument("invalid header: wrong node size");
    
    if( strncmp( infoNode.name, infostring, sizeof(infoNode.name) ) )
        throw std::invalid_argument("invalid header: wrong infostring");
    
    if( infoNode.version != version_number )
        throw std::invalid_argument("invalid header: wrong version");
    
    pos_t nextPosition = sizeof(infoNode_t);
    for( int level = INDEX_DEPTH; level--; ) {
        const indexNode_t* inode = m_cache.read( nextPosition );
        assert( inode->head.type == IndexNode );
        assert( inode->head.size == sizeof(indexNode_t) );
        
        bool found = false;
        for( int i = INDEX_NUMS; i--; ) {
            pos_t p = inode->nodes[ i ].position;
            if( p >= 0 ) {
                m_nodeCount |= i << (INDEX_BITS * level);
                nextPosition = p;
                found = true;
                break;
            }
        }
        
        if( !found ) {
            throw std::invalid_argument("last index node empty");
        }
    }
    
    // load last node
    pos_t pos = findNode( m_nodeCount );
    assert( pos >= 0 );
    
    nodeHead_t head;
    m_file.readHead( &head, pos );
    
    assert( head.index == m_nodeCount );
    
    if( pos + head.size != m_endpos )
        throw std::invalid_argument("file size mismatch");
    
    std::cout << m_nodeCount << " nodes found" << std::endl;
    
    m_nodeCount++;
}

LinearStorage::pos_t LinearStorage::findNode( index_t index )
{
    pos_t nextPosition = sizeof(infoNode_t);

    for( int level = INDEX_DEPTH; level--; ) {
        const indexNode_t* inode = m_cache.read( nextPosition );
        assert( inode->head.type == IndexNode );
        assert( inode->head.size == sizeof(indexNode_t) );

        index_t i = (index >> (INDEX_BITS * level)) & INDEX_MASK;

        nextPosition = inode->nodes[ i ].position;
        if( nextPosition < 0 )
            return -1; // not found
    }

    return nextPosition;
}

LinearStorage::pos_t LinearStorage::createIndex( int depth )
{
    pos_t firstNewNodePos = m_endpos;
    for( int n = depth; n--; ) {
        // create a new inode
        indexNode_t inode;
        pos_t inode_pos = newNode( (nodeHead_t*) &inode, IndexNode, sizeof( indexNode_t ) );
        
        for( int i = 0; i < INDEX_NUMS; i++ )
            inode.nodes[i].position = -1;
        
        if( n )
            inode.nodes[0].position = inode_pos + sizeof( indexNode_t );
        else {
            for( int i = 0; i < depth; i++ )
                inode.nodes[i].position = firstNewNodePos + sizeof( indexNode_t ) * i;
        }
        
        m_cache.write( &inode, inode_pos );
    }
    
    return firstNewNodePos;
}


void LinearStorage::createLink()
{
    pos_t nextPosition = sizeof(infoNode_t);
    
    for( int level = INDEX_DEPTH; level--; ) {
        const indexNode_t* inode = m_cache.read( nextPosition );
        assert( inode->head.type == IndexNode );
        assert( inode->head.size == sizeof(indexNode_t) );
        
        index_t p = (m_nodeCount >> (INDEX_BITS * level)) & INDEX_MASK;
        
        pos_t entryPos = inode->nodes[ p ].position;
        
        if( entryPos < 0 ) {
            indexNode_t writeableInodeCopy = *inode;
            
            if( level > 0 ) {
                writeableInodeCopy.nodes[ p ].position = entryPos = createIndex( level );
            }
            else {
                writeableInodeCopy.nodes[ p ].position = m_endpos;
            }
            
            m_cache.write( &writeableInodeCopy, nextPosition );
        } else if( level == 0 )
            throw std::runtime_error("index already set");
        
        nextPosition = entryPos;
    }
}



