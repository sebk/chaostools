/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_ELEKTR_OSZILL_PHASE
#endif

#ifdef USE_ELEKTR_OSZILL_PHASE
#define MAIN_CLASS ElektrOszillPhaseportrait

#include "instance.h"
#include "pool.h"
#include <functional>
#include "configreader.h"
#include "elektronischer-oszillator-system.h"
#include <iostream>
#include <list>

class ElektrOszillPhaseportrait : public Instance {
public:
    typedef double real;
    typedef ElektrOszillSystem<real>    system_t;
    typedef struct {
        system_t::state_t x;
        system_t::state_t dx;
    } point_t;
    
    typedef struct {
        real                    time;
        std::vector<point_t>    points;
    } entry_t;
    
    ElektrOszillPhaseportrait( ConfigReader& config ):
        Instance()
    {
        config.setSection("elektr-oszill");
        m_system.init( config );
        
        m_width = config.get<int>("width");
        m_height = config.get<int>("height");
        m_I_0 = config.get<real>("I_0");
        m_I_1 = config.get<real>("I_1");
        m_V_d_0 = config.get<real>("V_d_0");
        m_V_d_1 = config.get<real>("V_d_1");
        m_steps = config.get<int>("steps");
    }
    
    virtual void run() {
        real t_a = (2.0 * M_PI / m_system.omega / m_threadCount) * m_thread;
        real t_b = 2.0 * M_PI / m_system.omega / (m_steps * m_threadCount);
        real p0_b = (m_V_d_1 - m_V_d_0) / m_width;
        real p1_b = (m_I_1 - m_I_0) / m_height;
        
        for( int n = m_thread; n < m_steps; n ++ ) { 
            real t = t_a + n * t_b;
            
            m_data.push_back( entry_t() );
            entry_t& e = m_data.back();
            
            e.time = t;
            e.points.reserve( m_steps * m_width * m_height );
            
            for( int p0 = 0; p0 < m_width; p0++ ) {
                for( int p1 = 0; p1 < m_height; p1++ ) {
                    point_t p;
                    p.x[0] = m_V_d_0 + p0 * p0_b;
                    p.x[1] = m_I_0 + p1 * p1_b;
                    
                    m_system( p.x, p.dx, t );
                    e.points.push_back( p );
                }
            }
        }

        setProgress( 1.0 );
    }
    
    void collect( ElektrOszillPhaseportrait& other ) {
        m_data.splice( m_data.end(), other.m_data );
    }
    
    void finish( ConfigReader& config ) {
        config.setSection( "default" );
        std::ofstream f( config.get<std::string>( "datafile" ) );
        for( auto e_it = m_data.begin(); e_it != m_data.end(); e_it++ ) {
            const entry_t& e = *e_it;
            f << "# t = " << e.time << std::endl;
            for( auto p_it = e.points.begin(); p_it != e.points.end(); p_it++ ) {
                const point_t& p = *p_it;
                
                for( int i = 0; i < p.x.static_size; i++ )
                    f << p.x[ i ] << ' ';
                
                for( int i = 0; i < p.dx.static_size; i++ )
                    f << p.dx[ i ] << ' ';
                
                f << std::endl;
            }
        }
    }
    
protected:
    system_t    m_system;
    int         m_width;
    int         m_height;
    int         m_steps;
    real        m_I_0;
    real        m_I_1;
    real        m_V_d_0;
    real        m_V_d_1;
    
    std::list<entry_t> m_data;
};

#endif