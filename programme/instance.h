/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INSTANCE_H
#define INSTANCE_H

#include "configreader.h"

class Instance {
public:
    class Shared {
    public:
        Shared( ConfigReader& config, int threadCount ) {}
    };
    
    // no thread limit
    static const uint maxThreads = 0;
    
private:
    float m_progress;
    bool  m_running;
    
    
protected:
    bool        m_stop;
    int         m_thread;
    int         m_threadCount;
    Shared*     m_shared;
    virtual void run() = 0;
    
public:
    void setProgress( float p ) {
        m_progress = p;
    }
    
    void setProgress( int nom, int denom ) {
        m_progress = (float) nom / (float) denom;
    }
    
    static void start( int thread, int threadCount, Instance* i, Shared* s ) {
        i->m_thread = thread;
        i->m_threadCount = threadCount;
        i->m_shared = s;
        
        i->m_running = true;
        i->m_stop = false;
        i->run();
        i->m_running = false;
    }
    
    void stop() {
        m_stop = true;
    }
    
    float progress() const {
        return m_progress;
    }
    
    bool running() const {
        return m_running;
    }
    
    void finish( ConfigReader& ) {
    }
    
    void collect( Instance& other ) {   
    }
    
};

#endif