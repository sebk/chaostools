/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIGREADER_H
#define CONFIGREADER_H

#include <istream>
#include <fstream>
#include <iostream>
#include <array>
#include <vector>
#include <system_error>
#include <boost/property_tree/ptree.hpp>
#include <boost/concept_check.hpp>

class ConfigReader
{
    typedef boost::property_tree::ptree         ptree;
    typedef std::string                         string;
    
public:
    class ParseError: public std::exception
    {
        const char* message;
        const char* filename;
        int line;
        
    public:
        ParseError( const char* msg, const char* file, int line ) : message( msg ), filename( file ), line( line ) {}
        
        const char* what() const throw();
    };
    
    class MissingEntry : public std::exception
    {
        string section;
        string entry;
        
      public:
        MissingEntry( string section, string entry ) : section(section), entry(entry) {}
        
        virtual const char* what() const throw();
        
        virtual ~MissingEntry() throw() {};
    };
    
    class ConvertError : public std::exception
    {
        string section;
        string entry;
        string value;
        const std::type_info& type;
    
        typedef struct typeInfo {
            const size_t        hash;
            const char*         name;
        } typeEntry_t;
        
      public:
        ConvertError( std::string section, std::string entry, std::string value, const std::type_info& type ) :
            section( section ),
            entry( entry ),
            value( value ),
            type( type )
        {}
        virtual ~ConvertError() throw() {};
        
        const char* what() const throw();
    };
    
private:
    std::locale         loc;
    ptree               tree;
    ptree*              currentSection;
    ptree*              defaultSection;
    string              sectionName;
    
    bool                allowDublicateSections;
    bool                allowDublicateKeys;
    
    // merge b to a
    void merge( ptree& dest, const ptree& source );
    
    bool isspace( char c ) const {
        return std::isspace( c, loc );
    }
    
    string strip( string::const_iterator first, string::const_iterator end ) const {
        while( first != end && isspace( *first ) )
            ++first;
        
        if( first == end )
            return string();
        
        string::const_iterator last = end;
        do
            --last;
        while( last != first && isspace( *last ) );
        
        return string(first, last + 1);
    }
    
    string strip( const string& s ) const {
        if( !( isspace( s.front() ) || isspace( s.back() ) ) )
            return s;
        
        string::const_iterator first = s.begin();
        string::const_iterator end = s.end();
        
        return strip( first, end );
    }
    
    string strip( const string& s, string::size_type begin, string::size_type end ) const {
        string::const_iterator first = s.begin();
        string::const_iterator last = s.end();
        
        while( begin && first != last ) {
            first++;
            begin--;
        }
        
        while( end < s.length() && last != first ) {
            last--;
            end++;
        }
        
        if( first == last )
            return string();
        
        return strip( first, last );
    }
    
    std::vector<std::string> split( const std::string &s ) const {
        std::vector<std::string> elems;
        string::const_iterator start = s.begin();
        string::const_iterator it = s.begin();
        
        while( it != s.end() ) {
            // skip whitespaces
            for( ; it != s.end() && isspace( *it ); it++ );
            start = it;
            
            // find non-whitespaces
            for( ; it != s.end() && !isspace( *it ); it++ );
            
            if( it != start ) {
                elems.push_back( string( start, it ) );
            }
        }
        
        return elems;
    }
    
    void execute( std::vector<std::string> args, const char* filename, int line );
    
    void createAlias( const std::string& alias, const std::string& ref, const char* filename, int line )
    {
        ptree& entry = tree.get_child( ref );
        tree.put_child( alias, entry );
    }
    
    void dump( ptree* t, int level ) const;
    
    string parse( const string& s );
    
    template <class Type>
    Type getEntry( ptree* section, string s ) {
        try {
            const string& orig = section->get_child( s ).data();
            string result = parse( orig );
            
            boost::property_tree::stream_translator<char, string::traits_type, string::allocator_type, Type> translator( loc );
            boost::optional<Type> value = translator.get_value( result );
            if( !value )
                throw ConvertError( sectionName, s, result, typeid(Type) );
            
            return *value;
        } catch( boost::property_tree::ptree_bad_path ) {
            throw MissingEntry( sectionName, s );
        }
    }
    
    template <class Type>
    Type getEntryDefault( ptree* section, string s, Type defaultValue ) {
        try {
            const string& orig = currentSection->get_child( s ).data();
            string result = parse( orig );
            
            boost::property_tree::stream_translator<char, string::traits_type, string::allocator_type, Type> translator( loc );
            boost::optional<Type> value = translator.get_value( result );
            if( !value )
                throw ConvertError( sectionName, s, result, typeid(Type) );
            
            return *value;
        } catch( boost::property_tree::ptree_bad_path ) {
            return defaultValue;
        }
    }
    
    template <class Type>
    std::vector<Type> getEntryVector( ptree* section, string s ) {
        const string* orig;
        try {
            orig = &(section->get_child( s ).data());
        } catch( boost::property_tree::ptree_bad_path& e ) {
            throw MissingEntry( sectionName, s );
        }
        
        std::vector<string> parts = split( parse( *orig ) );
        
        std::vector<Type> result( parts.size() );
        for( int i = 0; i < parts.size(); i++ ) {
            boost::property_tree::stream_translator<char, string::traits_type, string::allocator_type, Type> translator( loc );
            boost::optional<Type> value = translator.get_value( parts[ i ] );
            if( !value )
                throw ConvertError( sectionName, s, parts[ i ], typeid(Type) );
                
            result[ i ] = *value;
        }
        
        return result;
    }
    
public:
    ConfigReader( ) : loc( "C" ) {
        allowDublicateSections = true;
        allowDublicateKeys = true;
        
        defaultSection = &tree.push_back( std::make_pair( "default", ptree() ) )->second;
        currentSection = defaultSection;
    }
    
    void dump() {
        dump( &tree, 0 );
    }
    
    // parse arguments passed by operating system
    int sysargs( int argc, char** argv, int& loglevel );
    
    // calls sysargs but exits at return code != 0
    void args( int argc, char** argv, int& loglevel )
    {
        int r = sysargs( argc, argv, loglevel );
        if( r )
            exit( r );
    }
    
    void parseStream( std::istream& stream, const char* filename = "" );
    
    void addFile( string file ) {
        std::ifstream s( file, std::ifstream::in );
        if( !s.is_open() )
            throw std::system_error( errno, std::system_category(), file );
        
        parseStream( s, file.c_str() );
    }
    
    void setOption( string option, string value );
    
    void setSection( string s ) {   
        currentSection = &tree.get_child( s );
        sectionName = s;
    }
    
    template <class Type>
    Type get( string s ) {
        return getEntry<Type>( currentSection, s );
    }
    
    template <class Type>
    Type get( string s, Type v ) {
        return getEntryDefault<Type>( currentSection, s, v );
    }
    
    template <class Type>
    Type default_get( string s ) {
        return getEntry<Type>( defaultSection, s );
    }
    
    template <class Type>
    Type default_get( string s, Type v ) {
        return getEntryDefault<Type>( defaultSection, s, v );
    }
    
    template <class Type>
    Type section_get( string section, string key ) {
        return getEntry<Type>( &tree.get_child( section ), key );
    }
    
    template <class Type>
    Type section_get( string section, string key, Type v ) {
        return getEntryDefault<Type>( &tree.get_child( section ), key, v );
    }
    
    template <class Type>
    std::vector<Type> getVector( string s ) {
        return getEntryVector<Type>( currentSection, s );
    }
    
    template <class Type>
    std::vector<Type> section_getVector( string section, string s ) {
        return getEntryVector<Type>( &tree.get_child( section ), s );
    }
};

#endif
