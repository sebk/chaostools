#ifndef PARAMETERSPACE_H
#define PARAMETERSPACE_H

#include <boost/array.hpp>
#include <boost/concept_check.hpp>
#include <array>
#include <math.h>

/*!
 * \param       BS_min          minimale blockgröße
 * \param       BS_max          maximale "
 * \param       N               zahl der prozesse
 * \param       Dim             Dimensionen
 * 
 * \section     Ablauf
 * 1.   gröbstes raster wählen:
 *              BS = div^Dim
 *              log2(BS) = Dim * log2(div)
 *              log2(div) = log2(BS) / Dim
 *              div = 2^(log2( BS_max ) / Dim)
 * 1.   eine sehr grobe übersicht rendern
 * 2.   verbleibende berechnungen: K
 * 2.   blockgröße berechnen: BS = K / 2 N
 *                                                                                              level   pos
 * x.......x.......x.......x.......................................  1/64               0       3       0
 * ................................x.......x.......x.......x.......                             3       4
 * 
 * ................................x...............................  1/64       2       32+64i  5
 * ................x...............................x...............  1/32       2       16+32i  4
 * ........x...............x...............x...............x.......  1/16       4       8+16i   3
 * 
 * ....x.......x.......x.......x.......x.......x.......x.......x...  1/8        8       4+8i    2
 * ..x...x...x...x...x...x...x...x...x...x...x...x...x...x...x...x.  1/4       16       2+4i    1
 * .x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x.x  1/2       32       1+2i    0
 * 0         1         2         3         4         5         6
 * 0123456789012345678901234567890123456789012345678901234567890123
 */

typedef uint16_t index_t;

template <uint N>
class ParameterSpacePart
{
public:
    enum {
        None = 0,
        SkipEven = 1
    } flags;
    
    std::array<index_t, N>      x0;
    std::array<index_t, N>      dx;
    std::array<index_t, N>      x1;
};

template <uint N>
class ParameterGenerator
{
    std::array<index_t, N>              pos;
    std::array<index_t, N>              size;
    
    const ParameterSpacePart<N>&        part;
    
    bool                                end;
    
    void advance( )
    {
        for( uint n = 0; n < N; n++ ) {
            pos[n]++;
            if( pos[n] < size[n] )
                return;
            
            pos[n] = 0;
        }
        
        end = true;
    }
    
    bool allEven()
    {
        for( uint n = 0; n < N; n++ ) {
            if( pos[n] & 1 ) {
                return false;
            }
        }
        return true;
    }
    
public:
    ParameterGenerator( const ParameterSpacePart<N>& part ) :
        part( part )
    {
        end = false;
        pos.fill(0);
        
        for( uint n = 0; n < N; n++ )
            size[n] = (part.x1[n] - part.x0[n] + part.dx[n] - 1) / part.dx[n];
    }
    
    
    bool next( std::array<index_t, N>& x )
    {
        if( end )
            return false;
        
        if( part.flags & ParameterSpacePart<N>::SkipEven ) {
            while( allEven() ) {
                advance();
                if( end )
                    return false;
            }
        }
        
        for( uint n = 0; n < N; n++ )
            x[n] = part.x0[n] + part.dx[n] * pos[n];
        
        advance();
        return true;
    }
};

template <uint N>
class BlockGenerator
{
    int startLevel;
    int level;
    index_t pos[N];
    
    std::array<index_t, N>      imageSize;
    std::array<index_t, N>      blockSize;
    
public:
    BlockGenerator( std::array<index_t, N> shape )
    {
        imageSize = shape;
        
        startLevel = std::max<int>( log2( *std::min_element( imageSize.begin(), imageSize.end() ) ), 0 );
        
        level = startLevel;
        for( uint n = 0; n < N; n++ )
            pos[n] = 0;
    }
    
    // calculate best size
    void setBlockSize( uint size )
    {
        uint shift = log2( size ) / N;
        uint bs = 1 << (N * shift);
        
        for( uint n = 0; n < N; n++ ) {
            if( bs * 2 <= size ) {
                blockSize[n] = 2 << shift;
                bs *= 2;
            } else {
                blockSize[n] = 1 << shift;
            }
        }   
    }
    
    bool take( ParameterSpacePart<N> &part )
    {
        if( level < 0 )
            return false;
        
        uint step = 1 << level;
        
        for( uint n = 0; n < N; n++ ) {
            part.x0[n] = pos[n] * step * 2;
            part.dx[n] = step;
            part.x1[n] = std::min<index_t>( imageSize[n], ( pos[n] + blockSize[n] ) * step * 2 );
            part.flags = level == startLevel ? ParameterSpacePart<N>::None : ParameterSpacePart<N>::SkipEven;
        }
        
        uint incdim = 0;
        while( incdim < N ) {
            pos[incdim] += blockSize[incdim];
            if( pos[incdim] * step * 2 >= imageSize[incdim] ) {
                pos[incdim] = 0;
                incdim++;
            }
            else
                break;
        }
        
        if( incdim == N ) {
            level--;
        }
        
        return true;
    }
};

/*!
 * \brief       helper to take all vectors into account
 */
template <uint Dims, class real>
class NullCondition
{
public:
    bool operator()( const std::array<real, Dims>& array ) const
    {
        return true;
    }
};


/*!
 * \brief       Class to distrubute the vector space to the workers
 * 
 * \param       Dim
 *              Number of dimensions of the vector space
 * \param       real
 *              Datatype of vector space (float, double)
 * \param       Cond
 *              Class with `bool operator() (const std::array<real, Dims>& array ) const`
 *              to test if the given vector should be computed or not
 * 
 **/
template <uint Dim, class real, class Cond = NullCondition<Dim, real>>
class ParameterSpace
{
    std::array<real, Dim>               m_x0, m_x1;
    std::array<std::size_t, Dim>        m_samples;
    
    class Node
    {
        std::array<std::size_t, Dim>    m_sep;
        std::array<Node*, 1 << Dim>     m_childs;
        
        enum {
            Empty = 0,
            Full = 1,
            Partial
        }                               m_state;
        
    public:
        bool resolve( const std::array<std::size_t, Dim>& vec ) const
        {
            if( m_state != Partial )
                return m_state;
            
            std::size_t index = 0;
            for( uint d = 0; d < Dim; d++ )
                index |= vec[d] < m_sep[d] ? 0 : 1 << d;
            
            return m_childs[index]->resolve( vec );
        }
        
        void setPoint( const std::array<std::size_t, Dim>& vec, bool state )
        {
            if( m_state != Partial ) {
                m_state = state;
            } else {
                
                
            }
        }
    };
    
public:
    /*!
     * \param   x0
     *          Lowest value
     * \param   x1
     *          highest value (not included)
     * \param   samples
     *          number of samples in each dimensions
     */
    ParameterSpace( std::array<real, Dim> x0, std::array<real, Dim> x1, std::array<std::size_t, Dim> samples ):
        m_x0( x0 ), 
        m_x1( x1 ),
        m_samples( samples )
    {
        
    }
    
    void finished( const ParameterSpacePart<Dim>& part )
    {
        
    }
    
    
};

#endif