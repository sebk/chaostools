/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_LOGISTISCHE_GL
#endif

#ifdef USE_LOGISTISCHE_GL
#define MAIN_CLASS LogistischeGleichung

#include <random>
#include "canvas.h"
#include "instance.h"
#include "pool.h"

class LogistischeGleichung : public Instance {
public:
    typedef double real;
    typedef Canvas<real, int32_t> canvas_t;
    
    LogistischeGleichung( ConfigReader& config ):
        Instance(),
        canvas( config, 0 )
    {
        config.setSection( "logistische" );
        samples = config.get<int>( "samples" );
        iterations = config.get<int>( "iterations" );
        exp_a = config.get<uint>("exp_a", 1);
        exp_b = config.get<uint>("exp_b", 1);
        x0 = config.get<real>("x0");
        x1 = config.get<real>("x1");
    }
    
    real powerInt( real r, uint p )
    {
        real o = 1.0;
        while( p-- )
            o *= r;
            
        return o;
    }
    
    real iterate( real r, real x, int iterations ) {
        while( --iterations )
            x = r * powerInt(x, exp_a) *  (1.0 - powerInt(x, exp_b));
        
        return x;
    }
    
    /* x_{n+1} = r * ( 2 x^2 + 2x^3 - 3x^6 )
    real iterate2( real r, real x, int iterations ) {
        while( --iterations )
            x = r * (-3. * powerInt(x, 6) + 2. * powerInt(x, 3) + 2. * powerInt(x, 2));
        
        return x;
    }
    */
    
    virtual void run() {
        std::mt19937_64 generator;
        
        
        std::uniform_real_distribution<real> distributionC( x0, x1 );
            /*
            std::max( 0.0, canvas.yValue( 0 ) ),
            std::min( 1.0, canvas.yValue( canvas.height() ) ) );
            */
        
        double canvasX0 = canvas.xValue( 0 );
        double canvasX1 = canvas.xValue( canvas.width() );
        
        /*
        if( canvasX0 < 2.9 ) {
            std::uniform_real_distribution<real> distributionA(canvasX0, 2.9);
            for( int sample = 0; sample < samples/100; sample++ ) {
                setProgress( sample / (samples * 1.01) );
                
                real r = distributionA( generator );
                real x = distributionC( generator );
                canvas.add( r, iterate( r, x, iterations ), 1 );
            }
        }
        */
        std::uniform_real_distribution<real> distributionB( canvasX0, canvasX1 );
        for( int sample = 0; sample < samples; sample++ ) {
            setProgress( sample, samples );
            
            real r = distributionB( generator );
            real x = distributionC( generator );
            canvas.add( r, iterate( r, x, iterations ), 1 );
        }
        
        setProgress( 1.0 );
    }
    
    void collect( LogistischeGleichung& other ) {
        canvas += other.canvas;
    }
    
    void finish( ConfigReader& config ) {
        canvas.write();
    }
    
private:
    int samples;
    int iterations;
    uint exp_a;
    uint exp_b;
    real x0;
    real x1;
    
    canvas_t canvas;
};

#endif