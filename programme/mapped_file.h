/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAPPED_FILE_H
#define MAPPED_FILE_H

#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <system_error>
#include <assert.h>
#include <stdint.h>
#include <limits>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>

class MappedFile {
    int                         m_fd;
    __off64_t                   m_size;
    __off64_t                   m_mmapped_size;
    std::size_t                 m_uses;
    
    static const int            pageSizeBits = 10 + 10 + 6;
    static const __off64_t      pageSizeMask = ~((__off64_t) ((1UL << pageSizeBits) - 1));
    static const __off64_t      pageSize = 1UL << pageSizeBits;
    static const uint           maxPages = 4;
    static const int            MAP_FLAGS = MAP_SHARED | MAP_LOCKED; // | MAP_HUGETLB; //  | MAP_POPULATE ;
    static const int            MAP_PROT = PROT_READ | PROT_WRITE;
    static const int            FILE_FLAGS = O_RDWR | O_CREAT | O_LARGEFILE;
    
    static const int            readAheadAdvise = MADV_SEQUENTIAL | MADV_WILLNEED;
    static const int            normalAdvice = MADV_NORMAL;
    
    struct {
        char*                   addr;
        __off64_t               offset;
        std::size_t             reads;
        std::size_t             writes;
        std::size_t             lastUse;
    }                           m_pages[ maxPages ];
    
    typedef std::function<void(__off64_t start, __off64_t end)> flushAreaCallback_t;
    flushAreaCallback_t         m_flushCallback;

public:
    void setFlushCallback( flushAreaCallback_t callback )
    {
        m_flushCallback = callback;
    }

protected:
    
    MappedFile()
    {
        m_fd = -1;
        
        long int sysPageSize = sysconf(_SC_PAGE_SIZE);
        if( pageSize % sysPageSize )
            throw std::runtime_error(std::string("page size mismatch: ") + std::to_string( sysPageSize ) );
        
        for( uint p = 0; p < maxPages; p++ ) {
            m_pages[ p ].addr = NULL;
            m_pages[ p ].offset = 0;
            m_pages[ p ].reads = 0;
            m_pages[ p ].writes = 0;
            m_pages[ p ].lastUse = 0;
        }
    }
    
    ~MappedFile()
    {
        if( m_fd >= 0 )
            close();
    }
    
    
    void open( const char* filename, bool overwrite )
    {
        assert( m_fd == -1 );
        m_fd = ::open64( filename, FILE_FLAGS | (overwrite ? O_TRUNC : 0) );
        if( m_fd < 0 )
            throw std::system_error( std::error_code( errno, std::system_category() ), std::string("open ").append(filename) );
        
        struct stat64 s;
        if( fstat64( m_fd, &s ) )
            throw std::system_error( std::error_code( errno, std::system_category() ), std::string("fstat") );
        
        m_size = s.st_size;
        m_mmapped_size = m_size;
        m_uses = 0;
    }
    
    void close()
    {
        assert( m_fd >= 0 );
        
        for( uint page = 0; page < maxPages; page++ ) {
            if( m_pages[ page ].addr )
                unmap( page );
        }
        
        if( m_mmapped_size > m_size )
            ftruncate64( m_fd, m_size );
        
        ::close( m_fd );
        m_fd = -1;
    }
    
    void map( uint page, __off64_t pageOffset, char* hint, bool sequential )
    {
        assert( m_pages[ page ].addr == NULL );
        assert( !(pageOffset & ~pageSizeMask) );
        
        std::cout << "MappedFile: mapping page " << page << " at " << pageOffset << std::endl;
        
        void* ptr = mmap64( hint, pageSize, MAP_PROT, MAP_FLAGS, m_fd, pageOffset );
        
        if( ptr == MAP_FAILED )
            throw std::system_error( std::error_code( errno, std::system_category() ), std::string("mmap") );
        
        m_pages[ page ].addr = (char*) ptr;
        m_pages[ page ].offset = pageOffset;
        m_pages[ page ].reads = 0;
        m_pages[ page ].writes = 0;
        m_pages[ page ].lastUse = 0;
        
        madvise( ptr, pageSize, sequential ? readAheadAdvise : normalAdvice );
    }
    
    void unmap( uint page )
    {
        if( m_flushCallback )
            m_flushCallback( m_pages[ page ].offset, m_pages[ page ].offset + pageSize );
        
        std::cout << "MappedFile: unmapping page " << page << std::endl;
        std::cout << "  offset: " << std::setw(10) << m_pages[ page ].offset << std::endl;
        std::cout << "  reads:  " << std::setw(10) << m_pages[ page ].reads << std::endl;
        std::cout << "  writes: " << std::setw(10) << m_pages[ page ].writes << std::endl;
        
        if( munmap( m_pages[ page ].addr, pageSize ) )
            throw std::system_error( std::error_code( errno, std::system_category() ), std::string("munmap") );
        
        m_pages[ page ].addr = NULL;
    }
    
    // return the page to use
    uint load( __off64_t offset, bool sequential )
    {
        __off64_t startOffset = offset & pageSizeMask;
        
        assert( offset >= startOffset && offset < startOffset + pageSize );
        
        /* look in mapped pages */
        for( uint p = 0; p < maxPages; p++ ) {
            if( m_pages[ p ].addr ) {
                if( startOffset == m_pages[ p ].offset ) {
                    return p;
                }
            }
        }
        
        /* create a page */
        uint oldestPage = 0;
        std::size_t minUse = std::numeric_limits<std::size_t>::max();
        for( uint p = 0; p < maxPages; p++ ) {
            if( m_pages[ p ].addr == NULL ) {
                map( p, startOffset, NULL, sequential );
                return p;
            }
            
            if( m_pages[ p ].lastUse < minUse ) {
                minUse = m_pages[ p ].lastUse;
                oldestPage = p;
            }
        }
        
        char* hint = m_pages[ oldestPage ].addr;
        unmap( oldestPage );
        
        map( oldestPage, startOffset, hint, sequential );
        
        return oldestPage;
    }
    
    void _read( char* data, __off64_t position, __off64_t size, bool sequential = false )
    {
        __off64_t written = 0;
        
        while( written < size ) {
            __off64_t start = position + written;
            
            uint page = load( start, sequential );
            if( sequential )
                load( start + pageSize, sequential );
            
            m_pages[ page ].reads++;
            m_pages[ page ].lastUse = ++m_uses;
            
            __off64_t length = std::min( m_pages[ page ].offset + pageSize - start, size - written );
            ::memcpy( data + written, m_pages[ page ].addr + start - m_pages[ page ].offset, length );
            
            written += length;
        }
    }
    
    void _write( const char* data, __off64_t position, __off64_t size )
    {
        if( position + size > m_size ) {
            m_size = position + size;
            
            if( m_size > m_mmapped_size ) {
                // resize to next pageSize
                m_mmapped_size = (m_size + pageSize - 1) & pageSizeMask;
                
                ftruncate64( m_fd, m_mmapped_size );
            }
        }
        
        __off64_t written = 0;
        
        while( written < size ) {
            __off64_t start = position + written;
            
            uint page = load( start, false );
            m_pages[ page ].writes++;
            m_pages[ page ].lastUse = ++m_uses;
            
            __off64_t length = std::min( m_pages[ page ].offset + pageSize - start, size - written );
            ::memcpy( m_pages[ page ].addr + start - m_pages[ page ].offset, data + written, length );
            
            written += length;
        }
    }
    
    __off64_t _append( const char* data, __off64_t size )
    {
        __off64_t p = m_size;
        
        _write( data, p, size );
        
        return p;
    }
    
public:
    std::size_t size()
    {
        return m_size;
    }
};

#endif