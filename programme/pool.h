/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POOL_H
#define POOL_H

#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "instance.h"
#include <time.h>
#include <signal.h>

template <class Task, typename... Args>

class Pool {
    int                         threadCount;
    typename Task::Shared       shared;
    
    class Runner {
        std::thread     m_thread;
        Instance*       m_instance;
        bool            m_running;
        
    public:
        Runner() {
            m_instance = NULL;
            m_running = false;
        }
        
        operator bool() const {
            return m_instance != NULL;
        }
        
        void create( int thread, int threadCount, typename Task::Shared* shared, Args&... args ) {
            m_running = true;
            m_instance = new Task( args... );
            m_thread = std::thread( Instance::start, thread, threadCount, m_instance, static_cast<Instance::Shared*>( shared ) );
        }
        
        bool running() const {
            if( m_instance )
                return m_instance->running();
            else
                return false;
        }
        
        float progress() const {
            if( m_instance )
                return m_instance->progress();
            else
                return 1.0;
        }
        
        void wait() {
            if( m_thread.joinable() )
                m_thread.join();
        }
        
        void stop() {
            if( m_instance )
                m_instance->stop();
        }
        
        Task* instance() const {
            return reinterpret_cast<Task*>( m_instance );
        }
        
    };
    
    Runner*             runners;
    
public:
    Pool( int N, ConfigReader& config ) : shared( config, N ) {
        if( N < 1 )
            N = 1;
        
        if( N > 128 )
            N = 128;
        
        threadCount = N;
        
        runners = new Runner[threadCount];
    }
    
    ~Pool() {
        delete[] runners;
    }
    
    void start( Args... args ) {
        for( int i = 0; i < threadCount; i++ ) {
            if( runners[ i ] )
                continue;
                
            runners[ i ].create( i, threadCount, &shared, args... );
        }
    }
    
    void stop() {
        for( int i = 0; i < threadCount; i++ )
            runners[ i ].stop();
        
    }
    
    void drawProgess() {
        for( int i = 0; i < threadCount; i++ ) {
            printf( "thread %i: %5.2f%%", i, 100 * runners[ i ].progress() );
        }
    }
    
    void wait( int loglevel = 0 ) {
        if( loglevel ) {
            int lastPercent = 0;
            while( true ) {
                sleep( 1 );
                if( loglevel > 1 ) {
                    fputc( '\r', stdout );
                    for( int i = 0; i < threadCount; i++ ) {
                        fprintf( stdout, "%i: %5.2f%%%s", i, 100 * runners[ i ].progress(), i < (threadCount-1) ? ",   " : "" );
                    }
                    
                    fflush( stdout );
                } else {
                    float progress = 0.0;
                    for( int i = 0; i < threadCount; i++ )
                        progress += runners[ i ].progress();
                    
                    int percent = progress * 100. / threadCount;
                    if( percent > lastPercent ) {
                        time_t now = time(0);
                        tm *ltm = localtime(&now);
                        
                        printf("%02i.%02i. %02i:%02i:%02i  %3i%%\n", ltm->tm_mday, ltm->tm_mon, ltm->tm_hour, ltm->tm_min, ltm->tm_sec, percent );
                        lastPercent = percent;
                    }
                }
                
                bool running = false;
                for( int i = 0; i < threadCount; i++ )
                    running |= runners[ i ].running();
                
                if( !running ) {
                    fprintf( stdout, "\n\n" );
                    break;
                }
            }
        }
        
        for( int i = 0; i < threadCount; i++ ) {
            if( runners[ i ] )
                runners[ i ].wait();
        }
    }
    
    Task* collect() {
        Task* first = reinterpret_cast<Task*>( runners[0].instance() );
        for( int i = 1; i < threadCount; i++ )
            first->collect( *(reinterpret_cast<Task*>( runners[i].instance() )) );
        
        return first;
    }
};

#endif
