/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_SINUSABBILGUNG
#endif

#ifdef USE_SINUSABBILGUNG
#define MAIN_CLASS SinusAbbildung

#include <random>
#include "canvas.h"
#include "instance.h"
#include "pool.h"

class SinusAbbildung : public Instance {
public:
    typedef double real;
    typedef Canvas<real, int32_t> canvas_t;
    
    SinusAbbildung( ConfigReader& config ):
        Instance(),
        canvas( config, 0 )
    {
        config.setSection( "sinusabb" );
        samples = config.get<int>( "samples" );
        iterations = config.get<int>( "iterations" );
        x0_min = config.get<real>( "x0_min", canvas.yValue( 0 ) );
        x0_max = config.get<real>( "x0_max", canvas.yValue( canvas.height() ) );
    }
    
    real iterate( real r, real x, int iterations ) {
        while( --iterations )
            x = r * sin( x );
        
        return x;
    }
    
    virtual void run() {
        std::mt19937_64 generator;
        
        std::uniform_real_distribution<real> distributionA( canvas.xValue( 0 ) , canvas.xValue( canvas.width() ) );
        std::uniform_real_distribution<real> distributionB( x0_min, x0_max );
        
        for( int sample = 0; sample < samples; sample++ ) {
            setProgress( sample, samples );
            
            real r = distributionA( generator );
            real x = distributionB( generator );
            canvas.add( r, iterate( r, x, iterations ), 1 );
        }
        
        setProgress( 1.0 );
    }
    
    canvas_t& result() {
        return canvas;
    }
    
    void collect( SinusAbbildung& other ) {
        canvas += other.canvas;
    }
    
    void finish( ConfigReader& config ) {
        canvas.write();
    }
    
private:
    int samples;
    int iterations;
    
    real x0_min;
    real x0_max;
    
    canvas_t canvas;
};

#endif