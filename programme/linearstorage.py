#!/usr/bin/python3.2

#   This file is part of ChaosTools.
#
#   ChaosTools is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   ChaosTools is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.

import struct
from sys import argv
import numpy
import overlay

class IntArrayInfo:
    def __init__(self, s, f, type, size):
        self.c, self.t0, self.dt = struct.unpack("Idd", f.read(24))
        self.info = numpy.fromfile(f, dtype=numpy.float32, count=self.c * 2).reshape((self.c, 2))
        
class Int16Array2d:
    _cache = {}
    vChannel = 2
    
    def __init__(self, s, f, type, size):
        ref, self.N, self.c = struct.unpack("QII", f.read(16))
        self.arr = numpy.fromfile(f, dtype=numpy.int16, count=self.N * self.c).reshape((self.N, self.c))
        
        if ref not in self._cache:
            self._cache[ref] = s.read(ref)
        
        self.ref = self._cache[ref]
    
    def voltage(self):
        a = numpy.array( self.arr[..., self.vChannel], dtype=numpy.float )
        a *= self.ref.info[self.vChannel, 1]
        a += self.ref.info[self.vChannel, 0]
        a *= a
        return a.sum() / self.N

class LinearStorage:
    nodes = {
        257: IntArrayInfo,
        522: Int16Array2d
    }
    
    def __init__(self, filename):
        self.file = open(filename, "rb")
        self.icache = {}
    
    def pos(self, index):
        if index in self.icache:
            return self.icache[index]
        
        p = 40
        
        for level in range(4, -1, -1):
            self.file.seek(p)
            d = self.file.read(24)
            nindex, size, type = struct.unpack("qqhxxxxxx", d)
            assert type == 1
            d = self.file.read(size-24)
            pos = struct.unpack("256q", d)
            p = pos[(index >> 8 * level) & 255]

        self.file.seek(p)
        d = self.file.read(24)
        nindex, size, type = struct.unpack("qqhxxxxxx", d)
        assert nindex == index
        
        self.icache[index] = p
        return p
    
    def read(self, index):
        p = self.pos(index)
        self.file.seek(p)
        d = self.file.read(24)
        nindex, size, type = struct.unpack("qqhxxxxxx", d)
        if type in self.nodes:
            return self.nodes[type](self, self.file, type, size)
        else:
            return None
        

def dump(filename):
    f = open(filename, "rb")
    while True:
        p = f.tell()
        d = f.read(24)
        if len(d) != 24:
            break
        
        index, size, type = struct.unpack("qqhxxxxxx", d)
        print( "%08i: Node type %i\n  - index %i\n  - size %i" % (p, type, index, size))
        
        d = f.read(size-24)
        if type == 1:
            pos = struct.unpack("256q", d)
            for i in range(256):
                if pos[i] != -1:
                    print("    - %i: %08i" % (i, pos[i]))
    f.close()

def saveDat(filename, arr):
    f = open(filename, "wt")
    for x, y in arr:
        f.write("%g %g\n" % (x, y))
    f.close()
    
def analyze(arr):
    a_min = arr.min()
    a_max = arr.max()
    da = arr[0:-1] - arr[1:]
    d = a_max - a_min
    for v in da:
        if v:
            v = abs(v)
            if v < d:
                d = v
            
    return a_min, a_max + 1, d
    
def saveImage(filename, a):
    xmin, xmax, xdelta = analyze(a.arr[..., 0])
    ymin, ymax, ydelta = analyze(a.arr[..., 1])
    
    width = (xmax - xmin) / xdelta + 1
    height = (ymax - ymin) / ydelta + 1
    
    xy = numpy.empty( shape=(a.N, 2), dtype=numpy.int )
    xy[..., 0] = (a.arr[..., 0] - xmin) / xdelta
    xy[..., 1] = (ymax - a.arr[..., 1]) / ydelta
    
    ma = numpy.zeros(shape=(height, width), dtype=numpy.int)
    for x, y in xy:
        ma[y, x] += 1
    
    cmap = overlay.cmaps["binary"]
    overlay.createImage( ma, cmap, 0.0, 5 ).save( filename ) 
    
if __name__ == "__main__":
    s = LinearStorage("/media/DATA/feigenbaum4.storage")
    flog = open("/media/DATA/feigenbaum4.voltage", "wt")
    
    for i in range(100000):
        a = s.read(i)
        if isinstance(a, Int16Array2d):
            v = a.voltage()
            print(i, v)
            flog.write("%5i %f\n" % (i, v))
    
    #saveDat("/tmp/test.dat", a.arr[..., :2])
    #saveImage("/tmp/test.png", a)
    