/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PENDEL_H
#define PENDEL_H

#include <boost/array.hpp>
#include <math.h>
#include "configreader.h"

template <class real>
class PendelGl
{
private:
    static real pow2( real x )
    {
        return x * x;
    }
    
public:
    // x, y, dx, dy
    typedef boost::array<real, 4> state_t;
    
    real        h_2; // höhe des pendels über magneten ^2
    real        k; // reibung
    real        L;
    real        L_2; // Länge des Pendels ^2
    
    int         numMagnets;
    typedef struct {
        real    x;
        real    y;
    } point2d_t;
    
    std::vector<point2d_t>  magnets;
    
    
    void init( ConfigReader& config )
    {
        h_2 = pow2( config.get<real>("h") );
        k = config.get<real>("k");
        L = config.get<real>("L");
        L_2 = pow2( L );
        numMagnets = config.get<uint>("magnets");
        
        magnets.resize( numMagnets );
        for( int i = 0; i < numMagnets; i++ ) {
            real phi = (real) i * M_PI * 2. / (real) numMagnets;
            magnets[ i ].x = cos( phi );
            magnets[ i ].y = sin( phi );
        }
    }
    
    real V( const real& x, const real& y )
    {
        real V = L - std::pow( L_2 - pow2( x ) - pow2( y ), 0.5 );
        
        for( int i = 0; i < numMagnets; i++ ) {
            real x_prime = x - magnets[i].x;
            real y_prime = y - magnets[i].y;
            
            V -= std::pow( h_2 + pow2( x_prime ) + pow2( y_prime ), -0.5 ) / numMagnets;
        }
        
        return V;
    }
    
    void operator() ( const state_t& p, state_t& dpdt, const real t )
    {
        real r_2 = pow2( p[0] ) + pow2( p[1] );
        real d_V_G = - std::pow( L_2 - r_2, -0.5 );
        
        dpdt[0] = p[2];
        dpdt[1] = p[3];
        dpdt[2] = p[0] * d_V_G - p[2] * k;
        dpdt[3] = p[1] * d_V_G - p[3] * k;
        
        for( int i = 0; i < numMagnets; i++ ) {
            real x_prime = p[0] - magnets[i].x;
            real y_prime = p[1] - magnets[i].y;
            real d_V_m = std::pow( h_2 + pow2( x_prime ) + pow2( y_prime ), -1.5 ) / numMagnets;
            dpdt[2] -= x_prime * d_V_m;
            dpdt[3] -= y_prime * d_V_m;
        }
    }
    
    int identify( const state_t& p )
    {
        if( pow2( p[0] ) + pow2( p[1] ) < 0.01 ) {
            std::cout << "could not identify:" << std::endl;
            std::cout << std::scientific;
            std::cout << p[0] << std::endl;
            std::cout << p[1] << std::endl;
            std::cout << p[2] << std::endl;
            std::cout << p[3] << std::endl;
            return -1;
        }
        
        int num = (atan2( p[1], p[0] ) + 2 * M_PI) * (numMagnets / (M_PI * 2.0)) + 0.5;
        return num % numMagnets;
    }
};


#endif