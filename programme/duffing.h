/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DUFFING_H
#define DUFFING_H

#include <boost/array.hpp>
#include <math.h>
#include "configreader.h"

template <class real>
class DuffingGl
{
public:
    typedef boost::array<real, 2> state_t;
    
    real        epsilon;
    real        lambda;
    real        beta;
    real        Omega;
    real        alpha;
    
    void init( ConfigReader& config )
    {
        epsilon = config.get<real>("epsilon");
        lambda = config.get<real>("lambda");
        beta = config.get<real>("beta");
        Omega = config.get<real>("Omega");
        alpha = config.get<real>("alpha");
    }
    
    void operator() ( const state_t& x, state_t& dxdt, const real t )
    {
        dxdt[0] = x[1];
        dxdt[1] = epsilon * std::cos( Omega * t ) - lambda * x[1] - alpha * x[0] - beta * x[0] * x[0] * x[0];
    }
};


#endif