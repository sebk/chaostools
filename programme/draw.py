#!/usr/bin/python

#   This file is part of ChaosTools.
#
#   ChaosTools is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   ChaosTools is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.

from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt
from matplotlib.axis import Tick
import matplotlib.ticker as ticker
import numpy
from math import log, floor, ceil, pi
from config import ConfigReader

def MathNumber(x):
  if x:
    p = int(floor(log(abs(x), 10)+0.001))
    if -2 < p < 2:
        p = 0
  else:
    p = 0
  
  f = x / (10.**p)
  i = int( f + (0.5 if f > 0 else -0.5) )
  
  if not p:
    return str(i if abs(f - i) < 1e-9 else f)
  
  return "%s \cdot 10^{%i}" % (i if abs(f - i) < 1e-9 else f, p)

def fmt(x, n):
  return "$%s$" % MathNumber(x)
  
def genFig(data, size, title, imArgs, plotArgs, colorbar, extent, text, textPos, isPhase=False):
    fig = plt.figure(figsize=size)
    a = fig.add_subplot(1,1,1, xmargin=0.0, ymargin=0.0)
    if title:
      a.set_title(title)
    a.xaxis.set_major_formatter(ticker.FuncFormatter( fmt ))
    # a.xaxis.set_major_locator(ticker.MaxNLocator(nbins=10))
    a.yaxis.set_major_formatter(ticker.FuncFormatter( fmt ))
    
    #labels = a.get_xticklabels()
    #for label in labels:
    #  label.set_rotation(-30) 
    
    if extent:
      x0, x1, y0, y1 = extent
      imArgs["extent"] = extent
    
    if isPhase:
      imArgs["vmin"] = -pi
      imArgs["vmax"] = pi
    
    
    im = plt.imshow(data, **imArgs)
    
    if colorbar != None:
      args = {"ax": a, "format": ticker.FuncFormatter( fmt ), "ticks": ticker.MaxNLocator(nbins=8)}
      for k, v in colorbar:
        args[k] = v
      plt.colorbar( **args )
    
    elif "colorbar" in plotArgs:
        c = plt.colorbar(ax=a, orientation=plotArgs["colorbar"], format=ticker.FuncFormatter( fmt ), ticks=ticker.MaxNLocator(nbins=8) )
        if( isPhase ):
            c.set_ticks( (-pi, -pi/2, 0., pi/2, pi) )
            c.set_ticklabels( ("$-\pi$", "$-\pi/2$", "$0$", "$\pi/2$", "$\pi$") )
    
    if "xlabel" in plotArgs:
        plt.xlabel( plotArgs["xlabel"] )
    if "ylabel" in plotArgs:
        plt.ylabel( plotArgs["ylabel"] )
    
    x, y = textPos
        
    if text:
      a.text(x, y, text, transform=a.transAxes, fontsize=14, verticalalignment='top')

def drawData( pp, config ):
    data = numpy.load(config.get("default", "datafile"))
    
    args = []
    for key, value in config.items("pyplot.keys", LaTeX = True):
        if not value: continue
        args.append( (key, value) )
        
    text = "\n".join( ("%s = %s" % (k, v)) for k, v in args )
    
    imArgs = {}
    for key, value in config.items("pyplot.imshow"):
        try:
            value = int(value)
        except:
            try:
                value = float(value)
            except:
                pass
        imArgs[key] = value
    
    xscale = float(config.getDefault("pyplot", "xscale", "1"))
    yscale = float(config.getDefault("pyplot", "yscale", "1"))
    coords = (
        float(config.get("canvas", "x0")) * xscale,
        float(config.get("canvas", "x1")) * xscale,
        float(config.get("canvas", "y0")) * yscale,
        float(config.get("canvas", "y1")) * yscale
    )
    
    textPos = eval( config.getDefault("pyplot", "keypos", "1.1, 0.95") )
    
    size = eval( config.getDefault("pdf", "size", "279, 210") )
    
    size = tuple( x / 25.4 for x in size )
    
    plotArgs = dict(config.items("pyplot", LaTeX = True))
    
    colorbar = config.items("pyplot.colorbar", convert=True, empty=None)
    
    valuemod = config.getDefault("canvas", "valuemod", "")
    if valuemod == "log":
        data = numpy.array( data, dtype="float64" )
        data = numpy.log( data + 1 )
    elif valuemod == "sqrt":
        data = numpy.sqrt( data )
    elif valuemod.startswith("pow(") and valuemod.endswith(")"):
        data = data**float(valuemod[4:-1])
    
    sigma = float( config.getDefault("canvas", "sigma", "0.0") )
    if( sigma ):
        from scipy.ndimage.filters import gaussian_filter
        data = numpy.array( data, dtype="float64" )
        data = gaussian_filter( data, sigma, mode="constant" )
    
    title = config.getDefault("pyplot", "title", "")
    pp.savefig( genFig( data, size, title, imArgs, plotArgs, colorbar, coords, text, textPos ),
        dpi=300
    )
    

if __name__ == "__main__":
    from sys import argv
    import os
    
    argv = argv[1:]
    
    # prevent too much memory from being used
    os.system("ulimit -m %i" % (2 * 1024 * 1024))
    os.system("ulimit -v %i" % (2 * 1024 * 1024))
  
    configs = []
    config = ConfigReader()
    
    filename = None
    
    if argv[0] == "-o":
        filename = argv[1]
        argv = argv[2:]
        
    for a in argv:
        if a == "-n":
            configs.append( config.copy() )
        else:
            config.read( a )
  
    configs.append( config )
    
    if not filename:
        filename = configs[0].get("pdf", "file")    
    
    pp = PdfPages(filename)
    
    for c in configs:
        try:
            canvasinfo = c.get("default", "canvasinfo")
            c.read( canvasinfo )
        except KeyError:
            pass
        
        drawData( pp, c )
  
    pp.close()

