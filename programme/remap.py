#!/usr/bin/python2.7

from PIL import Image
import numpy as np

import sys

data = np.load( sys.argv[1] )
data = np.array( data + 0.5, dtype=np.intc )

numA = int(sys.argv[2])
numB = int(sys.argv[3])

imap = np.zeros( shape=(data.max() + 1,), dtype=np.intc)
for i in range(0, data.max()+1):
  imap[i] = i

imap[numA] = numB
imap[numB] = numA

data2 = np.take(imap, data, axis=0, mode='clip')

np.save( sys.argv[1], data2 )
