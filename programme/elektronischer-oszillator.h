/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_ELEKTR_OSZILL
#endif

#ifdef USE_ELEKTR_OSZILL
#define MAIN_CLASS ElektrOszill

#include "canvas.h"
#include "instance.h"
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/integrate/integrate_times.hpp>
#include <functional>
#include <random>
#include "configreader.h"
#include "elektronischer-oszillator-system.h"

class ElektrOszill : public Instance {
public:
    static const uint maxThreads = 1;
    
    typedef double real;
    typedef Canvas<real, float>         canvas_t;
    typedef ElektrOszillSystem<real>    system_t;
    
    ElektrOszill( ConfigReader& config ):
        Instance(),
        m_canvas( config, 0 )
    {
        config.setSection("elektr-oszill");
        m_system.init( config );
        
        m_state[ 0 ] = config.get<real>("V_d_0");
        m_state[ 1 ] = config.get<real>("I_0");
        
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        m_poincare = config.get<bool>("poincare");
        m_theta = config.get<real>("theta");
    }
    
    virtual void run() {
        boost::numeric::odeint::runge_kutta_cash_karp54<system_t::state_t, real> stepper;
        
        if( m_poincare ) {
            real t0 = m_theta / m_system.omega;
            real increment = 2.0 * M_PI / m_system.omega;
            real t1 = t0 + increment;
            int stepCount = (m_t1 - m_t0) / increment;
            for( int step = 0; step < stepCount; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_system, m_state, t0, t1, m_dt );
                
                m_canvas.add( m_state[0], m_state[1], 1 );
                setProgress( step, stepCount );
            }
        } else {
            m_canvas.moveTo( m_state[0], m_state[1] );
            auto callback = std::bind(
                &ElektrOszill::addLine, this, std::placeholders::_1, std::placeholders::_2 );
            
            boost::numeric::odeint::integrate_adaptive( stepper, m_system, m_state, m_t0, m_t1, m_dt, callback );
        }
        setProgress( 1.0 );
    }
    
    void printState( system_t::state_t& state, real t ) {
        std::cout << t << " " << state[0] << " " << state[1] << std::endl;
    }
    
    void addLine( system_t::state_t& state, real t ) {
        m_canvas.lineTo( state[0], state[1], 1 );
        setProgress( (t - m_t0) / (m_t1 - m_t0) );
        //printState( state, t );
    }
    
    void addPoint( system_t::state_t& state, real t ) {
        m_canvas.add( state[0], state[1], 1 );
        setProgress( (t - m_t0) / (m_t1 - m_t0) );
    }
    
    void collect( ElektrOszill& other ) {
        m_canvas += other.m_canvas;
    }
    
    void finish( ConfigReader& config ) {
        m_canvas.write();
    }
    
protected:
    system_t    m_system;
    
    real        m_t0;
    real        m_t1;
    real        m_dt;
    real        m_theta;
    bool        m_poincare;
    system_t::state_t m_state;
    canvas_t    m_canvas;
};

#endif