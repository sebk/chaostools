import numpy, sys

a = numpy.load( sys.argv[1] )

print( numpy.log( a.clip(0., 1.).sum() ) / numpy.log( a.shape[0] ) )
