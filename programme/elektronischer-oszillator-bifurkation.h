/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_ELEKTR_OSZILL_BIFURK
#endif

#ifdef USE_ELEKTR_OSZILL_BIFURK

#define USE_ELEKTR_OSZILL
#include "elektronischer-oszillator.h"

#undef MAIN_CLASS
#define MAIN_CLASS ElektrOszillBifurk

class ElektrOszillBifurk : public ElektrOszill {
    
public:
    static const uint maxThreads = 0;
    
    ElektrOszillBifurk( ConfigReader& config ):
        ElektrOszill( config ),
        m_canvas2( config, 0, "canvas2" )
    {
        config.setSection("elektr-oszill");
        m_samples = config.get<int>("samples", 0);
        std::string param = config.get<std::string>("param");
        if( param == "omega" )
            m_param = &m_system.omega;
        else if( param == "V_s" )
            m_param = &m_system.V_s;
        else if( param == "C_r" )
            m_param = &m_system.C_r;
        else
            throw;
    }
    
    
    virtual void run() {
        real t0 = m_theta / m_system.omega;
        real increment = 2.0 * M_PI / m_system.omega;
        real t1 = t0 + increment;
        int stepCount = (m_t1 - m_t0) / increment;
        
        boost::numeric::odeint::runge_kutta_cash_karp54<system_t::state_t, real> stepper;
        
        real a = m_canvas.xValue( 0 );
        real b = (m_canvas.xValue( m_canvas.width() ) - a) / (m_samples * m_threadCount);
        
        for( int i = 0; i < m_samples; i++ ) {
            *m_param = a + (i * m_threadCount + m_thread) * b;
            system_t::state_t state( m_state );
            
            for( int step = 0; step < stepCount; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_system, state, t0, t1, m_dt );
                
                m_canvas.add( *m_param, state[0], 1 );
                m_canvas2.add( *m_param, state[1], 1 );
            }
            setProgress( i, m_samples );
        }
    }
    
    void collect( ElektrOszillBifurk& other ) {
        m_canvas += other.m_canvas;
        m_canvas2 += other.m_canvas2;
    }
    
    void finish( ConfigReader& config ) {
        m_canvas.write();
        m_canvas2.write();
    }
    
private:
    int         m_samples;
    canvas_t    m_canvas2;
    real*       m_param;
};

#endif