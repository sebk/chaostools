#!/usr/bin/python2.7

#   This file is part of ChaosTools.
#
#   ChaosTools is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   ChaosTools is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.


import numpy
from PIL import Image, PngImagePlugin
import math

def color(r, g, b, a=1.0):
    return numpy.array( (r, g, b, a), dtype=numpy.float )

colors = {
    "red": color(1.0, 0.0, 0.0),
    "green": color(0.0, 1.0, 0.0),
    "blue": color(0.0, 0.0, 1.0),
    "brown": color(.75,.5,.25),
    "lime": color(.75,1,0),
    "orange": color(1,.5,0),
    "pink": color(1,.75,.75),
    "purple": color(.75,0,.25),
    "teal": color(0,.5,.5),
    "violet": color(.5,0,.5),
    "cyan": color(0.0, 1.0, 1.0),
    "magenta": color(1.0, 0.0, 1.0),
    "yellow": color(1.0, 1.0, 0.0),
    "olive": color(.5,.5,0),
    "black": color(0.0, 0.0, 0.0),
    "darkgray": color(.25,.25,.25),
    "gray": color(.25,.25,.25),
    "lightgray": color(.75,.75,.75),
    "white": color(1.0, 1.0, 1.0),
    "transparent": color(0.0, 0.0, 0.0, 0.0),
}

class ColorMap:
    def __init__(self, N):
        self.N = N
        self.colors = numpy.zeros( shape=(N+2, 4), dtype=numpy.uint8 )
        
    def get(self, v):
        return self.colors[ int(v * N + 1.5) ]
        
    def __call__(self, data, vmin=None, vmax=None, out=None):
        if vmin == None: vmin = data.min()
        if vmax == None: vmax = data.max()
        data = (data - vmin) * float(self.N) / (vmax - vmin) + 1.
        
        indexes = numpy.array( data, dtype=numpy.intc )
        rgba = numpy.empty(shape=data.shape+(4,), dtype=numpy.uint8) if out == None else out
        numpy.take(self.colors, indexes, axis=0, mode='clip', out=rgba)
        
        return rgba
        

class ColorGradientMap(ColorMap):
    """
    N: number of color entries
    stops: list of color stops.
        stop: ( x, [r, g, b, a] )
        
    """
    
    @staticmethod
    def linear(x):
        return x
    
    @staticmethod
    def cos(x):
        return 0.5 * (1.0 - math.cos(math.pi * x))
    
    def __init__(self, N, stops, interpol=None, gamma=1.0, lower=None, upper=None):
        """
        lower: color for values below vmin
        upper: color for values abive vmax
        """
        if not interpol: interpol = ColorGradientMap.linear
        ColorMap.__init__(self, N)
        assert stops[0][0] == 0.0
        assert stops[-1][0] == 1.0
        
        for i in range(len(stops)-1):
            v0, c0 = stops[i]
            v1, c1 = stops[i+1]
            assert v1 > v0
            dv = v1 - v0
            
            g0, g1 = c0**gamma, c1**gamma
            
            for n in range(int(self.N * v0), int(self.N * v1)):
                v = float(n) / self.N
                s = (v - v0) / dv
                c = interpol(s) * c1 + interpol(1.0 - s) * c0
                self.colors[n+1] = c**(1./gamma) * 255 + 0.5
        
        self.colors[0] = self.colors[1] if lower == None else (lower*255 + 0.5)
        self.colors[self.N+1] = self.colors[self.N] if upper == None else (upper*255 + 0.5)

class ComplexColorMap(ColorGradientMap):
    zero = colors["black"]
    
    def __init__(self, N, stops, **args):
        ColorGradientMap.__init__(self, N, stops, **args)
    
    def __call__(self, data, vmin=0., vmax=None, amin=-numpy.pi, amax=numpy.pi, out=None):
        val = numpy.abs( data )
        arg = numpy.angle( data )
        
        if vmax == None:
          vmax = val.max()
        
        val = numpy.clip( val, vmin, vmax )
        
        val -= vmin
        val *= 1.0 / (vmax - vmin)

        arg -= amin
        arg *= float(self.N) / (amax - amin)
        arg += 1.5
        
        indexes = numpy.array( arg, dtype=numpy.intc )
        rgba = numpy.empty(shape=data.shape+(4,), dtype=numpy.uint8) if out == None else out
        
        numpy.take(self.colors, indexes, axis=0, mode='clip', out=rgba)
        
        rgba[..., ..., 3] = 255 * val
        
        return rgba
                
class ColorIndexMap:
    def __init__(self, colors):
        self.N = len(colors)
        self.colors = numpy.zeros( shape=(self.N, 4), dtype=numpy.uint8 )
        for i in range( self.N ):
            self.colors[i] = 255 * colors[i]
    
    def __call__(self, data, vmin, vmax, out=None):
        indexes = numpy.array( data, dtype=numpy.intc )
        del data
        indexes %= self.N
        rgba = numpy.empty(shape=indexes.shape+(4,), dtype=numpy.uint8) if out == None else out
        numpy.take(self.colors, indexes, axis=0, mode='clip', out=rgba)
        del indexes
        return rgba

cmaps = {}

cmaps["rainbow"] = ColorGradientMap(256*5, (
    ( 0.0, colors["red"] ),
    ( 1./5., colors["yellow"] ),
    ( 2./5., colors["green"] ),
    ( 3./5., colors["cyan"] ),
    ( 4./5., colors["blue"] ),
    ( 1.0, colors["magenta"] )
) )

cmaps["rainbow2"] = ColorGradientMap(256*6, (
    ( 0.0, colors["red"] ),
    ( 1./6., colors["yellow"] ),
    ( 2./6., colors["green"] ),
    ( 3./6., colors["cyan"] ),
    ( 4./6., colors["blue"] ),
    ( 5./6., colors["magenta"] ),
    ( 1.0, colors["red"] )
) )

cmaps["rainbow3"] = ColorGradientMap(256*6, (
    ( 0.0, colors["red"] ),
    ( 2./6., colors["green"] ),
    ( 4./6., colors["blue"] ),
    ( 1.0, colors["red"] )
), gamma=2.4, lower=colors["black"], upper=colors["white"] )

cmaps["darken"] = ColorGradientMap(256, (
    ( 0.0, color(0., 0., 0., 0.) ),
    ( 1.0, color(0., 0., 0., 1.) )
) )

cmaps["darken_red"] = ColorGradientMap(256, (
    ( 0.0, color(0., 0., 0., 0.) ),
    ( 1.0, color(1., 0., 0., 1.) )
) )
cmaps["darken_green"] = ColorGradientMap(256, (
    ( 0.0, color(0., 0., 0., 0.) ),
    ( 1.0, color(0., 1., 0., 1.) )
) )
cmaps["darken_blue"] = ColorGradientMap(256, (
    ( 0.0, color(0., 0., 0., 0.) ),
    ( 1.0, color(0., 0., 1., 1.) )
) )

cmaps["idarken"] = ColorGradientMap(256, (
    ( 0.0, color(0., 0., 0., 1.) ),
    ( 1.0, color(0., 0., 0., 0.) )
) )

cmaps["binary"] = ColorGradientMap(256, (
    ( 0.0, colors["white"] ),
    ( 1.0, colors["black"] )
) )

cmaps["binary2"] = ColorGradientMap(256, (
    ( 0.0, colors["black"] ),
    ( 1.0, colors["white"] )
) )

cmaps["lighten"] = ColorGradientMap(256, (
    ( 0.0, color(1., 1., 1., 0.) ),
    ( 1.0, color(1., 1., 1., 1.) )
) )

cmaps["ilighten"] = ColorGradientMap(256, (
    ( 0.0, color(1., 1., 1., 1.) ),
    ( 1.0, color(1., 1., 1., 0.) )
) )

cmaps["hot"] = ColorGradientMap(256*3, (
    ( 0.0, colors["black"] ),
    ( 1./3., colors["red"] ),
    ( 2./3., colors["yellow"] ),
    ( 1.0, colors["white"] ),
) )

cmaps["cold"] = ColorGradientMap(256*4, (
    ( 0.0, colors["black"] ),
    ( 1./4., colors["blue"] ),
    ( 2./4., colors["cyan"] ),
    ( 3./4., colors["purple"] ),
    ( 1.0, colors["red"] ),
) )

cmaps["jet"] = ColorGradientMap(256*4, (
    ( 0.0, color(0., 0., 0.5) ),
    ( 1./8., colors["blue"] ),
    ( 3./8., colors["cyan"] ),
    ( 5./8., colors["yellow"] ),
    ( 7./8., colors["red"] ),
    ( 1.0, color(0.5, 0., 0.) )
) )

cmaps["alien"] = ColorGradientMap(256*6, (
    ( 0.0, colors["black"] ),
    ( 1./5., colors["green"] ),
    ( 2./5., colors["cyan"] ),
    ( 3./5., colors["blue"] ),
    ( 4./5., colors["red"] ),
    ( 1.0, colors["white"] )
) )

cmaps["GrWM"] = ColorGradientMap(256*2, (
    ( 0.0, color(0.0, 0.5, 0.0) ),
    ( 0.5, colors["white"]),
    ( 1.0, colors["magenta"])
))

cmaps["index"] = ColorIndexMap( list(colors[n] for n in (
    "black", "red", "green", "blue", "yellow", "magenta", 
    "cyan", "brown", "pink", "violet", "olive"
) ) )

cmaps["index_print"] = ColorIndexMap( list(colors[n] for n in (
    "black", "red", "green", "blue", "magenta", "cyan",
    "lime", "pink", "violet", "olive", "yellow"
) ) )

cmaps["complex"] = ComplexColorMap(256*6, (
    ( 0.0, colors["red"] ),
    ( 1./6., colors["yellow"] ),
    ( 2./6., colors["green"] ),
    ( 3./6., colors["cyan"] ),
    ( 4./6., colors["blue"] ),
    ( 5./6., colors["magenta"] ),
    ( 1.0, colors["red"] )
) )

cmaps["range"] = ColorGradientMap( 1, (
  ( 0.0, color(0., 0., 0., 0.0) ),
  ( 1.0, color(0., 0., 0., 0.0) )
), lower=color(0., 0., 0., 0.25), upper=color(1., 1., 1., 0.25) )

def createImage( data, colormap, vmin, vmax, sigma=None ):
    if data.dtype not in (numpy.float, numpy.float32, numpy.float64, numpy.complex):
        print(data.dtype)
        data = numpy.array( data, dtype=numpy.float )
    
    if sigma:
        from scipy.ndimage.filters import gaussian_filter
        data = gaussian_filter( data, sigma, mode="constant" )
    
    im = Image.fromarray( colormap( data, vmin, vmax ) )
    return im

class Processor:
    def __init__(self, args):
        self.args = args
        self.argIndex = 1
        
        self.vmin = None
        self.vmax = None
        
        self.rawData = numpy.load( self.getArg("datafile") )
        
        self.operations = []
    
    def getArg(self, desc):
        return self.getArgs(desc, 1)[0]
    
    def getArgs(self, desc, n):
        if self.argIndex + n > len(self.args):
            self.usage()
            raise ValueError("expected %i %s %s" % (n, "arguments" if n > 1 else "argument", desc))
        
        r = self.args[self.argIndex : self.argIndex + n]
        self.argIndex += n
        return r
    
    def peek(self):
        if self.argIndex < len(self.args):
            return self.args[self.argIndex]
        else:
            return None
    
    def asFloat(self, arr):
        if arr.dtype not in (numpy.float32, numpy.float64, numpy.complex64, numpy.complex128):
            return numpy.array( arr, dtype=numpy.float )
        return arr
    
    def add(self, o):
        self.data += o
    
    def mul(self, f):
        self.data *= f
    
    def abs(self):
        self.data = numpy.abs( self.data )
    
    def pow(self, p):
        self.data **= p
    
    def log(self):
        self.data = numpy.log( self.data )
        
    def real(self):
        self.data = self.data.real
        
    def imag(self):
        self.data = self.data.imag
    
    def phase(self):
        self.data = numpy.angle(self.data)
    
    def gauss(self, sigma):
        from scipy.ndimage.filters import gaussian_filter
        self.data = gaussian_filter( self.data, sigma, mode="constant" )
    
    def ln(self):
        self.data = numpy.log(self.data)
    
    def addOp(self, op, *args):
        self.operations.append( (op, args) )
    
    def image(self, colormap, filename, vmin=None, vmax=None, descr=None, comment=None):
        if (vmin == None) or (vmax == None):
            if not self.operations:
                if vmin == None:
                    vmin = self.rawData.min()
                    print("min: %g" % vmin)
                    
                if vmax == None:
                    vmax = self.rawData.max()
                    print("max: %g" % vmax)
                
            elif self.rawData.shape[0] * self.rawData.shape[1] * 8 > 100e6:
                _vmin = numpy.inf
                _vmax = -numpy.inf
              
                print("first pass: compute vmin/vmax")
                im = numpy.empty( shape=self.rawData.shape+(4,), dtype=numpy.uint8 )
                y0 = 0
                dy = int(100e6 / 8 / self.rawData.shape[1])
                while y0 < self.rawData.shape[0]:
                    print("%i / %i" % (y0, self.rawData.shape[0]))
                    y1 = min(y0 + dy, self.rawData.shape[0])
                    self.data = self.asFloat( self.rawData[y0:y1] )
                    
                    for op, args in self.operations:
                        op(*args)
                    
                    _vmin = min( _vmin, self.data.min() )    
                    _vmax = min( _vmax, self.data.max() )  
                
                if vmin == None:
                    vmin = _vmin
                    print("min: %g" % vmin)
                    
                if vmax == None:
                    vmax = _vmax
                    print("max: %g" % vmax)
            
            else:
                self.data = self.rawData.copy()
                for op, args in self.operations:
                    op(*args)
                    
                if vmin == None:
                    vmin = self.data.min()
                    print("min: %g" % vmin)
                    
                if vmax == None:
                    vmax = self.data.max()
                    print("max: %g" % vmax)
          
        if self.rawData.shape[0] * self.rawData.shape[1] * 8 > 100e6:
            # more than 100MB
            im = numpy.empty( shape=self.rawData.shape+(4,), dtype=numpy.uint8 )
            y0 = 0
            dy = int(100e6 / 8 / self.rawData.shape[1])
            while y0 < self.rawData.shape[0]:
                print("%i / %i" % (y0, self.rawData.shape[0]))
                y1 = min(y0 + dy, self.rawData.shape[0])
                if self.operations:
                    self.data = self.asFloat( self.rawData[y0:y1] )
                    for op, args in self.operations:
                        op(*args)
                    
                else:
                    self.data = self.rawData[y0:y1]
                
                colormap( self.data, vmin, vmax, out=im[y0:y1] )
                
                y0 = y1
        else:
            self.data = self.asFloat( self.rawData )
            for op, args in self.operations:
                op(*args)
                
            im = colormap( self.data, vmin, vmax )
        
        meta = PngImagePlugin.PngInfo()
        if comment:
            meta.add_text("Comment", comment, 0)
        if descr:
            meta.add_text("Description", descr, 0)
        meta.add_text("Software", "ChaosTools::overlay.py", 0)
        
        Image.fromarray( im ).save( filename,"PNG", pnginfo=meta )
    
    def usage(self):
        print("usage: %s datafile [-o OPERATOR] colormap image [-C comment] [-D desciption] [-r [vmin]:[vmax]]" % self.args[0])
        print("""\
 OPERATOR:
    add o               x + a
    mul f               x * f
    abs                 |x|
    real                Re(x)
    imag                Im(x)
    phase               arg(x)
    pow p               x^p
    log                 log(x)
    gauss sigma         apply gaussian filter with given sigma
""")
        
""" helper function """
def _genChart(image, maps, data, lower, upper, vmin, vmax, **args):
    draw = ImageDraw.ImageDraw(image)
    for i in range(len(maps)):
      name, cmap = maps[i]
      draw.text((2, 2 + 20 * i), name, **args)
      if lower != None:
        image.paste(Image.fromarray( cmap( lower, vmin, vmax ) ), (100, 20 * i) )
      
      image.paste(Image.fromarray( cmap( data, vmin, vmax ) ), (130, 20 * i) )
      
      if upper != None:
        image.paste(Image.fromarray( cmap( upper, vmin, vmax ) ), (540, 20 * i) )
    
    del draw
        
def genChart():
    font = ImageFont.truetype("/usr/share/fonts/truetype/ttf-liberation/LiberationSerif-Regular.ttf", 16)
    gradientMaps = []
    indexMaps = []
    complexMaps = []
    for name, cmap in cmaps.iteritems():
      if isinstance(cmap, ColorGradientMap):
        gradientMaps.append((name, cmap))
      elif isinstance(cmap, ColorIndexMap):
        indexMaps.append((name, cmap))
      elif isinstance(cmap, ComplexColorMap):
        complexMaps.append((name, cmap))
    
    gradientMaps.sort()
    gradientImage = Image.new("RGBA", (560, 20*len(gradientMaps)))
    _genChart(gradientImage, gradientMaps,
      numpy.ones(shape=(20, 1)).dot(numpy.linspace(0., 1., num=400, endpoint=False).reshape(1, 400)),
      numpy.ones(shape=(20, 20)) * -1,
      numpy.ones(shape=(20, 20)) * 2,
      0.0, 1.0,
      font=font, fill="black")
    gradientImage.save("gradients.png")
    
        
if __name__ == "__main__":
    from sys import argv
    
    if argv[1] == "--chart":
      from PIL import ImageDraw, ImageFont
      genChart()
      exit(0)
    
    P = Processor( argv )
    
    while P.peek() == "-o":
        assert P.getArg("") == "-o"
        
        op = P.getArg("operator")
        if op == "add":
            o = float(P.getArg("o"))
            P.addOp(P.add, o)
        elif op == "mul":
            f = float(P.getArg("f"))
            P.addOp(P.mul, f)
        elif op == "abs":
            P.addOp(P.abs)
        elif op == "real":
            P.addOp(P.real)
        elif op == "imag":
            P.addOp(P.imag)
        elif op == "phase":
            P.addOp(P.phase)
            P.vmin = -numpy.pi
            P.vmax = numpy.pi
        elif op == "pow":
            p = float(P.getArg("power exponent"))
            P.addOp(P.pow, p)
        elif op == "log":
            P.addOp(P.log)
        elif op == "gauss":
            s = float(P.getArg("sigma"))
            P.addOp(P.gauss, s)
        elif op == "ln":
            P.addOp(P.ln)
        else:
            print("unknown operator")
            P.usage()
            exit(1)
    
    cmap = cmaps[P.getArg("colormap")]
    imfile = P.getArg("image")
    
    comment = None
    descr = None
    vmin = None
    vmax = None
    while True:
        a = P.peek()
        if not a: break
        P.getArg("")
        
        if a == "-C":
            comment = P.getArg("comment")
        elif a == "-D":
            descr = P.getArg("description")
        elif a == "-r":
            a, b = P.getArg("range").split(":")
            if a:
                vmin = float(a)
            if b:
                vmax = float(b)
    
    P.image( cmap, imfile, vmin, vmax, comment=comment, descr=descr )
