/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_PENDEL_POT
#endif

#ifdef USE_PENDEL_POT
#define MAIN_CLASS PendelPotential

#include "canvas.h"
#include "instance.h"
#include "pool.h"
#include "pendel.h"
#include <complex>

class PendelPotential : public Instance
{
    typedef double                      real;
    typedef std::complex<float>         complex;
    typedef PendelGl<real>::state_t     state_t;
    
    PendelGl<real>                      m_pendel;
    
public:
    class Shared : public Instance::Shared {
        friend class PendelPotential;
        Canvas<real, float>                 canvas_V;
        Canvas<real, complex>               canvas_dV;
        
    public:
        Shared( ConfigReader& config, int threadCount ) :
            Instance::Shared( config, threadCount ),
            canvas_V( config, 0.0, "canvas_V" ),
            canvas_dV( config, 0.0, "canvas_dV" )
        {}
    };
    
    PendelPotential( ConfigReader& config ):
        Instance()
    {
        config.setSection("pendel");
        
        m_pendel.init( config );
    }
    
    virtual void run() {
        Shared* s = static_cast<Shared*>( m_shared );
        
        int width = s->canvas_V.width();
        int height = s->canvas_V.height();
        
        state_t p;
        p[2] = 0.0;
        p[3] = 0.0;
        
        for( int px = 0; px < width; px++) {
            real x = s->canvas_V.xValue( px );
            for( int py = m_thread; py < height; py += m_threadCount ) {
                real y = s->canvas_V.yValue( py );
                
                s->canvas_V.addXY( px, py, m_pendel.V( x, y ) );
                
                p[0] = x;
                p[1] = y;
                
                state_t dpdt;
                
                m_pendel( p, dpdt, 0.0 );
                s->canvas_dV.addXY( px, py, complex( dpdt[2], dpdt[3] ) );
            }
        }
                
    }
    
    void finish( ConfigReader& config ) {
        Shared* s = static_cast<Shared*>( m_shared );
        s->canvas_V.write();
        s->canvas_dV.write();
    }
};

#endif
