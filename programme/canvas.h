/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CANVAS_H
#define CANVAS_H
#include <fstream>
#include <string>
#include <functional>
#include <type_traits>
#include <endian.h>
#include <boost/concept_check.hpp>
#include <iostream>
#include "typeinfo.h"
#include "configreader.h"
#include "numpyarray.h"
#include <cmath>

#ifndef CANVAS_MAX_SIZE
#define CANVAS_MAX_SIZE 100000
#endif

#ifndef CANVAS_TRACK_MAXVAL
#define CANVAS_TRACK_MAXVAL 0
#endif

#ifndef CANVAS_ENABLE_RESIZE
#define CANVAS_ENABLE_RESIZE 0
#endif

template <class real>
class ComplexType
{
public:
    std::complex<real> value;
    
    ComplexType() : value( 0.0, 0.0 ) {};
    ComplexType( const std::complex<real>& c ) : value( c ) {};
    ComplexType( const real& x ) : value( x, 0.0 ) {};
    ComplexType( const ComplexType<real>& other ) : value( other.value ) {};
    
    real abs2()
    {
        return value.real() * value.real() + value.imag() * value.imag();
    }
    
    bool operator>( const ComplexType<real>& other )
    {
        return abs2() > other.abs2();
    }
    
    void operator+=( const ComplexType<real>& other )
    {
        value += other.value;
    }
    void operator+=( const std::complex<real>& c )
    {
        value += c;
    }
};

class CanvasException : public std::exception
{
   const char* s;
   
public:
   CanvasException(const char* s) : s(s) {}
   const char* what() const throw() { return s; }
};

/*!
 * \brief               2D Canvas Class
 * \param               real
 *                          class for real numbers used in the coordinate system (`float` or `double`)
 * \param               T
 *                          type of elements (`int` for indexing or single points, `float` for smoth lines)
 *                          using a floating point type here enables anti-aliasing for lines
 * 
 * \note                    Define `CANVAS_MAX_SIZE` to change the maximum size of the canvas
 * \note                    Define `CANVAS_TRACK_MAXVAL` to enable tracking of the maximum value
 * \note                    Define `CANVAS_ENABLE_RESIZE` to enable automatic resizing if values out of the
 *                          canvas rect are added. (Requires activation via `enableResize()`.)
 */
template <class real, typename T>
class Canvas
{
public:
    typedef boost::multi_array<T, 2> array_t;
    typedef real                coords_type;
    typedef T                   value_type;
    
protected:
    typedef typename std::conditional<std::is_floating_point<T>::value, real, int>::type position_t;
    
    array_t m_data;
    T m_defaultValue;
    int m_width;
    int m_height;
    real x_a;
    real x_b;
    real y_a;
    real y_b;
    
    position_t last_x;
    position_t last_y;
    
    std::string m_datafile;
    
#if CANVAS_TRACK_MAXVAL
    T m_maxValue;
#endif
    
#if CANVAS_ENABLE_RESIZE
    bool m_resize;
#endif
    
    void init( real x0, real x1, real y0, real y1, int width, int height, T fill ) {
        if( width < 0 || height < 0 )
            throw CanvasException("dimensions must not be negative");
        
        if( width > CANVAS_MAX_SIZE || height > CANVAS_MAX_SIZE )
            throw CanvasException("size to big");
        
        boost::array<size_t, 2> shape = {{ (size_t) height, (size_t) width }};
        
        m_width = width;
        m_height = height;
        m_data.resize( shape );
        
        for( int y = 0; y < height; y++ ) {
            for( int x = 0; x < width; x++ )
                m_data[y][x] = fill;
        }
        
        x_a = x0;
        x_b = width / (x1 - x0);
        
        y_a = y1;
        y_b = height / (y0 - y1);
        
        m_defaultValue = fill;
#if CANVAS_ENABLE_RESIZE
        m_resize = false;
#endif
#if CANVAS_TRACK_MAXVAL
        m_maxValue = 0.0;
#endif
    }
    
#if CANVAS_ENABLE_RESIZE
    virtual void updateSize() {}
    
    void resize( bool toLeft, bool toRight, bool toTop, bool toBottom )
    {
        std::cout << "resize " << toLeft << " " << toRight << " " << toTop << " " << toBottom << std::endl;
        
        if( (toTop || toBottom) && (m_height * 2 > CANVAS_MAX_SIZE ) )
            return;
        
        if( (toLeft || toRight) && (m_width * 2 > CANVAS_MAX_SIZE ) )
            return;
            
        if( toLeft ) {
            boost::array<size_t, 2> shape = {{ (size_t) m_height, (size_t) (m_width * 2) }};
            m_data.resize( shape );
            
            for( int x = 0; x < m_width; x++ ) {
                for( int y = 0; y < m_height; y++ )
                    std::swap( m_data[ y ][ x + m_width ], m_data[ y ][ x ] );
            }
            
            x_a -= m_width / x_b;
            m_width *= 2;
        }
        
        if( toRight ) {
            boost::array<size_t, 2> shape = {{ (size_t) m_height, (size_t) (m_width * 2) }};
            m_data.resize( shape );
            
            m_width *= 2;
        }
        
        if( toBottom ) {
            boost::array<size_t, 2> shape = {{ (size_t) (m_height * 2), (size_t) m_width }};
            m_data.resize( shape );
            
            for( int x = 0; x < m_width; x++ ) {
                for( int y = 0; y < m_height; y++ )
                    std::swap( m_data[ y + m_height ][ x ], m_data[ y ][ x ] );
            }
            
            y_a -= m_height / y_b;
            m_height *= 2;
        }
        
        if( toTop ) {
            boost::array<size_t, 2> shape = {{ (size_t) (m_height * 2), (size_t) m_width }};
            m_data.resize( shape );
            
            m_height *= 2;
        }
        
        updateSize();
    }
#endif
    
    // http://en.wikipedia.org/wiki/Xiaolin_Wu%27s_line_algorithm
    
    void plot( int x, int y, T value ) {
        if( x >= 0 && x < m_width && y >= 0 && y < m_height ) {
#if CANVAS_TRACK_MAXVAL
            T pv = (m_data[y][x] += value);
            if( pv > m_maxValue )
                m_maxValue = pv;
#else
            m_data[y][x] += value;
#endif
        }
#if CANVAS_ENABLE_RESIZE
        else if( m_resize ) {
            resize( x < 0, x >= m_width, y >= m_height, y < 0 );
            plot( x, y, value );
        }
#endif
    }
    
    real ipart( real x ) const {
        return floor( x );
    }
    
    real round( real x ) const {
        return ipart( x + 0.5 );
    }
    
    real fpart( real x ) const {
        return x - ipart( x );
    }
    
    real rfpart( real x ) const {
        return 1.0 - fpart( x );
    }
    
    void lineToImpl( real x, real y, T value, std::false_type ) {
        int x0 = last_x;
        int y0 = last_y;
        int x1 = (x - x_a) * x_b;
        int y1 = (y - y_a) * y_b;
        
        if( x0 == x1 && y0 == y1 )
            return;
        
        // save values
        last_x = x1;
        last_y = y1;
        
        int dx = x1 - x0;
        int dy = y1 - y0;

        int D = 2 * dy - dx;
        plot( x0, y0, value );
        int py = y0;

        for( int x = x0 + 1; x <= x1; x++ ) {
            if( D > 0 ) {
                py++;
                plot( x, py, value );
                D += 2 * dy - 2 * dx;
            } else {
                plot( x, py, value );
                D += 2 * dy;
            }
        }
    }
    
    void lineToImpl( real x, real y, T value, std::true_type ) {
        real x0 = last_x;
        real y0 = last_y;
        real x1 = (x - x_a) * x_b;
        real y1 = (y - y_a) * y_b;
        
        if( std::max( abs( x1 - x0 ), abs( y1 - y0 ) ) < 0.1 )
            return;
        
        // save values
        last_x = x1;
        last_y = y1;
        
        // http://en.wikipedia.org/wiki/Xiaolin_Wu%27s_line_algorithm
        
        bool steep = abs(y1 - y0) > abs(x1 - x0);
        
        if( steep ) {
            std::swap( x0, y0 );
            std::swap( x1, y1 );
        }
        
        if( x0 > x1 ) {
            std::swap( x0, x1 );
            std::swap( y0, y1 );
        }
        
        real dx = x1 - x0;
        real dy = y1 - y0;
        real gradient = dy / dx;
        
        // handle first endpoint
        real xend = round( x0 );
        real yend = y0 + gradient * ( xend - x0 );
        real xgap = rfpart( x0 + 0.5 );
        int xpxl1 = xend;   //this will be used in the main loop
        int ypxl1 = ipart( yend );
        
        if( steep ) {
            plot( ypxl1,   xpxl1,  value * rfpart( yend ) * xgap );
            plot( ypxl1+1, xpxl1,  value * fpart( yend ) * xgap );
        } else {
            plot( xpxl1, ypxl1,    value * rfpart( yend ) * xgap );
            plot( xpxl1, ypxl1+1,  value * fpart( yend ) * xgap );
        }
        
        real intery = yend + gradient; // first y-intersection for the main loop
    
        // handle second endpoint
    
        xend = round( x1 );
        yend = y1 + gradient * ( xend - x1 );
        xgap = fpart( x1 + 0.5 );
        int xpxl2 = xend; //this will be used in the main loop
        int ypxl2 = ipart( yend );
        
        if( steep ) {
            plot( ypxl2,   xpxl2,  value * rfpart( yend ) * xgap );
            plot( ypxl2+1, xpxl2,  value * fpart( yend ) * xgap );
        } else {
            plot( xpxl2, ypxl2,   value * rfpart( yend ) * xgap );
            plot( xpxl2, ypxl2+1, value * fpart( yend ) * xgap );
        }
        
        // main loop
    
        for( int x = xpxl1 + 1; x < xpxl2; x++ ) {
            if( steep ) {
                plot( ipart(intery)  , x, value * rfpart(intery) );
                plot( ipart(intery)+1, x,  value * fpart(intery) );
            } else {
                plot( x, ipart (intery),  value * rfpart(intery) );
                plot( x, ipart (intery)+1, value * fpart(intery) );
            }
            intery += gradient;
        }
    }
    
public:
    /*!
     * \param               x0
     *                          x-value for the leftmost pixel
     * \param               x1
     *                          hightest x-value (excluded)
     * \param               width
     *                          width of the image
     * \param               fill
     *                          default value for all pixels
     * 
     * \note                    (x0 ... x1, y0 ... y1) describes the source coordinate system
     * \note                    (0 ... width-1, 0 ... height-1) describes the target coordinate system
     */
    Canvas( real x0, real x1, real y0, real y1, int width, int height, T fill )
    {
        init( x0, x1, y0, y1, width, height, fill );
    }
    
    /*!
     * \param               config
     *                          ConfigReader instance to load settings from
     * \param               fill
     *                          default value for all pixels
     * \param               section
     *                          name of the section where the settings should be taken from
     */
    Canvas( ConfigReader& config, T fill, const char* section = "canvas" )
    {
        config.setSection(section);
        init(
            config.get<real>("x0"),
            config.get<real>("x1"),
            config.get<real>("y0"),
            config.get<real>("y1"),
            config.get<int>("width"),
            config.get<int>("height"),
            fill
        );
        m_datafile = config.get<std::string>("data");
    }
    
    virtual ~Canvas() {
    }
    
    //! add value to the pixel at (x, y) in source coordinates
    void add( real x, real y, T value ) {
        int px = (x - x_a) * x_b;
        int py = (y - y_a) * y_b;
        
        plot( px, py, value );
    }
    
    //! add value to the pixel at (px, y) where px is in target coordinates and y in source coordinates
    void addX( int px, real y, T value ) {
        int py = (y - y_a) * y_b;
        
        plot( px, py, value );
    }
    
    //! add value to the pixel at (px, py) in target coordinates
    void addXY( int px, int py, T value ) {
        plot( px, py, value );
    }
    
    //! move cursor to x, y in source coordinates
    void moveTo( real x, real y ) {
        last_x = (x - x_a) * x_b;
        last_y = (y - y_a) * y_b;
    }
    
    /*!                         draw line from current cursor to (x, y) in source coordinates
     *                          cursor is moved to (x, y)
     * \note                    use a floating point type for T to enable smooth lines via
     *                          Xiaolin Wu's line algorithm
     */
    void lineTo( real x, real y, T value ) {
        lineToImpl( x, y, value, std::is_floating_point<T>() );
    }
    
    //! combine two canvas objects
    void operator+=( Canvas& other ) {
        if( m_width != other.m_width || m_height != other.m_height )
            throw CanvasException("canvas with is not the same");
        
        if( x_a != other.x_a || x_b != other.x_b || y_a != other.y_a || y_b != other.y_b )
            throw CanvasException("canvas height is not the same");
        
        for( int x = 0; x < m_width; x++ ) {
            for( int y = 0; y < m_height; y++ )
                m_data[ y ][ x ]  += other.m_data[ y ][ x ];
        }
    }
    
    //! combine two canvas objects
    Canvas operator+( Canvas& other ) {
        if( m_width != other.m_width || m_height != other.m_height )
            throw CanvasException("canvas with is not the same");
        
        if( x_a != other.x_a || x_b != other.x_b || y_a != other.y_a || y_b != other.y_b )
            throw CanvasException("canvas height is not the same");
        
        Canvas c( m_width, m_height );
        for( int x = 0; x < m_width; x++ ) {
            for( int y = 0; y < m_height; y++ )
                c.m_data[ y ][ x ] = m_data[ y ][ x ] + other.m_data[ y ][ x ];
        }
        
        return c;
    }
    
    //! write canvas data to a numpy (.npy) filestream
    void write( std::ofstream& file ) {
        NumPyArray<T, 2>::write( m_data, file );
    }
    
    //! write canvas data to a numpy (.npy) file
    void write( std::string filename ) {
        std::ofstream outfile( filename, std::ofstream::binary );
        write( outfile );
        outfile.close();
    }
    
    void write() {
        if( m_datafile.empty() )
            throw std::runtime_error("datafile not specified");
        
        write( m_datafile );
    }
    
#if CANVAS_ENABLE_RESIZE
    void writeInfo( std::ofstream& s ) {
        s << "[canvas]" << std::endl;
        s << "  x0 = " << std::scientific << x_a << std::endl;
        s << "  x1 = " << std::scientific << x_a + m_width / x_b << std::endl;
        s << "  y0 = " << std::scientific << y_a + m_height / y_b << std::endl;
        s << "  y1 = " << std::scientific << y_a << std::endl;
    }
    
    void writeInfo( std::string filename ) {
        std::ofstream s( filename );
        writeInfo( s );
        s.close();
    }
#endif
    
    int width() const {
        return m_width;
    }
    
    int height() const {
        return m_height;
    }
    
    real xValue( int x ) const {
        return x / x_b + x_a;
    }
    
    real yValue( int y ) const {
        return y / y_b + y_a;
    }
    
#if CANVAS_ENABLE_RESIZE
    void enableResize() {
        m_resize = true;
    }
    
    void clip( float xmargin = 0.02, float ymargin = 0.02 ) {
        int x0 = m_width;
        int x1 = 0;
        
        for( int y = 0; y < m_height; y++ ) {
            for( int x = 0; x < x0; x++ ) {
                if( m_data[ y ][ x ] != m_defaultValue ) {
                    x0 = x;
                }
            }
            
            for( int x = m_width - 1; x > x1; x-- ) {
                if( m_data[ y ][ x ] != m_defaultValue ) {
                    x1 = x;
                }
            }
        }
        
        if( x0 > x1 )
            x0 = x1;
        
        int y0 = m_height;
        int y1 = 0;
        
        for( int x = x0; x < x1; x++ ) {
            for( int y = 0; y < y0; y++ ) {
                if( m_data[ y ][ x ] != m_defaultValue ) {
                    y0 = y;
                }
            }
            
            for( int y = m_height - 1; y > y1; y-- ) {
                if( m_data[ y ][ x ] != m_defaultValue ) {
                    y1 = y;
                }
            }
        }
        
        if( y0 > y1 )
            y0 = y1;
        
        int new_width = x1 - x0;
        int new_height = y1 - y0;
        
        x0 = std::max<int>( 0, x0 - new_width * xmargin );
        x1 = std::min<int>( m_width - 1, x1 + new_width * xmargin );
        y0 = std::max<int>( 0, y0 - new_height * ymargin );
        y1 = std::min<int>( m_height - 1, y1 + new_height * ymargin );
        
        if( x0 > 0 || x1 < (m_width-1) || y0 > 0 || y1 < (m_height-1) ) {
            new_width = x1 - x0 + 1;
            new_height = y1 - y0 + 1;
            
            for( int x = 0; x < new_width; x++ ) {
                for( int y = 0; y < new_height; y++ ) {
                    m_data[ y ][ x ] = m_data[ y + y0 ][ x + x0 ];
                }
            }
            
            boost::array<size_t, 2> shape = {{ (size_t) new_height, (size_t) new_width }};
            m_data.resize( shape );
            
            m_width = new_width;
            m_height = new_height;
            
            x_a += x0 / x_b;
            y_a += y0 / y_b;
        }
    }
#endif
    
    const array_t& data() {
        return m_data;
    }
    
    void testpattern_linearX( T min, T max )
    {
        double _s = (max - min) / m_width;
        for( int x = 0; x < m_width; x++ )
            m_data[ 0 ][ x ] = min + x * _s;
    }
    
#if CANVAS_TRACK_MAXVAL
    T max() const {
        return m_maxValue;
    }
#endif
};

#endif // CANVAS_H
