import sys, os

prog = """
/draw {
  0                                     % array 0
  1 index                               % array 0 array
  length 2 idiv                          % array 0 length/2
  {
                                        % array 2i
    2 copy                              % array 2i array 2i
    get                                 % array 2i array[2i]
    width mul                           % array 2i (array[2i]*width)
    2 index                             % array 2i (array[2i]*width) array
    2 index                             % array 2i (array[2i]*width) array 2i
    1 add                               % array 2i (array[2i]*width) array (2i+1)
    get                                 % array 2i (array[2i]*width) array[2i+1]
    height mul                          % array 2i (array[2i]*width) (array[2i+1]*width)

    newpath
    0.1 0 360 arc                        % array 2i 
    fill

    2 add                               % array (2i+2)
  } repeat
  
} def
"""

mm = 72/25.6
cm = 10 * mm

width=12.*cm
height=12.*cm

x0 = 0.7
x1 = 1.9
y0 = -3.7
y1 = 4.

psname = sys.argv[2]
fdata = open(sys.argv[1])
fps = open(psname, "wt")

fps.write("""\
%%!PS-Adobe EPSF-3.0
%%%%BoundingBox: 0 0 %f %f

/width %f def
/height %f def
""" % (width, height, width, height))
fps.write(prog)
fps.write("/data [\n")

for line in fdata.readlines():
  x, y = (float(s) for s in line.split())
  cx = (x - x0) / (x1 - x0)
  cy = (y - y0) / (y1 - y0)
  fps.write("%f %f\n" % (cx, cy))

fps.write("] def\n")
fps.write("data draw\n")
fps.close()

#os.exec("ps2pdf %s %s" % (psname, pdfname))
