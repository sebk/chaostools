#include <iostream>

#include "configreader.h"
#include "dataprocessor.h"

#include <QApplication>
#include <QMainWindow>
#include <QPainter>
#include <QKeyEvent>
#include <QTime>
#include <QTimer>
#include <QThread>
#include <qwt6/qwt_raster_data.h>
#include <qwt6/qwt_plot.h>
#include <qwt6/qwt_plot_spectrogram.h>
#include <qwt6/qwt_color_map.h>
#include <qwt6/qwt_scale_engine.h>
#include <qwt6/qwt_scale_widget.h>
#include <qwt6/qwt_plot_zoomer.h>
#include <qwt6/qwt_plot_magnifier.h>
#include <qwt6/qwt_plot_marker.h>


class RootColorMap : public QwtLinearColorMap
{
    double      power;
    
public:
    RootColorMap(const QColor &from, const QColor &to)
        : QwtLinearColorMap(from, to)
    {
        setRoot( 2 );
    }

    void setRoot( int n ) {
        power = 1.0 / n;
    }
    
    QRgb rgb(const QwtInterval &interval, double value) const
    {
        return QwtLinearColorMap::rgb(
            QwtInterval(
                std::pow( interval.minValue(), power ),
                std::pow( interval.maxValue(), power )
            ),
            std::pow( value, power )
        );
    }
};

class HeatColorMap : public RootColorMap {
public:
    HeatColorMap() : RootColorMap( QColor(Qt::black), QColor(Qt::white) )
    {
        addColorStop( 1.0/3.0, QColor(Qt::red) );
        addColorStop( 2.0/3.0, QColor(Qt::yellow) );
    }
};

class PlotZoomer: public QwtPlotZoomer
{
public:
    PlotZoomer( QWidget *canvas ):
        QwtPlotZoomer( canvas )
    {
        setTrackerMode( AlwaysOn );
        setRubberBandPen( QPen( QColor( Qt::green ) ) );
    }

    virtual QwtText trackerTextF( const QPointF &pos ) const
    {
        QColor fg( Qt::green );

        QwtText text = QwtPlotZoomer::trackerTextF( pos );
        text.setColor( fg );
        return text;
    }
};

class FeigenbaumView : public QMainWindow
{
    Q_OBJECT
    
    QwtPlot                                     m_plot;
    QwtPlotSpectrogram                          m_spectogram;
    PlotZoomer*                                 m_zoomer;
    QwtPlotMarker                               m_marker;
    
    DataProcesser                               m_processer;
    
    QTimer                                      m_plotupdate;
    
    double                                      m_lastXPos;
    int                                         m_updates;
    
    ConfigReader&                               m_config;
    
private slots:
    void dataChanged( /*QwtRasterData* data*/ double voltage )
    {
        m_lastXPos = voltage;
        m_updates = m_updates >= 0 ? m_updates + 1: 1;
    }
    
    void updatePlot()
    {
        if( m_updates > 0 ) {
            m_updates = 0;
            
            QStack<QRectF> zoomStack( m_zoomer->zoomStack() );
            uint index = m_zoomer->zoomRectIndex();
            
            QwtInterval xInterval = m_spectogram.interval(Qt::XAxis);
            QwtInterval yInterval = m_spectogram.interval(Qt::YAxis);
            QwtInterval zInterval = m_spectogram.interval(Qt::ZAxis);
            
            if( zInterval.maxValue() > 500 )
                zInterval.setMaxValue( 500 );
            
            zoomStack[0].setCoords(
                xInterval.minValue(), yInterval.minValue(),
                xInterval.maxValue(), yInterval.maxValue()
            );
            
            //m_plot->setAxisScale( QwtPlot::xBottom, xInterval.minValue(), xInterval.maxValue() );
            //m_plot->setAxisScale( QwtPlot::yLeft, yInterval.minValue(), yInterval.maxValue() );
            m_plot.setAxisScale( QwtPlot::yRight, zInterval.minValue(), zInterval.maxValue() );
            m_plot.axisWidget( QwtPlot::yRight )->setColorMap( zInterval, new HeatColorMap() );
            
            m_zoomer->setZoomStack( zoomStack, index );
            
            m_marker.setXValue( m_lastXPos );
            m_marker.setVisible( true );
            
            m_plot.replot();
        } else if( m_updates < -30 ) {
            if( m_marker.isVisible() ) {
                m_marker.setVisible( false );
                m_plot.replot();
            }
        } else
            m_updates--;
    }
    
public:
    FeigenbaumView( ConfigReader& config ) :
        QMainWindow(),
        m_config( config )
    {
        setCentralWidget( &m_plot );
        m_spectogram.attach( &m_plot );
        m_spectogram.setRenderThreadCount( 1 );
        m_spectogram.setColorMap( new HeatColorMap() );
        
        //m_plot->setAxisScaleEngine( QwtPlot::yRight, new QwtLog10ScaleEngine() );
        QwtScaleWidget* y2Axis = m_plot.axisWidget( QwtPlot::yRight );
        y2Axis->setColorBarEnabled( true );
        y2Axis->setColorMap( QwtInterval( 0.0, 10.0 ), new HeatColorMap() );
        m_plot.enableAxis( QwtPlot::yRight );
        m_plot.setAxisScale( QwtPlot::yRight, 0.0, 10 );
        
        m_zoomer = new PlotZoomer( m_plot.canvas() );
        m_marker.setLineStyle( QwtPlotMarker::VLine );
        m_marker.setLinePen( QPen( QColor( 0, 255, 0, 100 ) ) );
        m_marker.attach( &m_plot );
        
        connect( &m_processer, SIGNAL(dataChanged(double)), SLOT(dataChanged(double)) );
        
        m_plotupdate.setInterval( 100 );
        m_plotupdate.setSingleShot( false );
        connect( &m_plotupdate, SIGNAL(timeout()), SLOT(updatePlot()) );
    }
    
    ~FeigenbaumView()
    {
    }
    
    void start()
    {
        m_plot.replot();
        
        m_spectogram.setData( m_processer.init( m_config ) );
        
        m_processer.start();
        m_plotupdate.start();
    }
    
protected:
    virtual void closeEvent( QCloseEvent* event )
    {
        m_processer.stop();
        m_processer.wait( 3 );
        m_processer.save();
        m_processer.terminate();
        
        QWidget::closeEvent( event );
    }
};

int main( int argc, char** argv )
{
    QApplication app( argc, argv );
    
    ConfigReader config;
    
    for( int i = 1; i < argc; i++ )
        config.addFile( argv[i] );
    
    FeigenbaumView v( config );
    v.show();
    v.start();
    
    return app.exec();
}

#include "moc_feigenbaumview.h"

