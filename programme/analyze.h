/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/multi_array.hpp>
#include <vector>

template <class real>
class Analyze {
public:
    typedef boost::const_multi_array_ref<real, 2>      array_ref;

    typedef struct {
        real min;
        real max;
        real delta;
    } channelInfo_t;

    enum edgeType {
        None = 0,
        RisingEdge = 1,
        FallingEdge = 2,
        BothEdges = 3
    };

    static void analyzeChannel( const array_ref &ref, std::size_t channel, channelInfo_t& info )
    {
        std::size_t N = ref.shape()[0];

        std::vector<real> vec( N );

        for( size_t n = 0; n < N; n++ )
            vec[n] = ref[n][channel];

        std::sort( vec.begin(), vec.end() );

        info.min = vec[0];
        info.max = vec[N - 1];

        real deltaMin = info.max - info.min;
        real lastValue = info.min;
        for( size_t n = 1; n < N; n++ ) {
            real v = vec[ n ];
            if( v > lastValue ) {
                real d = v - lastValue;
                if( d < deltaMin )
                    deltaMin = d;

                lastValue = v;
            }
        }

        info.delta = deltaMin;
    }

    static void findTriggerSlopes( std::vector<real>& slopes,
                                   const array_ref& ref,
                                   std::size_t channel,
                                   real threshold,
                                   edgeType slope = RisingEdge )
    {
        edgeType lastEdge = (ref[0][channel] > threshold) ? RisingEdge : FallingEdge;

        // which slope to ignore
        edgeType ignoreEdge = None; // none
        if( slope == RisingEdge )
            ignoreEdge = FallingEdge; // ignore falling
        else if( slope == FallingEdge )
            ignoreEdge = RisingEdge; // ignore rising edge

        std::size_t N = ref.shape()[0];

        for( std::size_t n = 1; n < N; n++ ) {
            double trigger = ref[n][channel];
            edgeType edge = (trigger > threshold) ? RisingEdge : FallingEdge;
            if( edge != lastEdge ) {
                if( edge != ignoreEdge ) {
                    double y0 = ref[n-1][channel] - threshold;
                    double y1 = ref[n][channel] - threshold;
                    double slopeTime = n + y1 / (y0 - y1);
                    slopes.push_back( slopeTime );
                }
                lastEdge = edge;
            }
        }
    }
};
