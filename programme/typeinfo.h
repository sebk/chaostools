/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPEINFO_H
#define TYPEINFO_H


#if __BYTE_ORDER == __LITTLE_ENDIAN
#define ENDIAN_STR "<"
#else
#define ENDIAN_STR ">"
#endif

#include <inttypes.h>
#include <complex>

template <typename T>
class TypeInfo {
public:
};


#define declareType(TYPE, NAME, NPY_NAME) \
template <> \
class TypeInfo<TYPE> { \
public: \
    static const char* name( ) { return NAME; } \
    static const char* numpyName() { return NPY_NAME; } \
    static size_t size() { return sizeof( TYPE ); } \
};

declareType( int32_t, "int32", "i4" )
declareType( int16_t, "int16", "i2" )
declareType( int8_t, "int8", "i1" )
declareType( uint8_t, "uint8", "u1" )
declareType( float, "float32", "f4" )
declareType( double, "float64", "f8" )
declareType( std::complex<float>, "complex64", "c8" )
declareType( std::complex<double>, "complex128", "c16" )

#endif
