#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <boost/iterator/iterator_concepts.hpp>

double iter( double r, double x, int n ) {
    while( n-- ) {
        x *= r * (1.0 - x);
    }
    return x;
}

const int start_iter = 1001;
const int N = 4000;
const int M = 16;
const int K = 64;
const double r_s = 4.0 / N;
const double toleranceA = 1e-10;
const double toleranceB = 5e-11;

#define PREC "%.10f"

bool isStable( double r, double x, int n ) {
    double x_try = x;
    for( int k = 0; k < K; k++ ) {
        x_try = iter( r, x_try, n );
        if( fabs( x_try - x ) > toleranceA ) {
            return false;
        }
    }
    
    return true;
}

int testR( double r, double x ) {
    x = iter( r, x, start_iter );
    
    double x_test = x;
    for( int m = 1; m <= 16; m++ ) {
        x = iter( r, x, 1 );
        if( fabs( x - x_test ) < toleranceB ) {
            if( isStable( r, x, m ) ) {
                return m;
            }
        }
    }
    
    return 0;
}

void find( double r, double x, double dr, int n1, int n2 ) {
    printf(PREC " " PREC " %i %i\n", r, r + dr, n1, n2 );
    dr *= 0.5;
    
    while( dr > toleranceB ) {
        int n = testR( r + dr, x );
        printf("  " PREC " %i\n", r + dr, n);
        if( n == n1 )
            r += dr;
        else if( n != n2 )
            return;
        
        dr *= 0.5;
    }
    
    printf(PREC " %i %i\n", r, n1, n2 );
}

int main() {
    int lastM = 0;
    for( int n = 0; n < N; n++ ) {
        
        double r = r_s * n;
        double x = 0.332143243245324;
        
        int m = testR( r, x );
        if( n && m && (m != lastM) ) {
            find( r_s * (n-1), x, r_s, lastM, m );
            lastM = m;
        }
    }
}
