import sys

def add(l_ar, f, off):
    for line in open(f).readlines():
        line = line.strip()
        if not line: continue
        if line.startswith("#"):
            continue
        
        l_arr.append(float(line.split()[0]) + off)
    

l_arr = []
add(l_arr, "/tmp/peak.dat.old", 0.00005)
add(l_arr, "/tmp/peak.dat", 0.)

for i in range(len(l_arr) - 2):
    a, b, c = l_arr[i], l_arr[i+1], l_arr[i+2]
    
    print( (b-a) / (c-b) )
