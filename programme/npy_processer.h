/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NPY_PROCESSOR_H
#define NPY_PROCESSOR_H

#include "instance.h"
#include <list>
#include <boost/filesystem.hpp>
#include "numpyarray.h"
#include "canvas.h"
#include "analyze.h"

class NpyProcessor : public Instance
{
protected:
    size_t      m_timeChannel;
    size_t      m_XChannel;
    size_t      m_YChannel;
    size_t      m_triggerChannel;
    double      m_triggerThreshold;
    double      m_phaseshift;
   
public:
    static const uint maxThreads = 1;
    
    typedef NumPyArray<double, 2>       npArray_t;
    typedef npArray_t::array_t          array_t;
    
    typedef Analyze<double>::edgeType   edgeType;               
    
    template <class real = double>
    static void findTriggerSlopes( std::vector<real>& slopes,
                            const array_t::const_multi_array_ref& ref,
                            size_t channel,
                            real threshold,
                            edgeType slope = edgeType::RisingEdge )
    {
        Analyze<real>::findTriggerSlopes( slopes, ref, channel, threshold, slope );
    }
 
protected:
    
    template <class real = double, class T = float>
    Canvas<real, T>* createCanvasForArray( array_t::const_multi_array_ref &ref, double margin = 0.1 )
    {
        typename Analyze<real>::channelInfo_t xInfo, yInfo;
        
        Analyze<real>::analyzeChannel( ref, m_XChannel, xInfo );
        Analyze<real>::analyzeChannel( ref, m_YChannel, yInfo );
        
        xInfo.max += xInfo.delta;
        yInfo.max += yInfo.delta;
        
        int width = (xInfo.max - xInfo.min) / xInfo.delta + 0.5;
        int height = (yInfo.max - yInfo.min) / yInfo.delta + 0.5;
        int xMargin = width * margin;
        int yMargin = height * margin;
        
        real x0 = xInfo.min - xMargin * xInfo.delta + xInfo.delta * 0.5;
        real x1 = xInfo.max + xMargin * xInfo.delta + xInfo.delta * 0.5;
        real y0 = yInfo.min - yMargin * yInfo.delta + yInfo.delta * 0.5;
        real y1 = yInfo.max + yMargin * yInfo.delta + yInfo.delta * 0.5;
        
        return new Canvas<real, T>( x0, x1, y0, y1, width + 2 * xMargin, height + 2 * yMargin, 0.0 );
    }
    
    virtual void processArray( npArray_t::array_ptr array ) = 0;
    
    void processFile( const std::string& filename ) 
    {
        if( !filename.compare( filename.length() - 4, 4, ".npy" ) ) {
            std::cout << "processing " << filename << " ... " << std::flush;
            
            std::ifstream stream( filename );
            if( stream.is_open() ) {
                npArray_t::array_ptr array = npArray_t::readNpy( stream );
                processArray( array );
                std::cout << "done";
            } else
                std::cout << "could not open";
                
            std::cout << std::endl;
            
        } else if( !filename.compare( filename.length() - 4, 4, ".npz" ) ) {
            std::cout << "processing " << filename << " ... " << std::flush;
            
            std::ifstream stream( filename );
            if( stream.is_open() ) {
                std::cout << std::endl;
                
                std::vector<npArray_t::npzEntry> entryVector = npArray_t::readNpz( stream );
                std::sort( entryVector.begin(), entryVector.end() );
                
                for( auto it = entryVector.begin(); it != entryVector.end(); it++ ) {
                    // std::cout << "  - " << it->name << "  ";
                    processArray( it->array );
                    // std::cout << std::endl;
                }
            } else
                std::cout << "could not open" << std::endl;
        }
    }
    
    void scanDirectory( const boost::filesystem::path& dir )
    {
        std::cout << "directory " << dir << std::endl;
        
        std::vector<boost::filesystem::directory_entry> entries;
        std::copy( boost::filesystem::directory_iterator( dir ),
                   boost::filesystem::directory_iterator(),
                   std::back_inserter( entries )
                 );
        
        std::sort( entries.begin(), entries.end() );
        
        for( auto it = entries.begin(); it != entries.end(); it++ ) {
            addEntry( *it );
        }
    }
    
    void addEntry( boost::filesystem::directory_entry& entry )
    {
        boost::filesystem::file_status status = entry.status();
        switch( status.type() ) {
            case boost::filesystem::regular_file:
                processFile( entry.path().string<std::string>() );
                break;
            case boost::filesystem::directory_file:
                scanDirectory( entry.path() );
                break;
            default:
                break;
        }
    }
    
    void addFilename( const std::string& filename )
    {
        boost::filesystem::directory_entry entry( filename );
        addEntry( entry );
    }
    
public:
    
    NpyProcessor( ConfigReader& config ) : Instance()
    {
        config.setSection( "npy" );
        m_timeChannel = config.get<size_t>("time");
        m_XChannel = config.get<size_t>("x");
        m_YChannel = config.get<size_t>("y");
        m_triggerChannel = config.get<size_t>("trigger");
        m_triggerThreshold = config.get<double>("threshold");
        
        double shift = config.get<double>("phase", 0.0) / (2.0*M_PI);
        m_phaseshift = shift - floor(shift);
    }
    
};

#endif
