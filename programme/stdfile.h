/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STDFILE_H
#define STDFILE_H

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <assert.h>
#include <stdexcept>

class StdFile {
    FILE*           m_fp;
    __off64_t       m_position;
    __off64_t       m_size;

    std::size_t     m_reads;
    std::size_t     m_writes;
    std::size_t     m_seeks;

public:
    
    StdFile()
    {
        m_reads = 0;
        m_writes = 0;
        m_seeks = 0;
        
        m_fp = NULL;
    }
    
    ~StdFile()
    {
        if( m_fp )
            ::fclose( m_fp );
        
        std::cout << "File:" << std::endl;
        std::cout << "  reads:  " << std::setw(10) << m_reads << std::endl;
        std::cout << "  writes: " << std::setw(10) << m_writes << std::endl;
        std::cout << "  seeks:  " << std::setw(10) << m_seeks << std::endl;
    }
    
    void open( const char* filename, bool overwrite )
    {
        if( overwrite ) {
            m_fp = ::fopen64( filename, "w+b" );
            if( !m_fp )
                throw std::runtime_error("could not open file for writing");

            m_size = 0;
            m_position = 0;
        } else {
            m_fp = ::fopen64( filename, "r+b" );
            if( m_fp ) {
                ::fseeko64( m_fp, 0, SEEK_END );
                m_size = ftello64( m_fp );
                m_position = m_size;
            } else {
                m_fp = ::fopen64( filename, "w+b" );
                if( !m_fp )
                    throw std::runtime_error("could not open file for writing");

                m_size = 0;
                m_position = 0;
            }
        }
    }
    
    __off64_t size()
    {
        return m_size;
    }
    
    void _seek( __off64_t position )
    {
        assert( m_fp );
        assert( position >= 0 );
        if( position != m_position ) {
            ::fseeko64( m_fp, position, SEEK_SET );
            m_position = position;
            m_seeks++;
        }
    }

    void _read( char* data, __off64_t position, __off64_t size )
    {
        _seek( position );

        ::fread( data, size, 1, m_fp );

        m_position += size;
        if( m_position > m_size )
            m_size = m_position;

        m_reads++;
    }


    void _write( const char* data, __off64_t position, __off64_t size )
    {
        _seek( position );

        ::fwrite( data, size, 1, m_fp );

        m_position += size;
        if( m_position > m_size )
            m_size = m_position;

        m_writes++;
    }


    __off64_t _append( const void* data, __off64_t size )
    {
        _seek( m_size );

        __off64_t p = m_position;
        ::fwrite( data, size, 1, m_fp );

        m_position += size;
        m_size = m_position;

        m_writes++;

        return p;
    }
};

#endif
