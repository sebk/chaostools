/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CANVAS_GUI_H
#define CANVAS_GUI_H

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "canvas.h"

template <class T>
class LinearMap
{
protected:
    T        offset;
    T        scale;
    
public:
    LinearMap() {
        offset = 0.0;
        scale = 1.0;
    }
    
    void setRange( T min, T max )
    {
        offset = min;
        scale = 1.0 / (max - min);
    }
    
    T map( T value )
    {
        T v = (value - offset) * scale;
        if( v > 1.0 )
            v = 1.0;
        else if( v < 0.0 )
            v = 0.0;
        
        return v;
    }
};

template <class T>
class PowerMap : public LinearMap<T>
{
    T        power;
    
public:
    PowerMap() : LinearMap<T>() {
        power = 1.0;
    }
    
    void setIndex( int index ) {
        if( index > 0 )
            power = index;
        else if( index < 0 )
            power = -1.0 / index;
        else
            power = 1.0;
    }
    
    T map( T value )
    {
        return pow( LinearMap<T>::map( value ), power );
    }
};

template <class Canvas_t = Canvas<double, float>>
class CanvasGUI
{
    typedef /*typename Canvas_t::value_type*/ double       value_t;
    
    Display*    display;
    Window      window; 
    Visual*     visual; 
    XImage*     image;
    GC          gc;
    int         screen;
    int         depth;
    
    Canvas_t*   canvas;
    
    void createWindow( int width, int height )
    {
        Window root = DefaultRootWindow( display );
        window= XCreateSimpleWindow( display, root,
                            0, 0, width, height, 0, 
                            BlackPixel(display, screen), 
                            WhitePixel(display, screen) );
        
        XMapWindow( display, window ); 
    }
    
    typedef struct {
        uint8_t blue;
        uint8_t green;
        uint8_t red;
    } pixel_t;
    
    void colorMap( pixel_t* pixel, value_t value )
    {
        // 0/3 -> black
        // 1/3 -> red
        // 2/3 -> yellow
        // 3/3 -> white
        
        value *= 3.;
        
        if( value < 1.0 ) {
            pixel->red = 255 * value;
            pixel->green = 0;
            pixel->blue = 0;
        } else if( value < 2.0 ) {
            pixel->red = 255;
            pixel->green = 255 * (value - 1.0);
            pixel->blue = 0;
        } else {
            pixel->red = 255;
            pixel->green = 255;
            pixel->blue = 255 * (value - 2.0);
        }
    }
    
public:
    PowerMap<value_t> map;
    
    CanvasGUI( Canvas_t* canvas ) : canvas( canvas )
    {
        display = XOpenDisplay( NULL );
        screen = DefaultScreen( display );
        visual = DefaultVisual( display, screen ); 
        depth  = DefaultDepth( display, screen );
        image = NULL;
        
        gc = DefaultGC(display,screen);
        
        createWindow( canvas->width(), canvas->height() );
    }
    
    ~CanvasGUI()
    {
        if( image )
            XDestroyImage(image);
        
        XDestroyWindow( display, window );
        XCloseDisplay( display );
    }
    
    void update()
    {
        int cwidth = canvas->width();
        int cheight = canvas->height();
        
        if( image && (image->width != cwidth || image->height != cheight) ) {
            XDestroyImage( image );
            image = NULL;
            
            XDestroyWindow( display, window );
            createWindow( cwidth, cheight );
        }
        
        if( image == NULL ) {
            char* data = (char*) malloc( cwidth * cheight * 4 ); // Allocate new drawing area
            image = XCreateImage( display, visual, depth, ZPixmap, 0, data, cwidth, cheight, 32, 0);
        }
        
        uint8_t* ptr = (uint8_t*) image->data;
        auto array = canvas->data();
        for( int y = 0; y < cheight; y++ ) {
            for( int x = 0; x < cwidth; x++ ) {
                float value = array[ y ][ x ];
                colorMap( (pixel_t*) ptr, map.map( value ) );
                ptr += 4;
            }
        }
        
        XPutImage( display, window, gc, image, 0, 0, 0, 0, cwidth, cheight );
        XFlush( display );
    }
};

#endif