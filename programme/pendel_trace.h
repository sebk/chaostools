/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_PENDEL_TRACE
#endif

#ifdef USE_PENDEL_TRACE
#define MAIN_CLASS PendelTrace

#include "canvas.h"
#include "instance.h"
#include "pool.h"
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/integrate/integrate_times.hpp>
#include <functional>
#include "configreader.h"
#include "pendel.h"

class PendelTrace : public Instance {
public:
    static const uint maxThreads = 1;
    
    typedef double real;
    typedef Canvas<real, float>       canvas_t;
    typedef boost::array<real, 4>     state_t;
    
    PendelTrace( ConfigReader& config ):
        Instance(),
        m_canvas( config, 0 )
    {
        config.setSection("pendel");
        
        m_pendel.init( config );
        
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        m_state[ 0 ] = config.get<real>("x0");
        m_state[ 1 ] = config.get<real>("y0");
    }
    
    virtual void run() {
        m_canvas.moveTo( m_state[0], m_state[1] );
        
        boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real> stepper;
        
        auto callback = std::bind(
            &PendelTrace::addLine, this, std::placeholders::_1, std::placeholders::_2 );
        
        boost::numeric::odeint::integrate_adaptive( stepper, m_pendel, m_state, m_t0, m_t1, m_dt, callback );
        setProgress( 1.0 );
    }
    
    void addLine( state_t& state, real t ) {
        m_canvas.lineTo( state[0], state[1], 1 );
        setProgress( (t - m_t0) / (m_t1 - m_t0) );
    }
    
    void collect( PendelTrace& other ) {
        m_canvas += other.m_canvas;
    }
    
    void finish( ConfigReader& config ) {
        m_canvas.write();
    }
    
private:
    PendelGl<real>      m_pendel;
    real                m_t0;
    real                m_t1;
    real                m_dt;
    state_t             m_state;
    canvas_t            m_canvas;
};

#endif