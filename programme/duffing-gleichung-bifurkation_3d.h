/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_DUFFING_GL_BIFURK_3D
#endif

#ifdef USE_DUFFING_GL_BIFURK_3D
#define MAIN_CLASS DuffingGleichungBifurkation3D

#include "instance.h"
#include "pool.h"
#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <functional>
#include <mutex>
#include <iomanip>
#include "configreader.h"
#include "duffing.h"
#include "statetools.h"

#define MAX_PERIODS 256

class DuffingGleichungBifurkation3D : public Instance {
public:
    typedef double real;
    typedef boost::array<real, 2>       state_t;
    
    typedef struct {
        float       x;
        float       y;
        float       z;
        float       velocity;
        float       angle;
        float       periodicity;
    } __attribute__((packed)) voxel_t;
        
    class Shared : public Instance::Shared {
        friend class DuffingGleichungBifurkation3D;
        
        std::vector<voxel_t>    m_data;
        std::mutex              m_mutex;
        std::ofstream           m_stream;
        
        uint                    m_cacheSize;
        uint                    m_pointsWritten;
        
        void writeHeader( bool restore = true )
        {
            std::streampos p = m_stream.tellp();
            
            m_stream.seekp( 0, std::ofstream::beg );
            
            m_stream << "VERSION 0.7\n"
                        "FIELDS x y z velocity angle periodicity\n"
                        "SIZE 4 4 4 4 4 4\n"
                        "TYPE F F F F F U\n"
                        "COUNT 1 1 1 1 1 1\n";
            m_stream << "WIDTH " << std::setw(12) << m_pointsWritten << "\n";
            m_stream << "HEIGHT 1\n"
                        "VIEWPOINT 0 0 0 1 0 0 0\n";
            m_stream << "POINTS " << std::setw(12) << m_pointsWritten << "\n";
            m_stream << "DATA binary\n";
            
            if( restore )
                m_stream.seekp( p );
        }
        
        void clearCache()
        {
            m_pointsWritten += m_data.size();
            m_stream.write( (const char*) &m_data[0], sizeof( voxel_t ) * m_data.size() );
            m_data.clear();
        }
        
    public:
        Shared( ConfigReader& config, uint threadCount ) :
            Instance::Shared( config, threadCount ),
            m_stream( config.default_get<std::string>( "data" ) )
        {
            m_cacheSize = config.default_get<uint>("cachesize");
            m_pointsWritten = 0;
            m_data.reserve( m_cacheSize );
            writeHeader( false );
        };
        
        ~Shared()
        {
            clearCache();
            writeHeader();
        }
        
        void addPoint( const voxel_t& v )
        {
            m_mutex.lock();
            if( m_data.size() == m_cacheSize ) {
                clearCache();
                writeHeader();
            }
            
            m_data.push_back( v );
            m_mutex.unlock();
        }
    };
    
    DuffingGleichungBifurkation3D( ConfigReader& config ):
        Instance()
    {
        config.setSection("duffing");
        m_duffing.init( config );
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        m_x0 = config.get<real>("x0");
        m_y0 = config.get<real>("y0");
        m_phase = config.get<real>("phase");
        m_samples = config.get<int>("samples");
        m_points = config.get<int>("points");
        
        m_convergence = config.get<bool>("convergence", false);
        
        std::string paramName = config.get<std::string>("param");
        if( paramName == "epsilon" )
            m_param = &m_duffing.epsilon;
        else if( paramName == "lambda" )
            m_param = &m_duffing.lambda;
        else if( paramName == "alpha" )
            m_param = &m_duffing.alpha;
        else if( paramName == "beta" )
            m_param = &m_duffing.beta;
        else
            throw std::runtime_error("param invalid");
        
        m_param_min = config.get<real>("param_min");
        m_param_max = config.get<real>("param_max");
        m_e_stop = config.get<real>("e_stop");
        m_e_same = config.get<real>("e_same");
    }
    
public:
    virtual void run()
    {
        // boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real> stepper;
        boost::numeric::odeint::controlled_runge_kutta<boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real>> stepper;
        
        boost::random::mt19937_64 generator;
        boost::random::uniform_real_distribution<real> distributionA( -m_x0, m_x0 );
        boost::random::uniform_real_distribution<real> distributionB( -m_y0, m_y0 );
        
        real p_delta = (m_param_max - m_param_min) / m_samples;
        
        Shared* shared = static_cast<Shared*>( m_shared );
        
        for( int s = m_thread; s < m_samples && !m_stop; s += m_threadCount ) {
            *m_param = m_param_min + p_delta * s;
        
            state_t state;
            state[ 0 ] = distributionA( generator );
            state[ 1 ] = distributionB( generator );
            
            ValueTester<real, MAX_PERIODS, 2> tester1( m_e_stop );
            
            real increment = 2.0 * M_PI / m_duffing.Omega;
            real t_0 = m_phase / m_duffing.Omega;
            real t_1 = t_0 + increment;
            int stepCount = (m_t1 - m_t0) / increment;
            
            for( int step = 0; step < stepCount; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt );
                
                if( tester1.test( state ) )
                    break;
            }
            
            int p = 0;
            
            ValueTester<real, MAX_PERIODS, 2> tester2( m_e_same );
            for( int step = 0; step < MAX_PERIODS && !p; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt );
                p = tester2.test( state );
            }
            
            for( int step = 0; step < (p ? p : m_points); step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, state, t_0, t_1, m_dt );
                
                voxel_t vox;
                vox.x = state[0];
                vox.y = state[1];
                vox.z = *m_param;
                
                state_t dxdt;
                m_duffing( state, dxdt, t_0 );
                
                vox.velocity = sqrt( dxdt[0] * dxdt[0] + dxdt[1] * dxdt[1] );
                vox.angle = atan2( dxdt[1], dxdt[0] );
                vox.periodicity = p ? log2( p ) : log2( MAX_PERIODS );
                
                shared->addPoint( vox );
            }
            
            setProgress( s, m_samples );
        }
        setProgress( 1.0 );
    }
    
    void collect( DuffingGleichungBifurkation3D& other ) {
    }
    
    void finish( ConfigReader& config ) {
    }
    
private:
    DuffingGl<real>     m_duffing;
    real                m_t0;
    real                m_t1;
    real                m_dt;
    bool                m_convergence;
    real                m_x0;
    real                m_y0;
    real                m_phase;
    int                 m_samples;
    int                 m_points;
    
    real                m_e_stop;
    real                m_e_same;
    
    real*               m_param;
    real                m_param_min;
    real                m_param_max;
};

#endif
