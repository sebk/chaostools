/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_PENDEL_CONV
#endif

#ifdef USE_PENDEL_CONV
#define MAIN_CLASS PendelConvergence

#include "canvas.h"
#include "instance.h"
#include "pool.h"
#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/integrate/integrate_times.hpp>
#include <condition_variable>
#include "configreader.h"
#include "pendel.h"
#include "statetools.h"

#define ERROR_STEPPER 1

class PendelConvergence : public Instance {
    
public:
    typedef double real;
    typedef Canvas<double, float>       canvasA_t;
    typedef Canvas<double, int8_t>      canvasB_t;
    typedef boost::array<real, 4>       state_t;
    
    class Shared : public Instance::Shared {
        friend class PendelConvergence;
        canvasA_t               canvasA;
        canvasB_t               canvasB;
        
    public:
        Shared( ConfigReader& config, int threadCount ) :
            Instance::Shared( config, threadCount ),
            canvasA( config, 0, "canvasA" ),
            canvasB( config, 0, "canvasB" )
        {
        }
        
        ~Shared()
        {
        }
    };
    
protected:
    real* getParam( const std::string& name )
    {
        if( name == "k" )
            return &m_pendel.k;
        //else if( name == "L" )
        //    return &m_pendel.L;
        else if( name == "x0" )
            return &m_initstate[ 0 ];
        else if( name == "y0" )
            return &m_initstate[ 1 ];
        else
            throw std::runtime_error("param invalid");
    }
    
public:
    PendelConvergence( ConfigReader& config ):
        Instance()
    {
        config.setSection("pendel");
        m_pendel.init( config );
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        
        m_initstate[ 0 ] = config.get<real>("x0");
        m_initstate[ 1 ] = config.get<real>("y0");
        m_initstate[ 2 ] = 0.0;
        m_initstate[ 3 ] = 0.0;
        
        m_param_x = getParam( config.get<std::string>("param_x") );
        m_param_y = getParam( config.get<std::string>("param_y") );
        
        m_epsilonStop = config.get<real>("e_stop");
    }
    
    real pow2( real x )
    {
        return x * x;
    }
    
    virtual void run()
    {
#if ERROR_STEPPER
        boost::numeric::odeint::controlled_runge_kutta<boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real>> stepper;
#else
        boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real> stepper;
#endif
        Shared* s = static_cast<Shared*>( m_shared );
        int width = s->canvasA.width();
        int height = s->canvasA.height();
        
        real stepSizeSum = 0.0;
        real minStepSize = m_t1;
        real maxStepSize = 0.0;
        uint   computations = 0;
        for( int px = 0; px < width; px++) {
            real x = s->canvasA.xValue( px );
            for( int py = m_thread; py < height; py += m_threadCount ) {
                real y = s->canvasA.yValue( py );
                
                *m_param_x = x;
                *m_param_y = y;
                
                state_t state = m_initstate;
                
                real dt = m_dt;
                real t = m_t0;
                
                const std::size_t max_attempts = 1000;
                while( t < m_t1 ) {
#if ERROR_STEPPER
                    std::size_t trials = 0;
                    boost::numeric::odeint::controlled_step_result res = boost::numeric::odeint::success;
                    do
                    {
                        res = stepper.try_step( m_pendel, state , t , dt );
                        ++trials;
                    }
                    while( ( res == boost::numeric::odeint::fail ) && ( trials < max_attempts ) );
                    if( trials == max_attempts )
                        continue;
#else
                        stepper.do_step( m_pendel, state, t, dt );
#endif
                    if( (pow2( state[2] ) + pow2( state[3] )) < pow2( m_epsilonStop ) ) {
                        state_t dpdt;
                        m_pendel( state, dpdt, 0.0 );
                        if( (pow2( state[2] ) + pow2( state[3] )) < pow2( m_epsilonStop ) ) {
                            int ident = m_pendel.identify(state);
                            s->canvasB.addXY( px, py, ident + 1 );
                            break;
                        }
                    }
                }
                
                s->canvasA.addXY( px, py, t );
                
                setProgress( px * height + py, width * height );
                
                stepSizeSum += dt;
                minStepSize = std::min( minStepSize, dt );
                maxStepSize = std::max( maxStepSize, dt );
                computations++;
            }
        }
        
        setProgress( 1.0 );
        
        std::cout << "step size:" << std::endl;
        std::cout << "  min:" << minStepSize << std::endl;
        std::cout << "  avg:" << stepSizeSum / computations << std::endl;
        std::cout << "  max:" << maxStepSize << std::endl;
    }
    
    void collect( PendelConvergence& other ) {
    }
    
    void finish( ConfigReader& config ) {
        Shared* s = static_cast<Shared*>( m_shared );
        s->canvasA.write();
        s->canvasB.write();
    }
    
private:
    PendelGl<real>      m_pendel;
    real                m_t0;
    real                m_t1;
    real                m_dt;
    real                m_epsilonStop;
    state_t             m_initstate;
    real*               m_param_x;
    real*               m_param_y;
};

#endif