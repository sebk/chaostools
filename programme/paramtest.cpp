#include <iostream>
#include "parameterspace.h"
#include "numpyarray.h"

const int N = 2;
typedef std::array<uint16_t, N> arr_t;

void putVec( std::ostream& s, arr_t& array )
{
    for( uint d = 0; d < N; d++ )
        s << (int) array.at(d) << " ";
    
    s << std::endl;
}

int main( int argc, char* argv[] )
{
    int numBlocks = argc > 1 ? atoi( argv[1] ) : 1<<31;
    
    std::array<uint16_t, N> shape;
    shape[0] = 50;
    shape[1] = 40;
    //shape[2] = 50;
    
    NumPyArray<int, 2>::array_t writeTime( boost::extents[50][40] );
    
    BlockGenerator<N> s( shape );
    s.setBlockSize(20);
    
    ParameterSpacePart<N> p;
    
    int counter = 1;
    while( numBlocks-- && s.take( p ) ) {
        std::cout << "part: " << p.flags << std::endl;
        std::cout << "  x0: ";
        putVec(std::cout, p.x0);
        std::cout << "  dx: ";
        putVec(std::cout, p.dx);
        std::cout << "  x1: ";
        putVec(std::cout, p.x1);
        
        ParameterGenerator<N> g( p );
        
        std::array<index_t, N> x;
        while( g.next( x ) ) {
            assert( writeTime[x[0]][x[1]] == 0 );
            writeTime[x[0]][x[1]] = counter;
        }
        counter++;
    }
    
    std::ofstream tf("/tmp/times.npy");
    NumPyArray<int, 2>::write( writeTime, tf );
    
    return 0;
}
