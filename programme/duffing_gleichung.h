/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef USE_SINGLE
#define USE_DUFFING_GL
#endif

#ifdef USE_DUFFING_GL
#define MAIN_CLASS DuffingGleichung

#include "canvas.h"
#include "instance.h"
#include "pool.h"
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54.hpp>
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/integrate/integrate_times.hpp>
#include <functional>
#include "configreader.h"
#include "duffing.h"
#include "statetools.h"

class DuffingGleichung : public Instance {
public:
    
    typedef double real;
    typedef Canvas<real, float>       canvas_t;
    typedef boost::array<real, 2>       state_t;
    
    DuffingGleichung( ConfigReader& config ):
        Instance(),
        m_canvas( config, 0 )
    {
        config.setSection("duffing");
        
        m_duffing.init( config );
        
        m_t0 = config.get<real>("t0");
        m_t1 = config.get<real>("t1");
        m_dt = config.get<real>("dt");
        m_state[ 0 ] = config.get<real>("x0");
        m_state[ 1 ] = config.get<real>("y0");
        m_phase = config.get<real>("phase", 0.);
        m_poincare = config.get<bool>("poincare");
        m_file = config.get<std::string>("pointsfile", std::string());
    }
    
    virtual void run() {
        m_canvas.moveTo( m_state[0], m_state[1] );
        
        boost::numeric::odeint::runge_kutta_cash_karp54<state_t, real> stepper;
        
        if( m_poincare ) {
            std::ofstream pointsStream;
            // ValueTester<real, 16, 2> tester( 1e-5 );
            
            if( m_file.size() ) {
                pointsStream.open( m_file );
                pointsStream << m_state[0] << " " << m_state[1] << std::endl;
            }
            real increment = 2.0 * M_PI / m_duffing.Omega;
            int stepCount = (m_t1 - m_t0) / increment;
            for( int step = 0; step < stepCount; step++ ) {
                boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, m_state, m_phase, m_phase + increment, m_dt );
                
                m_canvas.add( m_state[0], m_state[1], 1 );
                if( pointsStream.is_open() )
                    pointsStream << m_state[0] << " " << m_state[1] << std::endl;
                    
                setProgress( step, stepCount );
                //if( tester.test( m_state ) )
                //    break;
            }
        } else {
            auto callback = std::bind(
                &DuffingGleichung::addLine, this, std::placeholders::_1, std::placeholders::_2 );
            
            boost::numeric::odeint::integrate_adaptive( stepper, m_duffing, m_state, m_t0, m_t1, m_dt, callback );
        }
        setProgress( 1.0 );
    }
    
    void addLine( state_t& state, real t ) {
        m_canvas.lineTo( state[0], state[1], 1 );
        setProgress( (t - m_t0) / (m_t1 - m_t0) );
    }
    
    void collect( DuffingGleichung& other ) {
        m_canvas += other.m_canvas;
    }
    
    void finish( ConfigReader& config ) {
        m_canvas.write();
    }
    
private:
    DuffingGl<real>     m_duffing;
    real                m_t0;
    real                m_t1;
    real                m_dt;
    real                m_phase;
    bool                m_poincare;
    state_t             m_state;
    canvas_t            m_canvas;
    std::string         m_file;
};

#endif