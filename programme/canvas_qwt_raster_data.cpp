/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "canvas_qwt_raster_data.h"

CanvasQwtRasterData::CanvasQwtRasterData( ConfigReader& conf, const char* name ): Canvas<double, double>( conf, 0.0, name )
{
    updateSize();
}

CanvasQwtRasterData::CanvasQwtRasterData( double x0, double x1, double y0, double y1, int width, int height ):
    Canvas<double, double>( x0, x1, y0, y1, width, height, 0.0 )
{
    updateSize();
}

CanvasQwtRasterData::~CanvasQwtRasterData()
{

}

void CanvasQwtRasterData::updateRange()
{
    setInterval( Qt::ZAxis, QwtInterval( 0.0, m_maxValue ) );
}

void CanvasQwtRasterData::updateSize()
{
    setInterval( Qt::XAxis, QwtInterval( x_a, x_a + m_width / x_b, QwtInterval::ExcludeMaximum ) );
    setInterval( Qt::YAxis, QwtInterval( y_a + m_height / y_b, y_a, QwtInterval::ExcludeMaximum ) );
    updateRange();
}

QRectF CanvasQwtRasterData::pixelHint(const QRectF& area) const
{
    return QRectF( x_a, y_a, 1.0 / x_b, 1.0 / y_b );
}

double CanvasQwtRasterData::value( double x, double y ) const
{
        int px = (x - x_a) * x_b;
        int py = (y - y_a) * y_b;
        if( px >= 0 && px < m_width && py >= 0 && py < m_height )
            return m_data[ py ][ px ];
        else
            return 0.0;
}
