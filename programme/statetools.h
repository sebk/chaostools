/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STATETOOLS_H
#define STATETOOLS_H

template <class T, int N, int M>
class ValueTester {
    T   values[ N ][ M ];
    T   epsilonSquared;
    int p;
    
public:
    ValueTester( T epsilon ) : epsilonSquared( epsilon * epsilon )
    {
        for( int n = 0; n < N; n++ ) {
            for( int m = 0; m < M; m++ )
                values[ n ][ m ] = std::numeric_limits<T>::infinity();
        }
        p = 0;
    }
    
    int test( const boost::array<T, M>& v )
    {
        for( int n = 0; n < N; n++ ) {
            T sum = 0.0;
            for( int m = 0; m < M; m++ ) {
                T delta = v[ m ] - values[ n ][ m ];
                sum += delta * delta;
            }
            if( sum <= epsilonSquared )
                return n > p ? p + N - n : p - n;
        }
        
        for( int m = 0; m < M; m++ )
            values[ p ][ m ] = v[ m ];
        
        p++;
        if( p == N )
            p = 0;
        
        return 0;
    }
};

template <class T, uint M>
class TrajectoryList
{
public:
    typedef boost::array<T, 2>          state_t;
    typedef std::vector<state_t>        trajectory_t;
    
private:
    std::vector<trajectory_t>           trajectories;
    T                                   m_epsilonSquared;
    
    T stateDeltaSquared( const state_t& a, const state_t& b )
    {
        T sum = 0.0;
        for( uint m = 0; m < M; m++ ) {
            T d = a[ m ] - b[ m ];
            sum += d * d;
        }
        return sum;
    }
    
public:
    TrajectoryList( T epsilon )
    {
        m_epsilonSquared = epsilon * epsilon;
    }
    
    int index( const state_t& s )
    {
        for( uint i = 0; i < trajectories.size(); i++ ) {
            const trajectory_t& t = trajectories[ i ];
            
            for( uint j = 0; j < t.size(); j++ ) {
                if( stateDeltaSquared( s, t[ j ] ) < m_epsilonSquared ) {
                    return i;
                }
            }
                
        }
        
        return -1;
    }
    
    int add( trajectory_t t )
    {
        trajectories.push_back( t );
        return trajectories.size() - 1;
    }
    
    void update( uint index, trajectory_t t )
    {
        trajectories[ index ] = t;
    }
    
    void save( std::ofstream& stream )
    {
        std::scientific( stream );
        for( uint i = 0; i < trajectories.size(); i++ )  {
            const trajectory_t& t = trajectories[ i ];
            
            for( uint j = 0; j < t.size(); j++ ) {
                stream << i;
                const state_t& state = t[ j ];
                
                for( uint m = 0; m < M; m++ )
                    stream << "\t" << state[ m ];
                
                stream << std::endl;
            }
        }
    }
};

#endif
