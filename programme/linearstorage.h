/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINEARSTORAGE_H
#define LINEARSTORAGE_H

#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <memory>
#include <list>
#include <stdexcept>
#include <functional>
#include <mutex>
#include <iostream>
#include <iomanip>
#include "stdfile.h"
#include "mapped_file.h"

class LinearStorage
{
    const char*         infostring = "LinearStorage";
    const uint16_t      version_number = 0;
    
    typedef __off64_t   pos_t;
    typedef int64_t     index_t;
    typedef uint16_t    type_t;
    
    static const int           MAX_SIZE_BITS = 40;
    static const int           INDEX_BITS = 8;
    
    static const pos_t          MAX_NODE_SIZE = 1024 * 1024 * 1024;
    
    static_assert( MAX_SIZE_BITS < (sizeof(pos_t) * 8), "MAX_SIZE_BITS does not fit into pos_t" );
    // static_assert( (MAX_SIZE_BITS % INDEX_BITS), "MAX_SIZE_BITS has to be a multible of INDEX_BITS" );
    
    static const int           INDEX_DEPTH = MAX_SIZE_BITS / INDEX_BITS;
    static const pos_t         MAX_SIZE = 1LL << MAX_SIZE_BITS;
    static const index_t       INDEX_NUMS = 1LL << INDEX_BITS;
    static const index_t       INDEX_MASK = INDEX_NUMS - 1LL;
    
    std::mutex                  m_mutex;
    
public:
    enum nodeType {
        InfoNode = 0,
        IndexNode,
        MarkerNode,
        
        DataNode = 256,
        IntArrayInfo,
        FloatArrayNd = 500,
        FloatArray1d,
        FloatArray2d,
        DoubleArrayNd = 510,
        DoubleArray1d,
        DoubleArray2d,
        Int16ArrayNd = 520,
        Int16Array1d,
        Int16Array2d,
        
        numNodeTypes = 1024
    };
     
    template <typename T>
    struct node_delete {
        void operator()( T* ptr )
        {
            free( ptr );
        }
    };
    
    template <typename T>
    class node_ptr : public std::unique_ptr<T, node_delete<T>> {
        typedef std::unique_ptr<T, node_delete<T>> super;
        
        friend class LinearStorage;
        
        T* get()
        {
            return &(super::operator*());
        }
        
        node_ptr( T* p ) : super( p ) {}
        
    public:
        node_ptr() : super() {}
        
        void free()
        {
            ::free( super::release() );
        }
        
        template <typename C>
        node_ptr<C> convert()
        {
            return node_ptr<C>( (C*) super::release() );
        }
    };
    
    typedef struct {
        index_t         index; // not required, only safety
        pos_t           size;
        type_t          type;
    } nodeHead_t;
    
    typedef std::function<void(node_ptr<nodeHead_t>)> callback_t;
    
    callback_t          m_callbacks[numNodeTypes];
    
    typedef struct {
        nodeHead_t      head;
        struct {
            pos_t       position;
        }               nodes[INDEX_NUMS];
    } indexNode_t;
    
    typedef struct {
        nodeHead_t      head;
        char            name[14];
        uint16_t        version;
    } infoNode_t;
    
    typedef struct {
        float       offset;
        float       scale;
    } intArrayInfo_channel_t;
    
    typedef struct {
        nodeHead_t      head;
        uint32_t        channels;
        double          t0;
        double          dt;
        intArrayInfo_channel_t  channel[0];
    } intArrayInfo_t;
    
    typedef struct {
        nodeHead_t      head;
        index_t         ref;
        uint32_t        datapoints;
        uint32_t        channels;
        int16_t         data[0];
    } int16Array2d_t;
    
    typedef struct {
        nodeHead_t      head;
        uint32_t        datapoints;
        uint32_t        channels;
        float           data[0];
    } floatArray2d_t;
    
private:
    class File : public MappedFile //StdFile
    {
        
    public:
        File( const char* filename, bool overwrite = false )
        {
            open( filename, overwrite );
        }
        
        template <typename S>
        void read( S* s, pos_t position, pos_t size = sizeof(S), bool sequential = false )
        {
            assert( size >= sizeof(nodeHead_t) );
            _read( (char*) s, position, size, sequential );
            
            assert( size <= ((nodeHead_t*) s)->size );
        }
        
        void readHead( nodeHead_t* head, pos_t position )
        {
            _read( (char*) head, position, sizeof(nodeHead_t) );
        }
        
        template <typename S>
        void write( const S* s, pos_t position, pos_t size = sizeof(S) )
        {
            assert( ((nodeHead_t*) s)->size == size );
            _write( (char*) s, position, size );
        }
        
        template <typename S>
        pos_t append( const S* s, pos_t size = sizeof(S) )
        {
            assert( s->head.size == size );
            return _append( (char*) s, size );
        }
        
    };
    
    class IndexCache
    {
        const int               maxDirtyInodes = 1024;
        const int               maxReadCacheSize = 1024;
        
        class InodeEntry {
        public:
            pos_t                               position;
            indexNode_t                         inode;
            InodeEntry( pos_t pos, const indexNode_t& node ) : position( pos ), inode( node ) {}
            InodeEntry() {}
            bool operator<( const InodeEntry& other ) const {
                return position < other.position;
            }
        };
        
        std::list<InodeEntry>                   m_dirtyInodes;
        std::list<InodeEntry>                   m_readCache;
        File&                                   m_file;
        
        std::size_t                             m_read_misses;
        std::size_t                             m_read_hits_writecache;
        std::size_t                             m_read_hits_readcache;
        std::size_t                             m_write_misses;
        std::size_t                             m_write_hits_writecache;
        std::size_t                             m_write_hits_readcache;
        
    public:
        IndexCache( File& file) : m_file( file ) {
            m_read_misses = 0;
            m_read_hits_writecache = 0;
            m_read_hits_readcache = 0;
            m_write_misses = 0;
            m_write_hits_writecache = 0;
            m_write_hits_readcache = 0;
            
            m_file.setFlushCallback( std::bind( &IndexCache::flushArea, this, std::placeholders::_1, std::placeholders::_2 ) );
        }
        ~IndexCache() {
            flush();
            
            std::cout << "IndexCache:" << std::endl;
            std::cout << "  reads:                " << std::endl;
            std::cout << "    hits in writecache: " << std::setw(10) << m_read_hits_writecache << std::endl;
            std::cout << "    hits in readcache:  " << std::setw(10) << m_read_hits_readcache << std::endl;
            std::cout << "    misses:             " << std::setw(10) << m_read_misses << std::endl;
            std::cout << "  writes:               " << std::endl;
            std::cout << "    hits in writecache: " << std::setw(10) << m_write_hits_writecache << std::endl;
            std::cout << "    hits in readcache:  " << std::setw(10) << m_write_hits_readcache << std::endl;
            std::cout << "    misses:             " << std::setw(10) << m_write_misses << std::endl;
        }
        
        void flush();
        
        // flush write cache between [ lower, upper )
        void flushArea( LinearStorage::pos_t lower, LinearStorage::pos_t upper );
        
        // returned pointer is valid until the next read, write of flush operation
        const indexNode_t* read( pos_t position );
        
        // do not pass pointers taken from read()
        void write( const indexNode_t* node, pos_t position );
        
    };
    
    /* members */
    File                m_file;
    IndexCache          m_cache;
    index_t             m_nodeCount;
    
    pos_t               m_endpos;
    
    
    /* member functions */
    
    // return position of node
    pos_t findNode( index_t index );
    
    // initializes index and size and returns the position of the new node
    // call only once per node
    pos_t newNode( nodeHead_t* head, nodeType type, pos_t size) {
        head->index = m_nodeCount++;
        head->size = size;
        head->type = type;
        
        pos_t pos = m_endpos;
        m_endpos += size;
        
        return pos;
    }
    
    
    // create inode structure of depth depth and return the position of the first node
    pos_t createIndex( int depth );
    
    // create a link to the next node to be created with newNode
    void createLink();
    
    void initFile();
    void loadFile();
    
public:

    LinearStorage( const char* filename, bool create = false );
    
    ~LinearStorage()
    {
        m_mutex.lock();
    }
    
    // set index, type and pos of the newly linked node and write it
    template <typename T>
    void createNodeOfType( node_ptr<T>& node, nodeType type, pos_t size ) {
        std::lock_guard<std::mutex> lock( m_mutex );
        
        createLink();
        pos_t pos = newNode( &node->head, type, size );
        m_file.write<T>( &(*node), pos, size );
    }
    
    // read the node and return a reference to it. call freeNode on it later.
    // returns NULL if the node is not found
    template <typename T>
    node_ptr<T> readNode( index_t index )
    {
        std::lock_guard<std::mutex> lock( m_mutex );
        
        pos_t pos = findNode( index );
        if( pos < 0 )
            return NULL;
        
        nodeHead_t head;
        m_file.read<nodeHead_t>( &head, pos );
        assert( head.index == index );
        assert( head.size >= sizeof( nodeHead_t ) );
        
        node_ptr<T> node = createNode<T>( head.size );
        m_file.read<T>( node.get(), pos, head.size );
        
        return node;
    }
    
    template <typename T>
    node_ptr<T> createNode( pos_t size = sizeof(T) )
    {
        assert( size <= MAX_NODE_SIZE );
        node_ptr<T> p( (T*) std::malloc( size ) );
        return p;
    }
    
    void flush() {
        std::lock_guard<std::mutex> lock( m_mutex );
        
        m_cache.flush();
    }
    
    void registerCallback( callback_t callback, nodeType type )
    {
        std::lock_guard<std::mutex> lock( m_mutex );
        
        assert( type < numNodeTypes );
        m_callbacks[ type ] = callback;
    }
    
    void readNodes( bool& ok, index_t first = 0, index_t last = -1 )
    {
        m_mutex.lock();
        
        if( last < 0 )
            last = m_nodeCount;
        
        for( index_t index = first; ok && index < last; index++ ) {
            pos_t pos = findNode( index );
            if( pos < 0 )
                continue;
            
            nodeHead_t head;
            m_file.readHead( &head, pos );
            assert( head.index == index );
            assert( head.size >= sizeof( nodeHead_t ) );
            assert( head.type < numNodeTypes );
            
            if( m_callbacks[ head.type ] ) {
                node_ptr<nodeHead_t> node = createNode<nodeHead_t>( head.size );
                
                m_file.read<nodeHead_t>( node.get(), pos, head.size, true );
                
                m_mutex.unlock();
                m_callbacks[ head.type ]( std::move(node) );
                m_mutex.lock();
            }
        }
        
        m_mutex.unlock();
    }
};

#endif
