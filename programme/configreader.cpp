#include "configreader.h"
#include <iostream>

const char* ConfigReader::ParseError::what() const throw()
{
    std::ostringstream s;
    std::cerr << "parse error: " << message << " at " << filename << ":" << line << std::endl;
    
    return s.str().c_str();
}

const char* ConfigReader::MissingEntry::what() const throw()
{
    std::ostringstream s;
    s << "section [" << section << "]: entry '" << entry << "' not found.";
    
    std::cerr << s.str() << std::endl;    
    
    return s.str().c_str();
}

const char* ConfigReader::ConvertError::what() const throw()
{
    std::ostringstream s;
    s << "section [" << section << "]: entry '" << entry 
      << "': can not convert '" << value << "' to " << type.name();
    
    std::cerr << s.str() << std::endl;
    
    return s.str().c_str();
}

void ConfigReader::merge( ConfigReader::ptree& dest, const ConfigReader::ptree& source )
{
    for( ptree::const_iterator source_it = source.begin(); source_it != source.end(); ++source_it ) {
        ptree::assoc_iterator dest_it = dest.find( source_it->first );
        if( dest_it == dest.not_found() ) {
            // just copy it
            dest.push_back( *source_it );
        }
        else
            merge( dest_it->second, source_it->second );
    }

    dest.data() = source.data();
}

void ConfigReader::execute( std::vector< std::string > args, const char* filename, int line )
{
    if( args.size() < 1 )
        throw ParseError( "no command name given", filename, line );
    
    string name = args[ 0 ];
    
    if( name == "include" ) {
        if( args.size() != 2 )
            throw ParseError( "@include requires exactly one argument", filename, line );
        
        string filename = args[ 1 ];
        
        addFile( filename );
    } else if( name == "alias" ) {
        if( args.size() != 3 )
            throw ParseError( "@alias requires exactly two arguments", filename, line );
        
        createAlias( args[ 1 ], args[ 2 ], filename, line );
    } else {
        throw ParseError( "unknown command", filename, line );
    }
}

/*!
 * write config items to stdout
 */
void ConfigReader::dump( ConfigReader::ptree* t, int level ) const
{
    for( auto it = t->begin(); it != t->end(); it++ ) {
        for( int i = 0; i < level; i++ )
            std::cout << "  ";
        
        std::cout << it->first << " (" << it->second.data() << ")" << std::endl;
        dump( &it->second, level + 1 );
    }
}

ConfigReader::string ConfigReader::parse( const ConfigReader::string& s )
{
    std::ostringstream out;
    string::const_iterator it = s.begin();
    while( it != s.end() ) {
        if( *it == '%' ) {
            string::const_iterator begin = it + 1;
            if( *begin == '(' ) {
                begin++;
                
                string::const_iterator end = begin;
                while( end != s.end() && *end != ')' )
                    end++;
                
                if( end != s.end() ) {
                    string key( begin, end );
                    try {
                        out << parse( tree.get_child( key ).data() );
                        it = end + 1;
                        continue;
                    } catch( boost::property_tree::ptree_bad_path ) {
                        
                    }
                }
            }
        }
        
        out << *it++;
    }
    
    return out.str();
}

int ConfigReader::sysargs( int argc, char** argv, int& loglevel )
{
    const char* usage = "usage: %s [-h] [-v] [--section.key=value] [configfile]";
    
    for( int i = 1; i < argc; i++ ) {
        char* arg = argv[ i ];
        if( arg[0] == '-' ) {
            if( arg[1] == '-' ) {
                char* key = arg + 2;
                char* value = index( key, '=' );
                if( value ) {
                    *value = '\0';
                    value++;
                    
                    setOption( key, value );
                } else if( i < (argc - 1) ) {
                    value = argv[ i + 1 ];
                    setOption( key, value );
                    i++;
                }
            } else {
                if( arg[1] && !arg[2] ) {
                    switch( arg[1] ) {
                        case 'v':
                            loglevel++;
                            break;
                        case 'h':
                            printf( usage, argv[0] );
                            return 1;
                        default:
                            printf( "unknown option: -%c\nuse -h for help\n", arg[1] );
                            return 1;
                    }
                } else {
                    printf( "invalid argument: %s\ntry -h for help\n", arg );
                    return 1;
                }
            }
        } else {
            addFile( arg );
        }
    }
    
    return 0;
}

void ConfigReader::parseStream( std::istream& stream, const char* filename )
{
    const char comment = '#';
    const char lbracket = '[';
    const char rbracket = ']';
    const char command = '@';
    
    ptree* section = NULL;
    int lineno = 0;
    string line;
    while( stream.good() ) {
        getline( stream, line );
        lineno++;
        
        if( stream.eof() )
            break;
        
        line = strip( line );
        if( line.empty() )
            continue;
        
        switch( line[0] ) {
            case comment:
                break;
                
            case command:
                execute( split( strip( line, 1, string::npos ) ), filename, lineno );
                break;
                
            case lbracket: {
                string::size_type end = line.find( rbracket );
                if( end == string::npos )
                    throw ParseError( "missing ']'", filename, lineno );
                
                string key = strip( line, 1, end );
                auto it = tree.find( key );
                if( it == tree.not_found() )
                    section = &tree.push_back( std::make_pair( key, ptree() ) )->second;
                else if( !allowDublicateSections )
                    throw ParseError( "dublicate section", filename, lineno );
                else
                    section = &it->second;
                
                break;
            }
            
            default: {
                if( !section )
                    throw ParseError( "no section set", filename, lineno );
                
                string::size_type eqpos = line.find( '=' );
                if (eqpos == 0)
                    throw ParseError( "missing key", filename, lineno );
                
                string key = strip( line, 0, eqpos );
                string data = strip( line, eqpos + 1, string::npos );
                
                auto it = section->find( key );
                if( it == section->not_found() )
                    section->push_back( make_pair( key, ptree( data ) ) );
                else if( !allowDublicateKeys )
                    throw ParseError( "dublicate key", filename, lineno );
                else
                    it->second.data() = data;
            }
        }
    }
}

void ConfigReader::setOption( ConfigReader::string option, ConfigReader::string value )
{
    size_t dotPos = option.rfind('.');
    ptree* section;
    std::string keyStr;
    
    if( dotPos == std::string::npos ) {
        section = defaultSection;
        keyStr = option;
    } else {
        std::string sectionStr = option.substr( 0, dotPos );
        keyStr = option.substr( dotPos + 1, option.length() - dotPos - 1 );
        
        auto s_it = tree.find( sectionStr );
        if( s_it == tree.not_found() )
            section = &tree.push_back( std::make_pair( sectionStr, ptree() ) )->second;
        else
            section = &s_it->second;
    }
    auto k_it = section->find( keyStr );
    if( k_it == section->not_found() )
        section->push_back( std::make_pair( keyStr, ptree( value ) ) );
    else
        k_it->second.data() = value;
}
