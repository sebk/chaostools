/*
 *  This file is part of ChaosTools.
 *
 *  ChaosTools is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ChaosTools is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ChaosTools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dataprocessor.h"
#include "analyze.h"
#include <functional>
#include <memory>

void syserror( const char* message )
{
    throw std::system_error( std::error_code( errno, std::system_category() ), std::string( message ) );
}

QwtRasterData* DataProcesser::init( ConfigReader& config )
{
    config.setSection( "feigenbaum" );
    m_sourceChannel = config.get<int>("source");
    m_XChannel = config.get<int>("x-channel");
    m_YChannel = config.get<int>("y-channel");
    m_triggerChannel = config.get<int>("trigger");
    m_threshold = config.get<double>("threshold");
    m_phaseshift = config.get<double>("phaseshift");
    m_sourceMin = config.get<double>("source-min");
    m_sourceMax = config.get<double>("source-max");
    m_filename = config.default_get<std::string>("canvasinfo");
    m_datafile = config.default_get<std::string>("datafile");

    m_storage = new LinearStorage( config.get<std::string>("storage").c_str() );
    
    m_storage->registerCallback( std::bind( &DataProcesser::callback, this, std::placeholders::_1 ), LinearStorage::FloatArray2d );
    m_storage->registerCallback( std::bind( &DataProcesser::callback, this, std::placeholders::_1 ), LinearStorage::Int16Array2d );
    
    m_socket = socket(AF_INET, SOCK_STREAM, 0);
    if( m_socket < 0 ) 
        syserror("error opening socket");
    
    struct sockaddr_in serv_addr;
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons( config.get<unsigned short>("port") );
    
    /* Enable address reuse */
    int on = 1;
    if( setsockopt( m_socket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) ) )
        syserror("error on setsockopt reuseaddr");
    
    if( bind(m_socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0 )
        syserror("error on bind");
    
    /*
    bool autoscale = config.get<bool>("autoscale");
    if( autoscale ) {
        m_width = config.get<int>("width");
        m_oversample = config.get<int>("oversample");
    } else {
        m_canvas = new CanvasQwtRasterData( config );
    }
    */
    
    m_canvas = new CanvasQwtRasterData( config );
    
    config.setSection("canvas");
    m_clip_canvas = config.get<bool>("clip", false);
    
    if( config.get<bool>("resize", false) )
        m_canvas->enableResize();
    
    return dynamic_cast<QwtRasterData*>( m_canvas );
}

void DataProcesser::handler( SocketStream& s )
{
    char command[16];

    LinearStorage::node_ptr<LinearStorage::intArrayInfo_t> info;
    while( m_running ) {
        s.readLine( command, sizeof(command) );
        if( !strcmp(command, "SETUP") ) {
            if( info )
                info.free();

            LinearStorage::intArrayInfo_t info_head;
            s.read( (char*) &info_head.channels, sizeof(info_head.channels) );
            s.read( (char*) &info_head.t0, sizeof(info_head.t0) );
            s.read( (char*) &info_head.dt, sizeof(info_head.dt) );

            std::size_t datasize = sizeof(LinearStorage::intArrayInfo_t) + info_head.channels * sizeof(LinearStorage::intArrayInfo_channel_t);
            info = m_storage->createNode<LinearStorage::intArrayInfo_t>( datasize );
            *info = info_head;

            for( uint8_t c = 0; c < info->channels; c++ ) {
                LinearStorage::intArrayInfo_channel_t* ch = &info->channel[c];
                s.read( (char*) &ch->offset, sizeof(ch->offset) );
                s.read( (char*) &ch->scale, sizeof(ch->scale) );
            }

            char endmsg[4];
            s.readLine( endmsg, sizeof(endmsg) );
            if( strcmp( endmsg, "END" ) ) {
                std::cout << "bad data" << std::endl;
                break;
            }
            
            m_storage->createNodeOfType<LinearStorage::intArrayInfo_t>( info, LinearStorage::IntArrayInfo, datasize );

        } else if( !strcmp( command, "DATA INT16" ) ) {
            if( !info )
                break;

            LinearStorage::int16Array2d_t array_head;
            array_head.ref = info->head.index;
            s.read( (char*) &array_head.datapoints, sizeof(array_head.datapoints) );
            s.read( (char*) &array_head.channels, sizeof(array_head.channels) );

            if( array_head.channels != info->channels )
                break;

            std::size_t datasize = sizeof(LinearStorage::int16Array2d_t) + sizeof( int16_t ) * array_head.channels * array_head.datapoints;
            LinearStorage::node_ptr<LinearStorage::int16Array2d_t> array = m_storage->createNode<LinearStorage::int16Array2d_t>( datasize );
            *array = array_head;

            s.read( (char*) &array->data[0], sizeof( int16_t ) * array_head.channels * array_head.datapoints );

            char endmsg[4];
            s.readLine( endmsg, sizeof(endmsg) );
            if( strcmp( endmsg, "END" ) ) {
                std::cout << "bad data" << std::endl;
                break;
            }
            
            m_storage->createNodeOfType<LinearStorage::int16Array2d_t>( array, LinearStorage::Int16Array2d, datasize );

            processInt16Array2d( array, info );
        } else if( !strcmp( command, "DATA FLOAT" ) ) {
            LinearStorage::floatArray2d_t array_head;
            s.read( (char*) &array_head.datapoints, sizeof(array_head.datapoints) );
            s.read( (char*) &array_head.channels, sizeof(array_head.channels) );

            std::size_t dataSize = sizeof(LinearStorage::floatArray2d_t) + array_head.datapoints * array_head.channels * sizeof( float );
            LinearStorage::node_ptr<LinearStorage::floatArray2d_t> array = m_storage->createNode<LinearStorage::floatArray2d_t>( dataSize );
            *array = array_head;

            s.read( (char*) &array->data[0], sizeof( float ) * array_head.channels * array_head.datapoints );

            char endmsg[4];
            s.readLine( endmsg, sizeof(endmsg) );
            if( strcmp( endmsg, "END" ) ) {
                std::cout << "bad data" << std::endl;
                break;
            }
            
            m_storage->createNodeOfType( array, LinearStorage::FloatArray2d, dataSize );

            processFloatArray2d( std::move(array) );
        } else {
            std::cout << "invalid command" << std::endl;
            break;
        }
    }
}

void DataProcesser::run()
{
    m_running = true;
    m_storage->readNodes( m_running );
    
    listen( m_socket, 1 );
    while( m_running ) {
        struct sockaddr_in cli_addr;
        socklen_t clilen = sizeof(cli_addr);
        int newsockfd = accept(m_socket, (struct sockaddr *)&cli_addr, &clilen);
        if (newsockfd < 0) 
            break;
        
        try {
            SocketStream s( newsockfd );
            handler( s );
        }
        catch( SocketStream::SocketError& e ) {
            std::cout << "socket error" << std::endl;
        }
    }
}

void DataProcesser::callback( LinearStorage::node_ptr<LinearStorage::nodeHead_t> head )
{
    switch( head->type ) {
        case LinearStorage::FloatArray2d:
            processFloatArray2d( head.convert<LinearStorage::floatArray2d_t>() );
            break;
        case LinearStorage::Int16Array2d:
            processInt16Array2d( head.convert<LinearStorage::int16Array2d_t>() );
            break;
        default:
            break;
    }
}

void DataProcesser::processFloatArray2d( const LinearStorage::node_ptr<LinearStorage::floatArray2d_t>& floatArray )
{
    array_t array( boost::extents[floatArray->datapoints][floatArray->channels], boost::c_storage_order() );
    
    const float* ptr = &floatArray->data[0];
    for( uint n = 0; n < floatArray->datapoints; n++ ) {
        for( uint m = 0; m < floatArray->channels; m++ )
            array[ n ][ m ] = *ptr++;
    }

    processArray( array );
}

void DataProcesser::processInt16Array2d( const LinearStorage::node_ptr<LinearStorage::int16Array2d_t>& intArray,
                                         LinearStorage::node_ptr<LinearStorage::intArrayInfo_t>& info )
{
    if( !info ) {
        info = m_storage->readNode<LinearStorage::intArrayInfo_t>( intArray->ref );
    } else
        assert( intArray->ref == info->head.index );
    
    assert( info->channels == intArray->channels );
    
    array_t array( boost::extents[intArray->datapoints][intArray->channels + 1], boost::c_storage_order() );
    
    for( uint n = 0; n < intArray->datapoints; n++ )
        array[ n ][ 0 ] = info->t0 + info->dt * n;
        
    const int16_t* ptr = &intArray->data[0];
    for( uint n = 0; n < intArray->datapoints; n++ ) {
        for( uint m = 0; m < intArray->channels; m++ ) {
            double v = info->channel[m].offset + info->channel[m].scale * *ptr++;
            array[ n ][ m + 1 ] = v;
        }
    }

    processArray( array );
}

void DataProcesser::processArray(DataProcesser::array_t& array)
{
    array_t::const_multi_array_ref ref( array );
    /*
    if( !m_canvas ) {
        m_canvas = createCanvas( ref, m_XChannel );
        m_canvas->enableResize();
    }
    */
    feigenbaum( ref );
    
    update( ref );
}

void DataProcesser::update( array_ref& ref )
{
    m_canvas->updateRange();
    //QwtRasterData* rasterdata = dynamic_cast<QwtRasterData*>( m_canvas );
    emit( dataChanged( /*rasterdata*/ m_lastVoltage ) );
}


CanvasQwtRasterData* DataProcesser::createCanvas( array_ref& ref, std::size_t channel )
{
    Analyze<double>::channelInfo_t info;
    Analyze<double>::analyzeChannel( ref, m_triggerChannel, info );
    info.max += info.delta;

    info.delta /= m_oversample;

    int height = (info.max - info.min) / info.delta + 0.5;
    int margin = height * 0.1;
    double y0 = info.min - margin * info.delta + info.delta * 0.5;
    double y1 = info.max + margin * info.delta + info.delta * 0.5;

    if( height > 10000 )
        throw;
    
    return new CanvasQwtRasterData( m_sourceMin, m_sourceMax, y0, y1, m_width, height + 2 * margin );
}

void DataProcesser::feigenbaum( array_ref& ref )
{
    int N = ref.shape()[0];

    if( N < 2 )
        return;

    int M = ref.shape()[1];
    if( m_triggerChannel >= M || m_XChannel >= M || m_YChannel >= M || m_sourceChannel >= M )
        throw;


    // find trigger
    std::vector<double> slopes;
    Analyze<double>::findTriggerSlopes( slopes, ref, m_triggerChannel, m_threshold );

    int slopeCount = slopes.size();

    if( slopeCount < 2 )
        return;

    // get avg. voltage
    int n_min = slopes[0];
    int n_max = slopes[slopeCount-1];
    
    double voltageSum = 0.0;
    for( int n = n_min; n < n_max; n++ ) {
        double v = ref[n][m_sourceChannel];
        voltageSum += v * v;
    }
    
    double voltage = sqrt( voltageSum / (n_max - n_min) );
    
    for( int i = 0; i < (slopeCount - 1); i++ ) {
        double t = slopes[i] + (slopes[i+1] - slopes[i]) * m_phaseshift;
        int t0 = t;
        double value = ref[t0][m_XChannel] * (1.0 - (t - t0)) + ref[t0+1][m_XChannel] * (t - t0);

        m_canvas->add( voltage, value, 1.0 );
    }
    
    m_lastVoltage = voltage;
}


#include "moc_dataprocessor.h"

