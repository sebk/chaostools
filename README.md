# chaostools

## Dokumentation

### Inhaltsverzeichnis
 - Konfigurationsdateien
 - Datensatz erzeugende Programme
 - Canvas
 - Bilderzeugung

### Struktur

ChaosTools besteht aus einer Reihe von Programmen und Skripten, welche aus Konfigurationsdateien Datensätze erzeugen und diese Datensätze visualisieren.

### Konfigurationsdateien

Die Einträge sind in Sektionen gegliedert, welche mit [sektion] begonnen werden. Jeder Eintrag sieht folgendermaßen aus: key = value wobei Leerzeichen ignoriert werden. Es darf keine Einträge geben, ohne dass vorher eine Sektion begonnen wurde. In jeder Zeit darf entweder nur eine Sektion begonnen werden, ein Eintrag gesetzt werden oder ein Befehl ausgeführt werden.
```
[default]
  datafile = test.dat
  threads = 4

[duffing]
  epsilon = 2.3
```
In diesem Beispiel werden in der Sektion default die Einträge datafile und threads gesetzt.
In der Sektion duffing wird epsilon auf 2,3 gesetzt.

Zusätzlich gibt es die Möglichkeit weiter Konfigurationsdateien einzubinden,
indem `@include DATEINAME` genutzt wird. Der angegebene Pfad ist relativ zu dem Arbeitsordner des Programms.

Es können auch ganze Sektionen kopiert werden: `@alias NEU ALT`.
Hierbei wird der Inhalt, welcher die Sektion ALT zu diesen Zeitpunkt hat,
in die Sektion NEU kopiert. Enthielt diese vorher Werte, so gehen diese verloren.
```
[A]
  x = 1
  y = 2

@alias B A

[A]
  y = 3
```
Die Sektion `A` hat nun die Werte `x = 1` und `y = 3`. Die Sektion `B` aber noch `x = 1` und `y = 2`.

Im folgenden wird mit `A.x` der Wert `x` der Sektion `A` bezeichnet.
Datensatz erzeugende Programme

Diese Programme erwarten als Argumente Konfigurationsdateien oder zusätzlich einzelne Werte.

Um das Programm `prog` mit der Konfigurationsdatei `config1.conf` auszuführen:
```
$ ./prog config1.conf
```

Einzelne Werte in der Konfiguration können mit
```
$ ./prog config1.conf --A.x=4
```
geändert oder gesetzt werden. Dabei werden die Parameter in der Reihenfolge verarbeitet, in der sich auch angegeben werden.

Für länger laufende Programme kann es informativ sein, sich den Fortschritt ausgeben zu lassen:
```
$ ./prog config1.conf -v
$ ./prog config1.conf -v -v
```
Der erste Fall gibt die aktuelle Zeit aus, sobald der Gesamtfortschritt um 1% gestiegen ist.
Der zweite Fall gibt jede Sekunde den Fortschritt jedes einzelnen Threads aus.
Name	Datei	Erzeugt

| duffing |	duffing_gleichung.h | Trajektorien und Poincaré–Schnitte der Duffing–Gleichung |

 - `duffincb` : `duffing-gleichung-bifurkation.h`
   Bifurkationsdiagramm der Duffing–Gleichung
 - `duffingc`:	`duffing_gleichung_convergence.h`
   Abhängigkeit der periodischen Bahn von den Anfangsbedingungen
 - `oszill`: 	`elektronischer-oszillator.h`
   Trajektorien und Poincaré–Schnitte des elektronischen Oszillators
 - `oszillb`: 	`elektronischer-oszillator-bifurkation.h`
   Bifurkationsdiagramm des elektronischen Oszillators
 - `oszillp`: 	`elektronischer-oszillator-phasenportrait.h`
   Phasenportrait des elektronischen Oszillators
 - `logistische`: 	`logistische_gleichung.h`
   Bifurkationsdiagramm der logistischen Gleichung
 - `sinabb`: 	`sinusabbildung.h`
   Bifurkationsdiagramm der Sinusabbildung
 - `pendelt`: 	`pendel_trace.h`
   Trajektorien des eletrostatischen Pendels
 - `pendelp`: 	`pendel_pot.h`
   Potential des eletrostatischen Pendels
 - `pendelc`: 	`pendel_convergence.h`
   Abhängigkeit der Stillstandsposition von den Anfangsbedingungen

### Canvas

Alle Berechnungen werden in einer oder mehrerer Zeichenebenen gespeichert. Diese haben die folgenden Parameter:
Einstellung	Bedeutung
```
data 	  Dateiname des Datensatzes
width 	Breite in Pixeln
height 	Höhe in Pixeln
x0 	    minimaler x–Wert
x1 	    maximaler x–Wert
y0 	    minimaler y–Wert
y1 	    maximaler y–Wert
```
 
Bilderzeugung
```
./overlay datafile [-o OPERATOR] colormap image [-C comment] [-D desciption] [-r [vmin]:[vmax]]

OPERATOR:
    add o               x + a
    mul f               x * f
    abs                 |x|
    real                Re(x)
    imag                Im(x)
    phase               arg(x)
    pow p               x^p
    log                 log(x)
    gauss sigma         apply gaussian filter with given sigma
```
gradients.png
